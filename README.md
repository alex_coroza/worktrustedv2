# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact




************************ADDITIONAL NOTES*******************************
Edit angular-material.js(1.0.4 as of now) file to make its datepicker to accept string dates. Just replace this code

      if (value && !(value instanceof Date)) {
        throw Error('The ng-model for md-datepicker must be a Date instance. ' +
            'Currently the model is a: ' + (typeof value));
      }

by this new code

      if (value && !(value instanceof Date)) {
        var parseValue = self.dateLocale.parseDate(value);
        if (isNaN(parseValue)) {
          throw Error('The ng-model for md-datepicker must be a Date instance. ' +
              'Currently the model is a: ' + (typeof value));
        } else {
          value = parseValue;
        }
      }