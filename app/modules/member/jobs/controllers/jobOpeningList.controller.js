angular.module('jobsModule')
.controller('jobOpeningListController', ['$scope', '$rootScope', '$http', '$timeout', '$state', '$mdDialog', '$mdToast', 'httpJob', 'utilityFunctions', 'dataStorage', 'user', 'jobsServices', function($scope, $rootScope, $http, $timeout, $state, $mdDialog, $mdToast, httpJob, utilityFunctions, dataStorage, user, jobsServices) {
	console.log('jobOpeningListController initialized!');

	$rootScope.$emit('activeLinkValueUpdate', { value: 'jobs' });

	$scope.user = user;
	$scope.jobOpenings = [];

	$scope.filters = {
		status: 'Posted',
		validDateOnly: true,
		search: '',
		companyId: '',
		populate: {
			company: {},
			applicants: {}
		}
	};



	var storedData = dataStorage.fetch();
	if(storedData.companyId) {
		$scope.filters.companyId = storedData.companyId;
	}



	$scope.loadingProperties = {
		promise: null,
	};



	$scope.reloadData = function() {
		$scope.loadJobOpenings();
	};



	$scope.loadJobOpenings = function() {
		// unset filters on default value
		if($scope.filters.search == '') delete $scope.filters.search;
		if($scope.filters.companyId == '') delete $scope.filters.companyId;
		if($scope.filters.employmentType == 'All') delete $scope.filters.employmentType;
		if($scope.filters.industryType == 'All') delete $scope.filters.industryType;
		if($scope.filters.expectedSalary == '') delete $scope.filters.expectedSalary;

		var processedFilters = angular.copy($scope.filters);

		if(!$scope.filters.employmentType) $scope.filters.employmentType = 'All';
		if(!$scope.filters.industryType) $scope.filters.industryType = 'All';

		$scope.loadingProperties.message = 'Loading job openings...';
		$scope.loadingProperties.promise = $http.post(httpJob.enlistJobOpenings, processedFilters)
		.success(function(data ,status) {
			$scope.jobOpenings = data;
		});
	};




	$scope.loadJobApplications = function() {
		var options = {
			memberId: $scope.user.member.id,
			populate: {
				jobOpening: {
					company: {},
					applicants: {}
				}
			}
		};

		$scope.loadingProperties.message = 'Loading job openings...';
		$scope.loadingProperties.promise = $http.post(httpJob.enlistJobApplicants, options)
		.success(function(data ,status) {
			$scope.jobOpenings = [];
			angular.forEach(data, function(jobApplication, key) {
				$scope.jobOpenings.push(jobApplication.jobOpening);
			});
		});
	};




	$scope.jobOpeningInfo = function(info) {
		$state.go('member.jobOpeningInfo', { id: info.id });
	};




	// update list based on search
	var searchDelay;
	$scope.searchChange = function() {
		// fetch new records
		if(searchDelay) $timeout.cancel(searchDelay);

		searchDelay = $timeout(function() {
			$scope.loadJobOpenings();
		}, 1000);
	};





	// checks if you already applied for a specific jobOpening
	$scope.applicationCheck = function(jobOpening) {
		var jobApplication = jobsServices.extractJobApplication(jobOpening, $scope.user);
		if(jobApplication.status) return true;
		else return false;
	};




	// display advanced search dialog
	$scope.showAdvancedSearch = function() {
		$mdDialog.show({
			templateUrl: DOMAIN+'app/modules/member/jobs/views/jobOpeningList/advancedSearch.tpl.html',
			controller: 'advancedSearchController',
			locals: { filters: $scope.filters }
		})
		.then(function(response) {
			$scope.filters = response;
			$scope.loadJobOpenings();
		});
	};






	$scope.clearFiltersAndReload = function() {
		$scope.filters.search = '';
		$scope.filters.employmentType = 'All';
		$scope.filters.industryType = 'All';
		$scope.filters.expectedSalary = '';
		$scope.filters.companyId = '';
		$scope.loadJobOpenings();
	};





	// fetch all members on page load
	$scope.loadJobOpenings();





}]);