angular.module('jobsModule')
.controller('jobOpeningInfoController', ['$scope', '$rootScope', '$http', '$timeout', '$state', '$mdDialog', '$mdToast', 'httpJob', 'utilityFunctions', 'dataStorage', 'user', 'jobsServices', function($scope, $rootScope, $http, $timeout, $state, $mdDialog, $mdToast, httpJob, utilityFunctions, dataStorage, user, jobsServices) {
	console.log('jobOpeningInfoController initialized!');

	$rootScope.$emit('activeLinkValueUpdate', { value: 'jobs' });

	$scope.user = user;
	$scope.jobOpening = {};
	$scope.jobApplication = {};

	$scope.loadingProperties = {
		promise: null,
	};


	$scope.loadJobOpening = function() {
		var readOptions = {
			id: $state.params.id, 
			populate: {
				applicants: {},
				company: {}
			}
		};

		$scope.loadingProperties.message = 'Loading job opening\'s info...';
		$scope.loadingProperties.promise = $http.post(httpJob.readJobOpening, readOptions)
		.success(function(data, status) {
			$scope.jobOpening = data;
			$scope.jobApplication = jobsServices.extractJobApplication($scope.jobOpening, $scope.user);
		});
	};




	$scope.sendApplication = function() {
		var jobApplicantInfo = {
			jobOpeningId: $scope.jobOpening.id,
			memberId: $scope.user.member.id,
			status: 'Pending'
		};

		$scope.loadingProperties.message = 'Sending job application...';
		$scope.loadingProperties.promise = $http.post(httpJob.saveJobApplicant, { info: jobApplicantInfo })
		.success(function(data, status) {
			var toast = $mdToast.simple()
				.textContent('Job application sent!')
				.position('bottom right');
			$mdToast.show(toast);
			$scope.loadJobOpening();
		});
	};


	$scope.sendApplicationConfirm = function() {
		var confirm = $mdDialog.confirm()
			.title('Send application to this job?')
			.textContent('You can cancel your application anytime after this.')
			.ok('Proceed')
			.cancel('Close');
		$mdDialog.show(confirm).then(function() {
			$scope.sendApplication();
		});
	};



	$scope.cancelApplication = function() {
		$scope.loadingProperties.message = 'Canceling job application...';
		$scope.loadingProperties.promise = $http.post(httpJob.deleteJobApplicant, { id: $scope.jobApplication.id })
		.success(function(data, status) {
			var toast = $mdToast.simple()
				.textContent('Job application canceled!')
				.position('bottom right');
			$mdToast.show(toast);
			$scope.loadJobOpening();
		});
	};



	$scope.cancelApplicationConfirm = function() {
		var confirm = $mdDialog.confirm()
			.title('Cancel job application?')
			.textContent('This action cannot be undone.')
			.ok('Proceed')
			.cancel('Close');
		$mdDialog.show(confirm).then(function() {
			$scope.cancelApplication();
		});
	};






	$scope.applicationCheck = function(jobOpening) {
		var applied = false;
		angular.forEach(jobOpening.applicants, function(applicant, key) {
			if(applicant.memberId == $scope.user.member.id) applied = true;
		});
		return applied;
	};




	// go back to jobOpeningList but add companyId in filters
	$scope.showCompanyJobOpenings = function() {
		dataStorage.store({ companyId: $scope.jobOpening.company.id });
		$state.go('member.jobOpeningList');
	};




	$scope.backToJobOpeningList = function() {
		$state.go('member.jobOpeningList');
	};









	$scope.loadJobOpening();





}]);