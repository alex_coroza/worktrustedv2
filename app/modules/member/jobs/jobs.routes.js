angular.module('jobsModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('member.jobOpeningList', {
			url: 'job-openings/',
			templateUrl: DOMAIN+'app/modules/member/jobs/views/jobOpeningList.view.html',
			controller: 'jobOpeningListController'
		})
		.state('member.jobOpeningInfo', {
			url: 'job-opening-info/{id}',
			templateUrl: DOMAIN+'app/modules/member/jobs/views/jobOpeningInfo.view.html',
			controller: 'jobOpeningInfoController'
		})
	; /*closing $stateProvider*/
}]);
