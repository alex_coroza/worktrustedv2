angular.module('jobsModule')
.factory('jobsServices', ['$q', '$state', '$mdDialog', function($q, $state, $mdDialog) {



	// extracts applicant's jobApplication from jobOpening.applicants
	this.extractJobApplication = function(jobOpening, user) {
		var jobApplication = {};

		angular.forEach(jobOpening.applicants, function(applicant, key) {
			if(applicant.memberId == user.member.id) jobApplication = applicant;
		});

		return jobApplication;
	};







	return this;


}]);