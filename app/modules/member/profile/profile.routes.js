angular.module('profileModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('member.profileInfo', {
			url: 'profile/',
			templateUrl: DOMAIN+'app/modules/member/profile/views/profileInfo.view.html',
			controller: 'profileInfoController'
		})
		.state('member.profileAccountEdit', {
			url: 'profile-account-edit/',
			templateUrl: DOMAIN+'app/modules/member/profile/views/accountEdit.view.html',
			controller: 'accountEditController'
		})
	; /*closing $stateProvider*/
}]);

				