angular.module('memberModule')
.controller('sideNavController', ['$scope', '$rootScope', '$http', '$state', '$mdSidenav', 'httpAccount', 'user', function($scope, $rootScope, $http, $state, $mdSidenav, httpAccount, user) {
	console.log('sideNavController initialized!');

	$scope.sideNavIsOpen = false;
	$scope.activeLink = 'dashboard';
	$rootScope.$on('activeLinkValueUpdate', function(event, args) {
		$scope.activeLink = args.value;
	});

	$scope.user = user;



	$scope.logout = function() {
		$http.post(httpAccount.logout, {  })
		.success(function(data, status) {
			window.location.href = DOMAIN+'guest';
		});
	};



	$scope.changeState = function(state, link) {
		$state.go(state);
		$scope.activeLink = link;
		$scope.sideNavIsOpen = false;
	};



}]);