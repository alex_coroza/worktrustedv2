angular.module('memberModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.when('', '/');
	$stateProvider
		.state('notFound', {
			url: '/fucking-page-not-found',
			templateUrl: 'app/modules/core/views/custom404.view.html'
		})
		.state('member', {
			url: '/',
			views: {
				'': {
					templateUrl: DOMAIN+'app/modules/member/member/views/member.view.html',
					controller: 'memberController'
				},
				'sideNav@member': {
					templateUrl: DOMAIN+'app/modules/member/member/views/member/sideNav.view.html',
					controller: 'sideNavController'
				},
				'content@member': {
					templateUrl: DOMAIN+'app/modules/member/member/views/member/content.view.html'
				}
			},
			resolve: {
				user: ['$q', '$http', 'httpAccount', function($q, $http, httpAccount) {
					var deferred = $q.defer();
					$http.post(httpAccount.fetchLoggedUser, { type: 'member' })
					.success(function(data, status) {
						deferred.resolve(data);
					});
					return deferred.promise;
				}]
			}
		})
	; /*$stateProvider closing*/
}]);
