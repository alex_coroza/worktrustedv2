angular.module('applyMembershipModule')
.controller('applyMembershipNavbarController', ['$scope', '$uibModal', function($scope, $uibModal) {
	console.log('applyMembershipNavbarController initialized!');


	$scope.showLogin = function() {
		var modalInstance = $uibModal.open({
			templateUrl: APPPATH+'modules/guest/login/views/login.view.html',
			controller: 'loginController'
		});
	};



}]);