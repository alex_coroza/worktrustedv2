angular.module('applyMembershipModule')
.controller('applyMembershipFormController', ['$scope', '$state', '$http', 'Upload', 'httpApplicant', 'regexPattern', 'utilityFunctions', function($scope, $state, $http, Upload, httpApplicant, regexPattern, utilityFunctions) {
	console.log('applyMembershipFormController initialized!');

	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();

	$scope.loadingProperties = {
		promise: null,
	};

	$scope.currentForm = 1;
	$scope.noWorkHistory = false;
	$scope.pictureFile = '';
	$scope.pictureFileError = 'No file selected!';



	$scope.applicant = {};

	// optional values(must have default value)
	// some models requires initial values
	$scope.applicant.middleName = '';
	$scope.applicant.street = '';
	$scope.applicant.tags = [];
	$scope.applicant.parent = { motherName: '', motherOccupation: '', fatherName: '', fatherOccupation: '' };
	$scope.applicant.spouse = { occupation: '', mobile: '', email: '' }; /*add a default value to optional spouse properties*/
	$scope.applicant.emergencyContact = { name: '', relation: 'Mother', mobile: '', email: '' };
	$scope.applicant.gender = 'male';
	$scope.applicant.civilStatus = 'Single';
	$scope.applicant.education = {};
	$scope.applicant.education.attainment = 'College Graduate';
	$scope.applicant.skills = [
		{ skill: '' }
	]; // add initial value by adding an empty skill object
	$scope.applicant.workHistories = [
		{ company: '', position: '', startDate: '', endDate: '' }
	]; // add initial value by adding empty workHistory object
	$scope.applicant.beneficiaries = [
		{ name: '', birthDate: '', relation: 'Mother' }
	];
	$scope.applicant.characterReferences = [
		{ name: '', occupation: '', company: '', mobile: '', email: '' },
		{ name: '', occupation: '', company: '', mobile: '', email: '' },
		{ name: '', occupation: '', company: '', mobile: '', email: '' }
	];





	$scope.datepickerStatus = {
		applicantBirthDate: false,
		spouseBirthDate: false,
		applicantBeneficiaries: [false],
		workHistoryStart: [false],
		workHistoryEnd: [false]
	};


	$scope.openDatepicker = function($event, key, subKey) {
		if(subKey || subKey === 0) {
			$scope.datepickerStatus[key][subKey] = true;
		} else {
			$scope.datepickerStatus[key] = true;
		}
	};


	$scope.nextForm = function() {
		if($scope.currentForm < 3)
			$scope.currentForm++;
	};

	$scope.previousForm = function() {
		if($scope.currentForm > 1)
			$scope.currentForm--;
	};


	$scope.alertError = function(form) {
		// set all fields to "touched"
		angular.forEach(form.$error, function(field) {
			angular.forEach(field, function(errorField) {
				errorField.$setTouched();
			});
		});
		if(!utilityFunctions.isEmptyObject(form.$error) || $scope.pictureFile == '') alert('See and fix the errors above before proceeding!');
	};


	$scope.addSkillField = function() {
		var x = 0;
		angular.forEach($scope.applicant.skills, function(skillObject, key) {
			if(skillObject.skill.trim() == '') x++; 
		});
		if(x == 0) $scope.applicant.skills.push({ skill: '' }); 
	};

	$scope.deleteSkillField = function() {
		if($scope.applicant.skills.length > 1) {
			var lastIndex = $scope.applicant.skills.length - 1;
			$scope.applicant.skills.splice(lastIndex, 1);
		}
	};



	$scope.addWorkHistoryField = function() {
		var x = 0;
		angular.forEach($scope.applicant.workHistories, function(workHistory, key) {
			if(workHistory.company.trim() == '' || workHistory.position.trim() == '' || workHistory.startDate.toString().trim() == '' || workHistory.endDate.toString().trim() == '') {
				x++;
			}
		});
		if(x == 0) $scope.applicant.workHistories.push({ company: '', position: '', startDate: '', endDate: '' });
	};

	$scope.deleteWorkHistoryField = function() {
		if($scope.applicant.workHistories.length > 1) {
			var lastIndex = $scope.applicant.workHistories.length - 1;
			$scope.applicant.workHistories.splice(lastIndex, 1);
		}
	};

	$scope.noWorkHistoryToggle = function(noWorkHistory) {
		if(noWorkHistory == true) {
			$scope.applicant.workHistories = [];
		} else {
			$scope.applicant.workHistories = [
				{ company: '', position: '', startDate: '', endDate: '' }
			];
		}
	};



	$scope.addBeneficiaryField = function() {
		var x = 0;
		angular.forEach($scope.applicant.beneficiaries, function(beneficiary, key) {
			if(beneficiary.name.trim() == '' || beneficiary.birthDate.toString().trim() == '' || beneficiary.relation.trim() == '')
				x++;
		});
		if(x == 0 && $scope.applicant.beneficiaries.length < 3) $scope.applicant.beneficiaries.push({ name: '', birthDate: '', relation: 'Mother' });
	};

	$scope.deleteBeneficiaryField = function() {
		if($scope.applicant.beneficiaries.length > 1) {
			var lastIndex = $scope.applicant.beneficiaries.length - 1;
			$scope.applicant.beneficiaries.splice(lastIndex, 1);
		}
	};



	$scope.selectPicture = function(files) {
		// start file validation here
		if(files && files.length) {
			var pictureFile = files[0];
			var acceptedFiletypes = ['png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG'];
			var sizeLimit = 512000; // 500KB
			var fileType = pictureFile.name.split('.').pop();
			$scope.pictureFileError = '';

			if(!utilityFunctions.inArray(fileType, acceptedFiletypes)) {
				$scope.pictureFileError = 'Not a valid image!';
			}
			if(pictureFile.size > sizeLimit) {
				$scope.pictureFileError = 'Picture file must be 500KB and below!';
			}
			if($scope.pictureFileError == '') {
				$scope.pictureFile = pictureFile;
			} else {
				alert($scope.pictureFileError);
			}
		}
	};



	$scope.uploadPicture = function(callback) {
		$scope.loadingProperties.message = 'Uploading photo...';
		$scope.loadingProperties.promise = Upload.upload({
			url: DOMAIN+'applicant/uploadProfilePicture',
			method: 'POST',
			file: $scope.pictureFile
		}).then(function (response) {
			// success
			// console.log('Success ' + response.config.data.file.name + 'uploaded. Response: ' + response.data);
			console.log(response);
			$scope.applicant.photo = response.data.newFileName;
			if(callback) callback();
		}, function (response) {
			// error
			console.log(response);
			// console.log('Error status: ' + response.status);
		}, function (evt) {
			var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			console.log('progress: ' + progressPercentage + '% ');
		});;
	}



	$scope.submitForm = function() {
		$scope.uploadPicture(function() {
			console.log($scope.applicant);
			$scope.loadingProperties.message = 'Submitting application form...';
			$scope.loadingProperties.promise = $http.post(httpApplicant.create, { info: $scope.applicant })
			.success(function(data, status) {
				alert('Merbership form submitted successfully!');
				$state.go('guestHome');
			});
		});
	};




}]);