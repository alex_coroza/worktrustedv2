angular.module('applyMembershipModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('applyMembershipForm', {
			url: '/apply-membership-form',
			views: {
				'': {
					templateUrl: 'app/modules/guest/apply-membership/views/apply-membership-form.view.html',
					controller: 'applyMembershipFormController'
				},
				'navbar@applyMembershipForm': {
					templateUrl: 'app/modules/guest/apply-membership/views/apply-membership-form/navbar.view.html',
					controller: 'applyMembershipNavbarController'
				},
				'instructions@applyMembershipForm': {
					templateUrl: 'app/modules/guest/apply-membership/views/apply-membership-form/instructions.view.html'
				},
				'form1@applyMembershipForm': {
					templateUrl: 'app/modules/guest/apply-membership/views/apply-membership-form/form1.view.html'
				},
				'form2@applyMembershipForm': {
					templateUrl: 'app/modules/guest/apply-membership/views/apply-membership-form/form2.view.html'
				},
				'form3@applyMembershipForm': {
					templateUrl: 'app/modules/guest/apply-membership/views/apply-membership-form/form3.view.html'
				}
			}
		})
	; /*closing $stateProvider*/
}]);

				