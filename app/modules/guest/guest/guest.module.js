var DOMAIN = '/worktrustedV2/';
var APPPATH = DOMAIN+'app/';

angular.module('guestModule', [
	'ngMessages', 'ngAnimate', 'ui.router', 'ui.bootstrap', 'mgcrea.ngStrap.scrollspy',
	'ngAside', // extensions for ui.bootstrap
	'ngFileUpload', 'cgBusy',
	'http.service', 'utility.service',
	'guestHomeModule', 'applyMembershipModule', 'loginModule'
]);




// angular-busy override settings
angular.module('guestModule')
.value('cgBusyDefaults', {
	message:'Loading...',
	backdrop: true,
	// templateUrl: 'my_custom_template.html',
	delay: 0,
	minDuration: 1000,
	// wrapperClass: 'my-class my-class2'
});
