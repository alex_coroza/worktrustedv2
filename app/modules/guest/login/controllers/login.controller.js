angular.module('loginModule')
.controller('loginController', ['$scope', '$http', 'regexPattern', '$uibModalInstance', 'httpAccount', function($scope, $http, regexPattern, $uibModalInstance, httpAccount) {
	console.log('loginController initialized!');


	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();

	$scope.loadingProperties = {
		promise: null,
	};

	$scope.userName = '';
	$scope.password = '';
	$scope.loginType = 'memberLogin';


	$scope.loginTypeStatusChange = function(loginTypeStatus) {
		if(loginTypeStatus) {
			$scope.loginType = 'companyLogin';
		} else {
			$scope.loginType = 'memberLogin';
		}
	};



	$scope.login = function() {
		$scope.loadingProperties.message = 'Logging in...';
		$scope.loadingProperties.promise = $http.post(httpAccount.login, { userName: $scope.userName, password: $scope.password, loginType: $scope.loginType })
		.success(function(data, status) {
			if(data.message == 'success') {
				if($scope.loginType == 'companyLogin') {
					window.location = DOMAIN+'companyPage';
				} else {
					if(data.type == 'admin') window.location = DOMAIN+'adminPage';
					if(data.type == 'member') window.location = DOMAIN+'memberPage';
					if(data.type == 'lineLeader') window.location = DOMAIN+'lineLeaderPage';
				}
			} else {
				alert('User name or password is incorrect!');
			}
		});
	};


	$scope.dismiss = function() {
		$uibModalInstance.close();
	};


}]);