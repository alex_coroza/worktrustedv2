angular.module('guestHomeModule')
.controller('navbarAsideController', ['$scope', '$location', '$anchorScroll', '$uibModalInstance', 'activeLink',  function($scope, $location, $anchorScroll, $uibModalInstance, activeLink) {
	console.log('navbarAsideController initialized!');

	$scope.activeLink = activeLink;


	$scope.changeActiveLink = function(linkName) {
		$scope.activeLink = linkName;
		$location.hash(linkName);
		$anchorScroll();
		$scope.dismiss(linkName);
	};


	$scope.dismiss = function(activeLink) {
		$uibModalInstance.close(activeLink);
	};

}]);