angular.module('guestHomeModule')
.controller('contactUsController', ['$scope', '$http', '$state', 'httpFeedback', 'regexPattern', function($scope, $http, $state, httpFeedback, regexPattern) {
	console.log('contactUsController initialized!');


	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();

	$scope.loadingProperties = {
		promise: null,
	};

	$scope.feedback = {};
	$scope.sent = false;


	$scope.submitFeedback = function() {
		$scope.loadingProperties.message = 'Submitting feedback...';
		$scope.loadingProperties.promise = $http.post(httpFeedback.create, { info: $scope.feedback })
		.success(function(data, status) {
			$scope.sent = true;
			alert('Feedback sent!');
		});
	};



}]);