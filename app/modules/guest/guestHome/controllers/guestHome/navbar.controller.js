angular.module('guestHomeModule')
.controller('navbarController', ['$scope', '$state', '$animate', '$location', '$anchorScroll', '$uibModal', '$aside', function($scope, $state, $animate, $location, $anchorScroll, $uibModal, $aside) {
	console.log('navbarController initialized!');

	$scope.activeLink = 'home';


	$scope.changeActiveLink = function(linkName) {
		$scope.activeLink = linkName;
		$location.hash(linkName);
		$anchorScroll();
	};


	$scope.showNavbarAside = function() {
		var asideInstance = $aside.open({
			templateUrl: APPPATH+'modules/guest/guestHome/views/guestHome/navbar/sideNavbar.view.html',
			placement: 'right',
			size: 'md',
			controller: 'navbarAsideController',
			resolve: {
				activeLink: function() {
					return $scope.activeLink;
				}
			}
		});

		asideInstance.result.then(function(newActiveLink) {
			$scope.activeLink = newActiveLink;
		});
	};



	$scope.showLogin = function() {
		var modalInstance = $uibModal.open({
			templateUrl: APPPATH+'modules/guest/login/views/login.view.html',
			controller: 'loginController'
		});
	};



}]);