angular.module('guestHomeModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('guestHome', {
			url: '/home',
			views: {
				'': {
					templateUrl: 'app/modules/guest/guestHome/views/guestHome.view.html',
					controller: 'guestHomeController'
				},
				'navbar@guestHome': {
					templateUrl: 'app/modules/guest/guestHome/views/guestHome/navbar.view.html',
					controller: 'navbarController'
				},
				'default@guestHome': {
					templateUrl: 'app/modules/guest/guestHome/views/guestHome/default.view.html'
				},
				'ourFocus@guestHome': {
					templateUrl: 'app/modules/guest/guestHome/views/guestHome/ourFocus.view.html'
				},
				'services@guestHome': {
					templateUrl: 'app/modules/guest/guestHome/views/guestHome/services.view.html'
				},
				'contactUs@guestHome': {
					templateUrl: 'app/modules/guest/guestHome/views/guestHome/contactUs.view.html',
					controller: 'contactUsController'
				},
				'footer@guestHome': {
					templateUrl: 'app/modules/guest/guestHome/views/guestHome/footer.view.html'
				}
			}
		})
	; /*$stateProvider closing*/
}]);
