angular.module('profileModule')
.controller('profileInfoController', ['$scope', '$rootScope', '$http', '$state', '$mdDialog', '$mdToast', 'httpCompany', 'httpAccount', 'utilityFunctions', 'user', function($scope, $rootScope, $http, $state, $mdDialog, $mdToast, httpCompany, httpAccount, utilityFunctions, user) {
	console.log('profileInfoController initialized!');

	$rootScope.$emit('activeLinkValueUpdate', { value: 'profile' });
	
	$scope.user = user;

	$scope.loadingProperties = {
		promise: null,
	};	



	// fetch companyInfo
	var populate = {
		0: 'account'
	};


	$scope.loadingProperties.message = 'Loading company profile info...';
	$scope.loadingProperties.promise = $http.post(httpCompany.read, { id: $scope.user.companyId, populate: populate })
	.success(function(data, status) {
		$scope.company = data;
	});




	$scope.accountEdit = function() {
		$state.go('company.profileAccountEdit');
	};





}]);