angular.module('profileModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('company.profileInfo', {
			url: 'profile/',
			templateUrl: DOMAIN+'app/modules/company/profile/views/profileInfo.view.html',
			controller: 'profileInfoController'
		})
		.state('company.profileAccountEdit', {
			url: 'profile-account-edit/',
			templateUrl: DOMAIN+'app/modules/company/profile/views/accountEdit.view.html',
			controller: 'accountEditController'
		})
	; /*closing $stateProvider*/
}]);

				