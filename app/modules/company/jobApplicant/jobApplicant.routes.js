angular.module('jobApplicantModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('company.jobApplicantList', {
			url: 'job-applicants/',
			templateUrl: DOMAIN+'app/modules/company/jobApplicant/views/jobApplicantList.view.html',
			controller: 'jobApplicantListController'
		})
		.state('company.jobApplicantInfo', {
			url: 'job-applicant-info/{id}',
			templateUrl: DOMAIN+'app/modules/company/jobApplicant/views/jobApplicantInfo.view.html',
			controller: 'jobApplicantInfoController'
		})
	; /*closing $stateProvider*/
}]);

