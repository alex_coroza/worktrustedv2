angular.module('jobApplicantModule')
.controller('jobApplicantListController', ['$scope', '$rootScope', '$http', '$timeout', '$state', '$mdDialog', '$mdToast', 'httpJob', 'utilityFunctions', 'user', function($scope, $rootScope, $http, $timeout, $state, $mdDialog, $mdToast, httpJob, utilityFunctions, user) {
	console.log('jobApplicantListController initialized!');

	$rootScope.$emit('activeLinkValueUpdate', { value: 'jobApplicant' });

	$scope.user = user;
	$scope.jobApplicants = [];

	$scope.filters = {
		companyId: $scope.user.company.id,
		status: 'All',
		search: '',
		jobOpeningId: 'All',
		populate: {
			jobOpening: {},
			member: {}
		}
	};

	$scope.loadingProperties = {
		promise: null,
	};



	$scope.reloadData = function() {
		$scope.loadJobApplicants();
	};



	$scope.loadJobApplicants = function() {
		var processedFilters = angular.copy($scope.filters);
		if(processedFilters.status == 'All') delete processedFilters.status;
		if(processedFilters.jobOpeningId == 'All') delete processedFilters.jobOpeningId;

		$scope.loadingProperties.message = 'Loading job applicants...';
		$scope.loadingProperties.promise = $http.post(httpJob.enlistJobApplicants, processedFilters)
		.success(function(data ,status) {
			$scope.jobApplicants = data;
		});
	};




	$scope.jobApplicantInfo = function(info) {
		$state.go('company.jobApplicantInfo', { id: info.id });
	};




	// update list based on search
	var searchDelay;
	$scope.searchChange = function() {
		// fetch new records
		if(searchDelay) $timeout.cancel(searchDelay);

		searchDelay = $timeout(function() {
			$scope.loadJobApplicants();
		}, 1000);
	};





	// display advanced search dialog
	$scope.showAdvancedSearch = function() {
		$mdDialog.show({
			templateUrl: DOMAIN+'app/modules/company/jobApplicant/views/jobApplicantList/advancedSearch.tpl.html',
			controller: 'advancedSearchController',
			locals: { filters: $scope.filters }
		})
		.then(function(response) {
			$scope.filters = response;
			$scope.loadJobApplicants();
		});
	};










	// fetch all members on page load
	$scope.loadJobApplicants();





}]);