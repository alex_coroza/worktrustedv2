angular.module('jobApplicantModule')
.controller('jobApplicantInfoController', ['$q', '$scope', '$rootScope', '$http', '$state', '$mdDialog', '$mdToast', 'httpJob', 'httpMember', 'utilityFunctions', 'user', function($q, $scope, $rootScope, $http, $state, $mdDialog, $mdToast, httpJob, httpMember, utilityFunctions, user) {
	console.log('jobApplicantInfoController initialized!');

	$rootScope.$emit('activeLinkValueUpdate', { value: 'jobApplicant' });
	
	$scope.user = user;

	$scope.loadingProperties = {
		promise: null,
	};	



	$scope.member = {};
	$scope.jobOpening = {};



	// fetch memberInfo
	var populate = {
		jobOpening: {
			company: {}
		},
		member: {
			beneficiaries: {}, 
			characterReferences: {}, 
			education: {}, 
			parent: {}, 
			skills: {}, 
			spouse: {}, 
			workHistories: {}
		}
	};


	$scope.loadingProperties.message = 'Loading job application details...';
	$scope.loadingProperties.promise = $http.post(httpJob.readJobApplicant, { id: $state.params.id, populate: populate })
	.success(function(data, status) {
		// designate and separate data
		$scope.member = data.member;
		$scope.jobOpening = data.jobOpening;
		$scope.jobApplicant = angular.copy(data);
		delete $scope.jobApplicant.member;
		delete $scope.jobApplicant.jobOpening;
		
		// empty data if applicant is not applying in any of the company's job openings
		if($scope.jobOpening.companyId != $scope.user.company.id) {
			$scope.member = {};
			$scope.jobOpening = {};
			$scope.jobApplicant = {};
		}
	});


	

	$scope.backToJobApplicantList = function() {
		$state.go('company.jobApplicantList');
	};





	$scope.hireApplicant = function() {
		// check if job opening still has slots available
		if(parseInt($scope.jobOpening.slots) < 1) {
			var toast = $mdToast.simple()
				.textContent('No slots available for this job opening!')
				.position('bottom right');
			$mdToast.show(toast);
		} else {
			// change/update job applicant status
			$scope.jobApplicant.status = 'Hired';
			$scope.loadingProperties.message = 'Updating applicant status...';
			$scope.loadingProperties.promise = $http.post(httpJob.saveJobApplicant, { info: $scope.jobApplicant })
			.then(function(response) {
				// update job opening's available slots
				$scope.jobOpening.slots = parseInt($scope.jobOpening.slots) - 1;
				$scope.loadingProperties.message = 'Updating job opening\'s available slots...';
				return $scope.loadingProperties.promise = $http.post(httpJob.saveJobOpening, { info: $scope.jobOpening });
			}).then(function(response) {
				// update member's work history
				$scope.member.workHistories.push({
					company: $scope.jobOpening.company.name,
					position: $scope.jobOpening.title,
					startDate: new Date()
				});
				$scope.loadingProperties.message = 'Updating applicant\'s work history...';
				return $scope.loadingProperties.promise = $http.post(httpMember.update, { info: $scope.member });
			}).then(function(response) {
				var toast = $mdToast.simple()
					.textContent($scope.member.firstName+' has been hired as '+$scope.jobOpening.title+'.')
					.position('bottom right');
				$mdToast.show(toast);
				$scope.backToJobApplicantList();
			}).catch(function(errorMessage) {
				var toast = $mdToast.simple()
					.textContent(errorMessage)
					.position('bottom right');
				$mdToast.show(toast);
			});
		}
	};





	$scope.hireApplicantConfirm = function() {
		var confirm = $mdDialog.confirm()
			.title('Hire applicant?')
			.textContent('This will accept/hire '+$scope.member.firstName+' for "'+$scope.jobOpening.title+'".')
			.ariaLabel('Lucky day')
			.ok('OK!')
			.cancel('Cancel');
		$mdDialog.show(confirm).then(function() {
			$scope.hireApplicant();
		}, function() {
			// cancel, no action
		});
	};






}]);