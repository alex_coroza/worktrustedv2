angular.module('jobApplicantModule')
.controller('advancedSearchController', ['$scope', '$rootScope', '$http', '$state', '$mdDialog', '$mdToast', 'httpJob', 'utilityFunctions', 'formUtilities', 'regexPattern', 'filters', function($scope, $rootScope, $http, $state, $mdDialog, $mdToast, httpJob, utilityFunctions, formUtilities, regexPattern, filters) {
	console.log('advancedSearchController initialized!');

	$scope.filters = angular.copy(filters);

	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();

	$scope.statusOptions = ['All', 'Pending', 'Agency Interview', 'Company Interview', 'Hired'];
	$scope.jobOpenings = [];

	$scope.loadingProperties = {
		promise: null
	};



	$scope.loadJobOpenings = function() {
		$scope.loadingProperties.message = 'Loading job openings...';
		$scope.loadingProperties.promise = $http.post(httpJob.enlistJobOpenings, { 'companyId': $scope.filters.companyId, 'status': 'Posted' })
		.success(function(data, status) {
			$scope.jobOpenings = data;
		});
	};



	$scope.clearFilters = function() {
		$scope.filters.search = '';
		$scope.filters.status = 'All';
		$scope.filters.jobOpening = 'All';
	};



	$scope.dismiss = function() {
		$mdDialog.cancel();
	};



	$scope.search = function(form) {
		formUtilities.formErrorChecker(form);

		if(!utilityFunctions.isEmptyObject(form.$error)) {
			var toast = $mdToast.simple()
				.textContent('Errors detected!')
				.position('bottom right');
			$mdToast.show(toast);
		} else {
			$mdDialog.hide($scope.filters);
		}
	};






	$scope.loadJobOpenings();



}]);