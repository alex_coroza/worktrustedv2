angular.module('companyModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.when('', '/');
	$stateProvider
		.state('notFound', {
			url: '/fucking-page-not-found',
			templateUrl: 'app/modules/core/views/custom404.view.html'
		})
		.state('company', {
			url: '/',
			views: {
				'': {
					templateUrl: DOMAIN+'app/modules/company/company/views/company.view.html',
					controller: 'companyController'
				},
				'sideNav@company': {
					templateUrl: DOMAIN+'app/modules/company/company/views/company/sideNav.view.html',
					controller: 'sideNavController'
				},
				'content@company': {
					templateUrl: DOMAIN+'app/modules/company/company/views/company/content.view.html'
				}
			},
			resolve: {
				user: ['$q', '$http', 'httpAccount', function($q, $http, httpAccount) {
					var deferred = $q.defer();
					$http.post(httpAccount.fetchLoggedUser, { type: 'company' })
					.success(function(data, status) {
						deferred.resolve(data);
					});
					return deferred.promise;
				}]
			}
		})
	; /*$stateProvider closing*/
}]);
