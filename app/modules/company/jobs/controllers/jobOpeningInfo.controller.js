angular.module('jobsModule')
.controller('jobOpeningInfoController', ['$scope', '$rootScope', '$http', '$timeout', '$state', '$mdDialog', '$mdToast', 'httpJob', 'utilityFunctions', 'user', function($scope, $rootScope, $http, $timeout, $state, $mdDialog, $mdToast, httpJob, utilityFunctions, user) {
	console.log('jobOpeningInfoController initialized!');

	$rootScope.$emit('activeLinkValueUpdate', { value: 'jobs' });

	$scope.user = user;
	$scope.jobOpening;

	$scope.loadingProperties = {
		promise: null,
	};


	$scope.loadJobOpenings = function() {
		var readOptions = {
			id: $state.params.id, 
			companyId: $scope.user.company.id,
			populate: {
				applicants: {
					member: {}
				}
			}
		};

		$scope.loadingProperties.message = 'Loading job opening\'s info...';
		$scope.loadingProperties.promise = $http.post(httpJob.readJobOpening, readOptions)
		.success(function(data, status) {
			$scope.jobOpening = data;
		});
	};




	$scope.backToJobOpeningList = function() {
		$state.go('company.jobOpeningList');
	};



	$scope.applicantInfo = function(applicant) {
		$state.go('company.jobApplicantInfo', { id: applicant.id });
	};



	$scope.jobOpeningEdit = function() {
		$state.go('company.jobOpeningEdit', { id: $scope.jobOpening.id });
	};








	// fetch all members on page load
	$scope.loadJobOpenings();





}]);