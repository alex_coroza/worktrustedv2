angular.module('jobsModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('company.jobOpeningList', {
			url: 'job-openings/',
			templateUrl: DOMAIN+'app/modules/company/jobs/views/jobOpeningList.view.html',
			controller: 'jobOpeningListController'
		})
		.state('company.addJobOpening', {
			url: 'add-job-opening/',
			templateUrl: DOMAIN+'app/modules/company/jobs/views/addJobOpening.view.html',
			controller: 'addJobOpeningController'
		})
		.state('company.jobOpeningInfo', {
			url: 'job-opening-info/{id}',
			templateUrl: DOMAIN+'app/modules/company/jobs/views/jobOpeningInfo.view.html',
			controller: 'jobOpeningInfoController'
		})
		.state('company.jobOpeningEdit', {
			url: 'job-opening-edit/{id}',
			templateUrl: DOMAIN+'app/modules/company/jobs/views/jobOpeningEdit.view.html',
			controller: 'jobOpeningEditController'
		})
	; /*closing $stateProvider*/
}]);

