angular.module('lineLeaderModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.when('', '/');
	$stateProvider
		.state('notFound', {
			url: '/fucking-page-not-found',
			templateUrl: 'app/modules/core/views/custom404.view.html'
		})
		.state('lineLeader', {
			url: '/',
			views: {
				'': {
					templateUrl: DOMAIN+'app/modules/lineLeader/lineLeader/views/lineLeader.view.html',
					controller: 'lineLeaderController'
				},
				'sideNav@lineLeader': {
					templateUrl: DOMAIN+'app/modules/lineLeader/lineLeader/views/lineLeader/sideNav.view.html',
					controller: 'sideNavController'
				},
				'content@lineLeader': {
					templateUrl: DOMAIN+'app/modules/lineLeader/lineLeader/views/lineLeader/content.view.html'
				}
			},
			resolve: {
				user: ['$q', '$http', 'httpAccount', function($q, $http, httpAccount) {
					var deferred = $q.defer();
					$http.post(httpAccount.fetchLoggedUser, { type: 'member' })
					.success(function(data, status) {
						deferred.resolve(data);
					});
					return deferred.promise;
				}]
			}
		})
	; /*$stateProvider closing*/
}]);
