angular.module('profileModule')
.controller('accountEditController', ['$q', '$scope', '$rootScope', '$http', '$timeout', '$state', '$mdDialog', '$mdToast', 'httpMember', 'httpAccount', 'regexPattern', 'utilityFunctions', 'formUtilities', 'user', function($q, $scope, $rootScope, $http, $timeout, $state, $mdDialog, $mdToast, httpMember, httpAccount, regexPattern, utilityFunctions, formUtilities, user) {
	console.log('accountEditController initialized!');

	$rootScope.$emit('activeLinkValueUpdate', { value: 'profile' });

	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();
	
	$scope.user = user;
	$scope.accountInfo = angular.copy($scope.user);
	$scope.accountInfo.newUserName = angular.copy($scope.user.userName);

	$scope.dontChangePassword = false;

	$scope.loadingProperties = {
		promise: null,
	};	







	$scope.checkUserName = function() {
		$scope.loadingProperties.message = 'Checking username availability...';
		$scope.loadingProperties.promise = $http.post(httpAccount.checkUserName, { type: 'member', userName: $scope.accountInfo.newUserName })
		.then(function(response) {
			if(response.data == 0 || $scope.accountInfo.userName == $scope.accountInfo.newUserName) {
				formUtilities.inputErrorToggle($scope.accountEditForm, 'newUserName', 'alreadyTaken', 'errorOff');
			} else {
				formUtilities.inputErrorToggle($scope.accountEditForm, 'newUserName', 'alreadyTaken', 'errorOn');
				var toast = $mdToast.simple()
					.textContent('Username already taken!')
					.position('bottom right');
				$mdToast.show(toast);
			}
		});
	};




	$scope.confirmPasswordCheck = function() {
		if($scope.accountInfo.password == $scope.accountInfo.confirmPassword) {
			formUtilities.inputErrorToggle($scope.accountEditForm, 'confirmPassword', 'passwordNotMatch', 'errorOff');
		} else {
			formUtilities.inputErrorToggle($scope.accountEditForm, 'confirmPassword', 'passwordNotMatch', 'errorOn');
		}
	};




	$scope.backToProfileInfo = function() {
		$state.go('admin.profileInfo');
	};




	// check userName availability
	var searchDelay;
	$scope.userNameCheck = function() {
		if(searchDelay) $timeout.cancel(searchDelay);

		if($scope.accountInfo.userName)
		searchDelay = $timeout(function() {
			$scope.checkUserName();
		}, 1500);
	};


	// check password
	var searchDelay;
	$scope.passwordCheck = function() {
		if(searchDelay) $timeout.cancel(searchDelay);

		if($scope.accountInfo.password)
		searchDelay = $timeout(function() {
			$scope.checkPassword();
		}, 1500);
	};






	// save data to database
	// check userName >> save member data >> create account
	$scope.saveAccountInfo = function() {
		// confirm password check
		if($scope.accountInfo.password == $scope.accountInfo.confirmPassword) {
			if($scope.dontChangePassword == true) delete $scope.accountInfo.password;

			// check userName
			$scope.loadingProperties.message = 'Checking username availability...';
			$scope.loadingProperties.promise = $http.post(httpAccount.checkUserName, { type: 'member', userName: $scope.accountInfo.newUserName })
			.then(function(response) {
				if(response.data == 0 || $scope.accountInfo.userName == $scope.accountInfo.newUserName) {
					// save changes
					$scope.loadingProperties.message = 'Saving changes...';
					return $scope.loadingProperties.promise = $http.post(httpAccount.save, { type: 'member', info: $scope.accountInfo });
				} else {
					return $q.reject('UserName already taken!');
				}
			}).then(function(response) {
				// actions for "save changes"
				var toast = $mdToast.simple()
					.textContent('Changes saved!')
					.position('bottom right');
				$mdToast.show(toast);
				
				// logout
				$scope.loadingProperties.message = 'Logging out...';
				return $scope.loadingProperties.promise = $http.post(httpAccount.logout)
			}).then(function(response) {
				// actions for "logout"
				window.location.href = DOMAIN+'guest';
			}).catch(function(errorMessage) {
				// jump here if any $q.reject() is found
				var toast = $mdToast.simple()
					.textContent(errorMessage)
					.position('bottom right');
				$mdToast.show(toast);
			});
		}
	};




	$scope.submitAccountEditForm = function(form) {
		// check for form errors first
		formUtilities.formErrorChecker(form);

		// alert error message if errors detected
		// else submit the form
		if(!utilityFunctions.isEmptyObject(form.$error)) {
			var alert = $mdDialog.alert()
				.title('Errors detected!')
				.textContent('See and fix the errors to save changes!')
				.ok('Close')
			$mdDialog.show(alert);
		} else {
			$scope.saveAccountInfo();
		}
	};




}]);