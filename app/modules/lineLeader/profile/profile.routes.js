angular.module('profileModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('lineLeader.profileInfo', {
			url: 'profile/',
			templateUrl: DOMAIN+'app/modules/lineLeader/profile/views/profileInfo.view.html',
			controller: 'profileInfoController'
		})
		.state('lineLeader.profileAccountEdit', {
			url: 'profile-account-edit/',
			templateUrl: DOMAIN+'app/modules/lineLeader/profile/views/accountEdit.view.html',
			controller: 'accountEditController'
		})
	; /*closing $stateProvider*/
}]);

				