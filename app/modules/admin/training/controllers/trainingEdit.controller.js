angular.module('trainingModule')
.controller('trainingEditController', ['$scope', '$rootScope', '$http', '$state', '$mdDialog', '$mdToast', 'httpTraining', 'utilityFunctions', 'schedulingServices', 'regexPattern', function($scope, $rootScope, $http, $state, $mdDialog, $mdToast, httpTraining, utilityFunctions, schedulingServices, regexPattern) {
	console.log('trainingEditController initialized!');

	$rootScope.$emit('activeLinkValueUpdate', { value: 'trainings' });
	
	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();

	$scope.loadingProperties = {
		promise: null,
	};



	// fetch trainingInfo
	var populate = {
		'trainingEvents': {
			'attendees': {
				0: 'member'
			}
		}
	};
	$scope.loadingProperties.message = 'Loading training\'s info...';
	$scope.loadingProperties.promise = $http.post(httpTraining.read, { id: $state.params.id, populate: populate })
	.success(function(data, status) {
		$scope.training = data;
		$scope.countTrainingEventTypes();
	});



	// counts the number of upcoming/done/ongoing events using a specifuc training
	// also set training event title and type
	$scope.countTrainingEventTypes = function() {
		var count = { upcoming: 0, done: 0, ongoing: 0 };

		angular.forEach($scope.training.trainingEvents, function(trainingEvent, key) {
			trainingEvent.title = $scope.training.name;
			trainingEvent.type = 'training';
			var start = moment(trainingEvent.start).unix();
			var end = moment(trainingEvent.end).unix();
			var now = moment().unix();

			if(end < now) { /*upcoming*/
				count.upcoming++;
			} else if(start > now) { /*done*/
				count.done++;
			} else if(now >= start && now <= end) { /*ongoing*/
				count.ongoing++;
			}
		});

		$scope.training.trainingEventCount = count;
	};

	

	$scope.backToTrainingInfo = function() {
		$state.go('admin.trainingInfo', { id: $scope.training.id });
	};


	




	$scope.trainingEventInfo = function(trainingEvent) {
		trainingEvent.start = moment(trainingEvent.start);
		trainingEvent.end = moment(trainingEvent.end);
		trainingEvent.type = 'training';
		schedulingServices.showScheduleInfo(trainingEvent).then(function(response) {
			if(response == 'deleted') {
				$state.go($state.current, {}, {reload: true});
			}
		});
	};




	$scope.addTrainingEvent = function() {
		schedulingServices.showAddSchedule().then(function(response) {
			if(response == 'formSubmitted') {
				$state.go($state.current, {}, {reload: true});
			}
		});
	};




	$scope.cancelEditConfirm = function() {
		var confirm = $mdDialog.confirm()
			.title('Cancel edit?')
			.textContent('This action will discard all unsaved changes!')
			.ok('Proceed')
			.cancel('Close');
		$mdDialog.show(confirm).then(function() {
			$scope.backToTrainingInfo();
		});
	};





	$scope.submitTrainingEditForm = function(form) {
		// check for errors first
		// set all fields to "touched"
		angular.forEach(form.$error, function(field) {
			angular.forEach(field, function(errorField) {
				errorField.$setTouched();
			});
		});

		// alert error message if errors detected
		// else submit the form
		if(!utilityFunctions.isEmptyObject(form.$error)) {
			var alert = $mdDialog.alert()
				.title('Errors detected!')
				.textContent('See and fix the errors to save changes!')
				.ok('Close')
			$mdDialog.show(alert);
		} else {
			$scope.loadingProperties.message = 'Saving changes...';
			$scope.loadingProperties.promise = $http.post(httpTraining.update, { info: $scope.training })
			.success(function(data ,status) {
				var toast = $mdToast.simple()
					.textContent('Changes Saved!')
					.position('bottom right');
				$mdToast.show(toast);
				$state.go('admin.trainingInfo', { id: $scope.training.id });
			});
		}
	}






}]);