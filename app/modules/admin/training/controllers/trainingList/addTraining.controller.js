angular.module('trainingModule')
.controller('addTrainingController', ['$scope', '$rootScope', '$http', '$compile', '$state', '$mdDialog', '$mdToast', 'regexPattern', 'utilityFunctions', 'httpTraining', 'trainingServices', function($scope, $rootScope, $http, $compile, $state, $mdDialog, $mdToast, regexPattern, utilityFunctions, httpTraining, trainingServices) {
	console.log('addTrainingController initialized!');


	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();

	$scope.loadingProperties = {
		promise: null,
	};











	$scope.submitForm = function(form) {
		// check for errors first
		// set all fields to "touched"
		angular.forEach(form.$error, function(field) {
			angular.forEach(field, function(errorField) {
				errorField.$setTouched();
			});
		});

		// alert error message if errors detected
		// else submit the form
		if(!utilityFunctions.isEmptyObject(form.$error)) {
			var toast = $mdToast.simple()
				.textContent('Errors Detected! See and fix the errors to save changes!')
				.position('bottom right');
			$mdToast.show(toast);
		} else {
			$scope.loadingProperties.message = 'Saving changes...';
			$scope.loadingProperties.promise = $http.post(httpTraining.create, { info: $scope.training })
			.success(function(data, status) {
				var toast = $mdToast.simple()
					.textContent('Changes Saved!')
					.position('bottom right')
					.action('View');
				$mdToast.show(toast).then(function(response) {
					if(response == 'ok') $state.go('admin.trainingInfo', { id: data });
				});
				$mdDialog.hide();
			});
		}
	}





	$scope.dismiss = function() {
		$mdDialog.cancel();
	};


}]);