angular.module('trainingModule')
.controller('trainingInfoController', ['$scope', '$rootScope', '$http', '$state', '$mdDialog', '$mdToast', 'httpTraining', 'utilityFunctions', 'schedulingServices', function($scope, $rootScope, $http, $state, $mdDialog, $mdToast, httpTraining, utilityFunctions, schedulingServices) {
	console.log('trainingInfoController initialized!');

	$rootScope.$emit('activeLinkValueUpdate', { value: 'trainings' });
	

	$scope.loadingProperties = {
		promise: null,
	};

	



	// fetch trainingInfo
	var populate = {
		'trainingEvents': {
			'attendees': {
				0: 'member'
			}
		}
	};

	$scope.loadTrainingInfo = function() {
		$scope.loadingProperties.message = 'Loading training\'s info...';
		$scope.loadingProperties.promise = $http.post(httpTraining.read, { id: $state.params.id, populate: populate })
		.success(function(data, status) {
			$scope.training = data;
			$scope.countTrainingEventTypes();
		});
	};



	// counts the number of upcoming/done/ongoing events using a specific training
	// also set training event title
	$scope.countTrainingEventTypes = function() {
		var count = { upcoming: 0, done: 0, ongoing: 0 };

		angular.forEach($scope.training.trainingEvents, function(trainingEvent, key) {
			trainingEvent.title = $scope.training.name;
			trainingEvent.type = 'training';
			var start = moment(trainingEvent.start).unix();
			var end = moment(trainingEvent.end).unix();
			var now = moment().unix();

			if(end < now) { /*upcoming*/
				count.upcoming++;
			} else if(start > now) { /*done*/
				count.done++;
			} else if(now >= start && now <= end) { /*ongoing*/
				count.ongoing++;
			}
		});

		$scope.training.trainingEventCount = count;
	};

	

	$scope.backToTrainingList = function() {
		$state.go('admin.trainingList');
	};




	$scope.editTraining = function() {
		$state.go('admin.trainingEdit', { id: $scope.training.id });
	};




	$scope.deleteTrainingConfirm = function() {
		var confirm = $mdDialog.confirm()
			.title('Delete this training?')
			.textContent('All training events(also its attendees) that are associated with this training will also be deleted!')
			.ok('Proceed')
			.cancel('Close');
		$mdDialog.show(confirm).then(function() {
			$scope.deleteTraining();
		});
	};




	$scope.deleteTraining = function() {
		$scope.loadingProperties.message = 'Deleting training...';
		$scope.loadingProperties.promise = $http.post(httpTraining.permanentDelete, { id: $scope.training.id })
		.success(function(data, status) {
			$scope.backToTrainingList();
		});
	};





	$scope.trainingEventInfo = function(trainingEvent) {
		trainingEvent.start = moment(trainingEvent.start);
		trainingEvent.end = moment(trainingEvent.end);
		trainingEvent.type = 'training';
		schedulingServices.showScheduleInfo(trainingEvent).then(function(response) {
			if(response == 'deleted') {
				$scope.loadTrainingInfo();
			}
		});
	};




	$scope.addTrainingEvent = function() {
		schedulingServices.showAddSchedule().then(function(response) {
			if(response == 'formSubmitted') {
				$scope.loadTrainingInfo();
			}
		});
	};




	$scope.loadTrainingInfo();


}]);