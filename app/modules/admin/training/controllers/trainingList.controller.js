angular.module('trainingModule')
.controller('trainingListController', ['$scope', '$rootScope', '$timeout', '$http', '$state', '$stateParams', '$mdDialog', 'httpTraining', 'trainingServices', function($scope, $rootScope, $timeout, $http, $state, $stateParams, $mdDialog, httpTraining, trainingServices) {
	console.log('trainingListController initialized!');

	$rootScope.$emit('activeLinkValueUpdate', { value: 'trainings' });


	$scope.searchKeyword = '';
	$scope.trainings = [];


	// for loading indicator
	$scope.loadingProperties = {
		promise: null,
	};

	

	// populate value for fetching members
	var populate = {
		'trainingEvents': {
			'attendees': {
				0: 'member'
			}
		}
	};



	$scope.reloadData = function() {
		$scope.loadTrainingList();
	};



	$scope.trainingInfo = function(info) {
		$state.go('admin.trainingInfo', { id: info.id });
	};




	// counts the number of upcoming/done/ongoing events using a specifuc training
	$scope.countTrainingEventTypes = function(trainingIndex) {
		var count = { upcoming: 0, done: 0, ongoing: 0 };
		var training = $scope.trainings[trainingIndex];

		angular.forEach(training.trainingEvents, function(trainingEvent, key) {
			var start = moment(trainingEvent.start).unix();
			var end = moment(trainingEvent.end).unix();
			var now = moment().unix();

			if(end < now) { /*upcoming*/
				count.upcoming++;
			} else if(start > now) { /*done*/
				count.done++;
			} else if(now >= start && now <= end) { /*ongoing*/
				count.ongoing++;
			}
		});

		training.trainingEventCount = count;
	};




	// watch for $scope.searchKeyword
	// update list based on searchKeyword
	var searchDelay;
	$scope.$watch('searchKeyword', function(newValue, oldValue) {
		if(newValue.trim() != oldValue.trim()) {
			// fetch new records
			if(searchDelay) $timeout.cancel(searchDelay);

			searchDelay = $timeout(function() {
				var options;
				if(newValue.trim() == '') options = { populate: populate };
				else options = { populate: populate, search: newValue };

				$scope.loadingProperties.promise = $http.post(httpTraining.enlist, options)
				.success(function(data ,status) {
					$scope.trainings = data;
				})
			}, 1000);
		}
	});






	$scope.showAddTraining = function() {
		trainingServices.showAddTraining().then(function() {
			$scope.reloadData();
		});
	};











	// fetch all trainings on page load
	$scope.loadTrainingList = function() {
		$scope.loadingProperties.promise = $http.post(httpTraining.enlist, { populate: populate })
		.success(function(data ,status) {
			$scope.trainings = data;
		});
	};




	$scope.loadTrainingList();




}]);