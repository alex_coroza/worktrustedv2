angular.module('trainingModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('admin.trainingList', {
			url: 'training-list/',
			templateUrl: DOMAIN+'app/modules/admin/training/views/trainingList.view.html',
			controller: 'trainingListController'
		})
		.state('admin.trainingInfo', {
			url: 'training-info/{id}',
			templateUrl: DOMAIN+'app/modules/admin/training/views/trainingInfo.view.html',
			controller: 'trainingInfoController'
		})
		.state('admin.trainingEdit', {
			url: 'training-edit/{id}',
			templateUrl: DOMAIN+'app/modules/admin/training/views/trainingEdit.view.html',
			controller: 'trainingEditController'
		})
	; /*closing $stateProvider*/
}]);

				