angular.module('trainingModule')
.factory('trainingServices', ['$q', '$state', '$mdDialog', function($q, $state, $mdDialog) {



	this.showAddTraining = function() {
		var deferred = $q.defer();

		$mdDialog.show({
			templateUrl: DOMAIN+'app/modules/admin/training/views/trainingList/addTraining.tpl.html',
			controller: 'addTrainingController'
		})
		.then(function(answer) {
			deferred.resolve(answer);
		});

		return deferred.promise;
	};






	return this;

}]);





