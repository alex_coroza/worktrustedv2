angular.module('profileModule')
.controller('profileInfoController', ['$scope', '$rootScope', '$http', '$state', '$mdDialog', '$mdToast', 'httpMember', 'httpAccount', 'utilityFunctions', 'schedulingServices', 'user', function($scope, $rootScope, $http, $state, $mdDialog, $mdToast, httpMember, httpAccount, utilityFunctions,schedulingServices, user) {
	console.log('profileInfoController initialized!');

	$rootScope.$emit('activeLinkValueUpdate', { value: 'profile' });
	
	$scope.user = user;

	$scope.loadingProperties = {
		promise: null,
	};	



	// fetch memberInfo
	var populate = {
		0: 'beneficiaries', 
		1: 'characterReferences', 
		2: 'education', 
		3: 'emergencyContact', 
		4: 'parent', 
		5: 'skills', 
		6: 'spouse', 
		7: 'workHistories',
		8: 'account',
		'trainingEvents': {
			'trainingEvent' : {
				0: 'training',
				'attendees': {
					0: 'member'
				}
			}
		}
	};


	$scope.loadProfile = function() {
		$scope.loadingProperties.message = 'Loading your profile info...';
		$scope.loadingProperties.promise = $http.post(httpMember.read, { id: $scope.user.memberId, populate: populate })
		.success(function(data, status) {
			$scope.member = data;
		});
	};



	$scope.trainingEventInfo = function(trainingEvent) {
		trainingEvent.title = trainingEvent.training.name;
		trainingEvent.type = 'training';
		schedulingServices.showScheduleInfo(trainingEvent).then(function(response) {
			if(response == 'deleted') {
				$scope.loadProfile();
			}
		});
	};


	$scope.accountEdit = function() {
		$state.go('admin.profileAccountEdit');
	};





	$scope.loadProfile();




}]);