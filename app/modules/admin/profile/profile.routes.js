angular.module('profileModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('admin.profileInfo', {
			url: 'profile/',
			templateUrl: DOMAIN+'app/modules/admin/profile/views/profileInfo.view.html',
			controller: 'profileInfoController'
		})
		.state('admin.profileAccountEdit', {
			url: 'profile-account-edit/',
			templateUrl: DOMAIN+'app/modules/admin/profile/views/accountEdit.view.html',
			controller: 'accountEditController'
		})
	; /*closing $stateProvider*/
}]);

				