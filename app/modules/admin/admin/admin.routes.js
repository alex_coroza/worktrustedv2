angular.module('adminModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.when('', '/');
	$stateProvider
		.state('notFound', {
			url: '/fucking-page-not-found',
			templateUrl: 'app/modules/core/views/custom404.view.html'
		})
		.state('admin', {
			url: '/',
			views: {
				'': {
					templateUrl: DOMAIN+'app/modules/admin/admin/views/admin.view.html',
					controller: 'adminController'
				},
				'sideNav@admin': {
					templateUrl: DOMAIN+'app/modules/admin/admin/views/admin/sideNav.view.html',
					controller: 'sideNavController'
				},
				'content@admin': {
					templateUrl: DOMAIN+'app/modules/admin/admin/views/admin/content.view.html'
				}
			},
			resolve: {
				user: ['$q', '$http', 'httpAccount', function($q, $http, httpAccount) {
					var deferred = $q.defer();
					$http.post(httpAccount.fetchLoggedUser, { type: 'member' })
					.success(function(data, status) {
						deferred.resolve(data);
					});
					return deferred.promise;
				}]
			}
		})
	; /*$stateProvider closing*/
}]);
