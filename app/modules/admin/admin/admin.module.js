var DOMAIN = '/worktrustedV2/';

angular.module('adminModule', [
	'ngMaterial', 'ngAria', 'ngMessages', 'ngAnimate', 'ui.router', 
	'http.service', 'utility.service', 'utility.directive', 'utility.filter',
	'angularMoment', 'ngFileUpload', 'camera', 'ui.calendar', 'cgBusy',
	'profileModule', 'membershipApplicantModule', 'manageMemberModule', 'jobsModule', 'schedulingModule', 'trainingModule', 'companyModule'
]);


// angular material date settings 
angular.module('adminModule')
.config(['$mdDateLocaleProvider', function($mdDateLocaleProvider) {
	// set format for md-datepicker
	$mdDateLocaleProvider.formatDate = function(date) {
		if(date == null) return date;
		return moment(date).format('MMM D, YYYY');
	};
}]);


// angular material theme settings
angular.module('adminModule')
.config(['$mdThemingProvider', function($mdThemingProvider) {
	$mdThemingProvider.theme('default')
		.primaryPalette('teal', {
			'default': '500',
			'hue-1': '700',
			'hue-2': '900'
		})
		.accentPalette('deep-orange', {
			'default': 'A400',
			'hue-1': '100'
		})
}]);



// angular-busy override settings
angular.module('adminModule')
.value('cgBusyDefaults', {
	message:'Loading...',
	backdrop: true,
	// templateUrl: 'my_custom_template.html',
	delay: 0,
	minDuration: 1000,
	// wrapperClass: 'my-class my-class2'
});


