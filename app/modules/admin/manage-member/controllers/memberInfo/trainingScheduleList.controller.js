angular.module('manageMemberModule')
.controller('trainingScheduleListController', ['$scope', '$rootScope', '$http', '$state', '$mdDialog', '$mdToast', 'dataStorage', 'httpSchedule', 'httpMember', 'utilityFunctions', 'member', 'schedulingServices', function($scope, $rootScope, $http, $state, $mdDialog, $mdToast, dataStorage, httpSchedule, httpMember, utilityFunctions, member, schedulingServices) {
	console.log('trainingScheduleListController initialized!');


	$scope.trainingSchedules = [''];
	$scope.memberTrainingScheduleIdCollection = [];
	$scope.selectedSchedule = '';
	$scope.member = member;


	$scope.loadingProperties = {
		promise: null,
	};


	$scope.loadingProperties.message = 'Loading training events/schedules...';
	$scope.loadingProperties.promise = $http.post(httpSchedule.enlistTrainingEvents, { populate: { 0: 'training', 'attendees': ['member'] }, upcoming: true, orderBy: ['start', 'asc'] })
	.success(function(data, status) {
		$scope.trainingSchedules = data;
	});




	// get all trainingEventId for the current member
	angular.forEach($scope.member.trainingEvents, function(memberTrainingEvent, index) {
		$scope.memberTrainingScheduleIdCollection.push(memberTrainingEvent.trainingEvent.id);
	});





	$scope.addEvent = function() {
		schedulingServices.showAddSchedule();
	};




	// check if a training event is already scheduled for the current member
	// return true if trainingEvent is not yet scheduled
	$scope.scheduledTrainingEventCheck = function(trainingEventIdSample) {
		if(utilityFunctions.inArray(trainingEventIdSample, $scope.memberTrainingScheduleIdCollection)) {
			return false;
		} else {
			return true;
		}
	};



	// submit the chosen training event
	$scope.chooseEvent = function() {
		$scope.member.trainingEvents.push({ trainingEventId: $scope.selectedSchedule, remark: 'PENDING' });
		$scope.loadingProperties.message = 'Submitting...';
		$scope.loadingProperties.promise = $http.post(httpMember.update, { info: $scope.member })
		.success(function(data, status) {
			var toast = $mdToast.simple()
				.textContent('Changes Saved!')
				.position('bottom right');
			$mdToast.show(toast);
			$mdDialog.hide('chosenScheduleSubmitted');
		});
	}



	$scope.dismiss = function() {
		$mdDialog.cancel();
	};




}]);