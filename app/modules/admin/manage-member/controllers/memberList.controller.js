angular.module('manageMemberModule')
.controller('memberListController', ['$scope', '$rootScope', '$timeout', '$http', '$state', '$stateParams', '$mdDialog', 'httpMember', 'user', function($scope, $rootScope, $timeout, $http, $state, $stateParams, $mdDialog, httpMember, user) {
	console.log('memberListController initialized!');

	$rootScope.$emit('activeLinkValueUpdate', { value: 'manageMembers' });

	$scope.user = user;


	$scope.searchKeyword = '';
	$scope.memberListView = 'member'; /*its either admins, members, or lineLeaders*/
	$scope.members = [];

	$scope.loadingProperties = {
		promise: null,
	};

	// populate value for fetching members
	var populate = {
		0: 'beneficiaries', 
		1: 'characterReferences', 
		2: 'education', 
		3: 'emergencyContact', 
		4: 'parent', 
		5: 'skills', 
		6: 'spouse', 
		7: 'workHistories',
		8: 'account',
		'trainingEvents': {
			'trainingEvent' : {
				0: 'training',
				'attendees': {
					0: 'member'
				}
			}
		}
	};



	$scope.reloadData = function() {
		$scope.loadMembers();
	};



	$scope.loadMembers = function() {
		var options = {
			populate: populate,
			accountType: $scope.memberListView
		};

		// search condition
		if($scope.searchKeyword.trim() != '') options.search = $scope.searchKeyword;

		$scope.loadingProperties.promise = $http.post(httpMember.enlist, options)
		.success(function(data ,status) {
			$scope.members = data;
		});
	};




	$scope.memberInfo = function(info) {
		$state.go('admin.memberInfo', { id: info.id });
	};





	// watch for $scope.searchKeyword
	// update list based on searchKeyword
	var searchDelay;
	$scope.$watch('searchKeyword', function(newValue, oldValue) {
		if(newValue.trim() != oldValue.trim()) {
			// fetch new records
			if(searchDelay) $timeout.cancel(searchDelay);

			searchDelay = $timeout(function() {
				$scope.loadMembers();
			}, 1000);
		}
	});




	$scope.addMember = function() {
		$state.go('admin.addMember');
	};










	// fetch all members on page load
	$scope.loadMembers();







}]);