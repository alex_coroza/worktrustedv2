angular.module('manageMemberModule')
.controller('addMemberController', ['$q', '$scope', '$rootScope', '$http', '$state', '$timeout', '$mdDialog', '$mdToast', 'regexPattern', 'utilityFunctions', 'formUtilities', 'httpMember', 'httpAccount', function($q, $scope, $rootScope, $http, $state, $timeout, $mdDialog, $mdToast, regexPattern, utilityFunctions, formUtilities, httpMember, httpAccount) {
	console.log('addMemberController initialized!');

	$rootScope.$emit('activeLinkValueUpdate', { value: 'manageMembers' });

	$scope.form = {};

	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();

	$scope.loadingProperties = {
		promise: null,
	};



	$scope.account = {
		userName: '',
		type: 'member'
	};

	// defaults for $scope.member
	$scope.member = {
		civilStatus: 'Single',
		gender: 'Male',
		birthDate: new Date(),
		education: {
			attainment: 'College Graduate'
		},
		skills: [
			{ skill: '' }
		],
		workHistories: [
			{ company: '', position: '', startDate: new Date(), endDate: new Date() }
		],
		beneficiaries: [
			{ name: '', relation: '', birthDate: new Date() }
		],
		characterReferences: [
			{ name: '', occupation: '', company: '', mobile: '', email: '' },
			{ name: '', occupation: '', company: '', mobile: '', email: '' },
			{ name: '', occupation: '', company: '', mobile: '', email: '' }
		],
		trainingEvents: [
			// wala pa
		],
		photo: 'default-avatar.jpg',
		tags: []
	};


	$scope.civilStatusOptions = ['Single', 'Married', 'Separated', 'Divorced', 'Widowed'];
	$scope.genderOptions = ['Male', 'Female'];
	$scope.attainmentOptions = ['Elementary Graduate', 'High School Graduate', 'Vocational Graduate', 'College Graduate', 'Vocational Undergraduate', 'College Undergraduate'];
	$scope.relationOptions = ['Father', 'Mother', 'Brother', 'Sister', 'Child']; /*spouse is separated because it needs to be dynamic(hide if single)*/
	$scope.accountTypeOptions = [
		{ id: 'member', value: 'Member' },
		{ id: 'admin', value: 'Admin' },
		{ id: 'lineLeader', value: 'Line Leader' }
	];










	$scope.backToMemberList = function() {
		$state.go('admin.memberList');
	};




	// function for add/remove field
	$scope.addSkillField = function() {
		$scope.member.skills.push({ skill: '' });
	};
	$scope.removeSkillField = function(key) {
		$scope.member.skills.splice(key, 1);
	};

	$scope.addWorkHistoryField = function() {
		$scope.member.workHistories.push({ company: '', position: '', startDate: new Date(), endDate: new Date() });
	};
	$scope.removeWorkHistoryField = function(key) {
		$scope.member.workHistories.splice(key, 1);
	};

	$scope.addBeneficiaryField = function() {
		if($scope.member.beneficiaries.length < 2) {
			$scope.member.beneficiaries.push({ name: '', relation: '', birthDate: new Date() });
		}
	};
	$scope.removeBeneficiaryField = function(key) {
		$scope.member.beneficiaries.splice(key, 1);
	};
	$scope.addCharacterReferenceField = function() {
		$scope.member.characterReferences.push({ name: '', occupation: '', company: '', mobile: '', email: '' });
	};
	$scope.removeCharacterReferenceField = function(key) {
		if($scope.member.characterReferences.length > 3) {
			$scope.member.characterReferences.splice(key, 1);
		}
	};
	$scope.removeTrainingEventField = function(key) {
		$scope.member.trainingEvents.splice(key, 1);
	};




	$scope.cancelAddConfirm = function() {
		var confirm = $mdDialog.confirm()
			.title('Discard changes?')
			.textContent('This action will discard all unsaved changes and will take you back to member list!')
			.ok('Proceed')
			.cancel('Close');
		$mdDialog.show(confirm).then(function() {
			$scope.backToMemberList();
		});
	};






	// save data to database
	// check userName >> save member data >> create account
	$scope.saveNewMemberData = function() {
		// check userName availability
		$scope.loadingProperties.message = 'Checking username availability...';
		$scope.loadingProperties.promise = $http.post(httpAccount.checkUserName, { type: 'member', userName: $scope.account.userName })
		.then(function(response) {
			if(response.data == 0) {
				// save memberInfo to database
				$scope.loadingProperties.message = 'Adding member to database...';
				return $scope.loadingProperties.promise = $http.post(httpMember.create, { info: $scope.member });
			} else {
				return $q.reject('Username already taken!');
			}
		}).then(function(response) {
			$scope.account.memberId = response.data;

			// create account
			$scope.loadingProperties.message = 'Creating account...';
			return $scope.loadingProperties.promise = $http.post(httpAccount.create, { type: 'member', info: $scope.account });
		}).then(function(response) {
			var toast = $mdToast.simple()
				.textContent('Changes saved!')
				.position('bottom right')
				.action('View');
			$mdToast.show(toast).then(function(response) {
				if(response == 'ok') {
					$state.go('admin.memberInfo', { id: $scope.account.memberId });
				} else {
					$state.go('admin.memberList');
				}
			});
		}).catch(function(errorMessage) {
			var toast = $mdToast.simple()
				.textContent(errorMessage)
				.position('bottom right');
			$mdToast.show(toast);
		});
	};






	$scope.submitAddMemberForm = function(form) {
		// check for errors first
		formUtilities.formErrorChecker(form);

		// alert error message if errors detected
		// else submit the form
		if(!utilityFunctions.isEmptyObject(form.$error)) {
			var alert = $mdDialog.alert()
				.title('Errors detected!')
				.textContent('See and fix the errors to save changes!')
				.ok('Close')
			$mdDialog.show(alert);
		} else {
			$scope.saveNewMemberData();
		}
	}








	$scope.showTrainingScheduleList = function() {
		$mdDialog.show({
			templateUrl: DOMAIN+'app/modules/admin/manage-member/views/memberEdit/trainingScheduleList2.tpl.html',
			controller: 'trainingScheduleList2Controller',
			locals: {
				member: $scope.member
			}
		}).then(function(response) {
			$scope.member = response;
		});
	};







	$scope.checkUserName = function() {
		$scope.loadingProperties.message = 'Checking username availability...';
		$scope.loadingProperties.promise = $http.post(httpAccount.checkUserName, { type: 'member', userName: $scope.account.userName })
		.success(function(data, status) {
			if(data == 0) {
				$scope.addMemberForm.accountUserName.$error.alreadyTaken = false;
				$scope.addMemberForm.accountUserName.$invalid = false;
				$scope.addMemberForm.accountUserName.$valid = true;
			} else {
				$scope.addMemberForm.accountUserName.$error.alreadyTaken = true;
				$scope.addMemberForm.accountUserName.$invalid = true;
				$scope.addMemberForm.accountUserName.$touched = true;
				var toast = $mdToast.simple()
					.textContent('Username already taken!')
					.position('bottom right');
				$mdToast.show(toast);
			}
		});
	};






	// check userName availability
	var searchDelay;
	$scope.userNameCheck = function() {
		if(searchDelay) $timeout.cancel(searchDelay);

		if($scope.account.userName)
		searchDelay = $timeout(function() {
			$scope.checkUserName();
		}, 1500);
	};







}]);