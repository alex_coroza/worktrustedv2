angular.module('manageMemberModule')
.controller('memberChangePictureController', ['$scope', '$rootScope', '$http', '$state', '$mdToast', '$mdDialog', 'Upload', 'utilityFunctions', 'httpMember', function($scope, $rootScope, $http, $state, $mdToast, $mdDialog, Upload, utilityFunctions, httpMember) {
	console.log('memberChangePictureController initialized!');

	$rootScope.$emit('activeLinkValueUpdate', { value: 'manageMembers' });
	
	$scope.pictureFile = '';
	$scope.pictureFileError = 'no picture selected';

	$scope.loadingProperties = {
		promise: null,
	};


	// fetch memberInfo
	var populate = {
		0: 'beneficiaries', 
		1: 'characterReferences', 
		2: 'education', 
		3: 'emergencyContact', 
		4: 'parent', 
		5: 'skills', 
		6: 'spouse', 
		7: 'workHistories',
		8: 'account',
		'trainingEvents': {
			'trainingEvent' : {
				0: 'training',
				'attendees': {
					0: 'member'
				}
			}
		}
	};
	$scope.loadingProperties.message = 'Loading member\'s info...';
	$scope.loadingProperties.promise = $http.post(httpMember.read, { id: $state.params.id, populate: populate })
	.success(function(data, status) {
		$scope.member = data;
	});



	$scope.backToMemberInfo = function() {
		$state.go('admin.memberInfo', { id: $scope.member.id });
	};



	$scope.selectPicture = function(files) {
		// start file validation here
		if(files && files.length) {
			var alert;
			var pictureFile = files[0];
			var acceptedFiletypes = ['png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG'];
			var sizeLimit = 512000; // 500KB
			var fileType = pictureFile.name.split('.').pop();
			$scope.pictureFileError = '';

			if(!utilityFunctions.inArray(fileType, acceptedFiletypes)) {
				$scope.pictureFileError = 'Not a valid image!';
				alert = $mdDialog.alert()
					.title('Errors detected!')
					.textContent($scope.pictureFileError)
					.ok('Close')
			}
			if(pictureFile.size > sizeLimit) {
				$scope.pictureFileError = 'Picture file must be 500KB and below!';
				alert = $mdDialog.alert()
					.title('Errors detected!')
					.textContent($scope.pictureFileError)
					.ok('Close')
			}
			if($scope.pictureFileError == '') {
				$scope.pictureFile = pictureFile;
			} else {
				$mdDialog.show(alert);
			}
		}
	};



	$scope.uploadPicture = function() {
		$scope.loadingProperties.message = 'Uploading photo...';
		$scope.loadingProperties.promise = Upload.upload({
			url: DOMAIN+'member/uploadProfilePicture',
			method: 'POST',
			file: $scope.pictureFile
		}).then(function (response) {
			// update member's photo in database
			$scope.loadingProperties.message = 'Assigning uploaded photo...';
			return $scope.loadingProperties.promise = $http.post(httpMember.memberPictureUpdate, { info: $scope.member, newPictureName: response.data.newFileName });
		}, function (response) {
			// error
			console.log(response);
			// console.log('Error status: ' + response.status);
		}, function (evt) {
			var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			console.log('progress: ' + progressPercentage + '% ');
		}).then(function(response) {
			var toast = $mdToast.simple()
				.textContent('Changes Saved!')
				.position('bottom right');
			$mdToast.show(toast);
			$scope.backToMemberInfo();
		});
	};




	$scope.uploadBase64 = function() {
		$scope.loadingProperties.message = 'Uploading photo...';
		$scope.loadingProperties.promise = $http.post(httpMember.uploadBase64ProfilePicture, { base64File: $scope.pictureFile })
		.then(function(response) {
			$scope.loadingProperties.message = 'Assigning uploaded photo...';
			return $scope.loadingProperties.promise = $http.post(httpMember.memberPictureUpdate, { info: $scope.member, newPictureName: response.data.newFileName });
		}).then(function(response) {
			var toast = $mdToast.simple()
				.textContent('Changes Saved!')
				.position('bottom right');
			$mdToast.show(toast);
			$scope.backToMemberInfo();
		});
	};




	$scope.submit = function() {
		if(($scope.pictureFile !== null && typeof $scope.pictureFile === 'object') || $scope.pictureFile == '') {
			$scope.uploadPicture();
		} else {
			$scope.uploadBase64();			
		}
	};





}]);