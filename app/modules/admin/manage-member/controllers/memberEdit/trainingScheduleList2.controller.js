angular.module('manageMemberModule')
.controller('trainingScheduleList2Controller', ['$scope', '$rootScope', '$http', '$state', '$mdDialog', '$mdToast', 'httpSchedule', 'httpMember', 'utilityFunctions', 'memberTrainingEvents', 'schedulingServices', function($scope, $rootScope, $http, $state, $mdDialog, $mdToast, httpSchedule, httpMember, utilityFunctions, memberTrainingEvents, schedulingServices) {
	console.log('trainingScheduleList2Controller initialized!');


	$scope.trainingSchedules = [''];
	$scope.memberTrainingScheduleIdCollection = [];
	$scope.selectedSchedule = '';
	$scope.memberTrainingEvents = memberTrainingEvents;


	$scope.loadingProperties = {
		promise: null,
	};


	$scope.loadingProperties.message = 'Loading training events/schedules...';
	$scope.loadingProperties.promise = $http.post(httpSchedule.enlistTrainingEvents, { populate: { 0: 'training', 'attendees': ['member'] }, upcoming: true, orderBy: ['start', 'asc'] })
	.success(function(data, status) {
		$scope.trainingSchedules = data;
	});




	// get all trainingEventId for the current member
	angular.forEach($scope.memberTrainingEvents, function(memberTrainingEvent, index) {
		$scope.memberTrainingScheduleIdCollection.push(memberTrainingEvent.trainingEvent.id);
	});





	$scope.addEvent = function() {
		schedulingServices.showAddSchedule();
	};




	// check if a training event is already scheduled for the current member
	// return true if trainingEvent is not yet scheduled
	$scope.scheduledTrainingEventCheck = function(trainingEventIdSample) {
		if(utilityFunctions.inArray(trainingEventIdSample, $scope.memberTrainingScheduleIdCollection)) {
			return false;
		} else {
			return true;
		}
	};



	// submit the chosen training event
	$scope.chooseEvent = function() {
		$scope.memberTrainingEvents.push({ trainingEventId: $scope.selectedSchedule.id, remark: 'PENDING', trainingEvent: $scope.selectedSchedule });
		$mdDialog.hide($scope.memberTrainingEvents);
	}



	$scope.dismiss = function() {
		$mdDialog.cancel();
	};




}]);