angular.module('manageMemberModule')
.controller('memberEditController', ['$scope', '$rootScope', '$http', '$state', '$mdDialog', '$mdToast', 'regexPattern', 'utilityFunctions', 'httpMember', 'httpAccount', function($scope, $rootScope, $http, $state, $mdDialog, $mdToast, regexPattern, utilityFunctions, httpMember, httpAccount) {
	console.log('memberEditController initialized!');

	$rootScope.$emit('activeLinkValueUpdate', { value: 'manageMembers' });

	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();

	$scope.loadingProperties = {
		promise: null,
	};

	$scope.civilStatusOptions = ['Single', 'Married', 'Separated', 'Divorced', 'Widowed'];
	$scope.genderOptions = ['Male', 'Female'];
	$scope.attainmentOptions = ['Elementary Graduate', 'High School Graduate', 'Vocational Graduate', 'College Graduate', 'Vocational Undergraduate', 'College Undergraduate'];
	$scope.relationOptions = ['Father', 'Mother', 'Brother', 'Sister', 'Child']; /*spouse is separated because it needs to be dynamic(hide if single)*/
	$scope.accountTypeOptions = [
		{ id: 'member', value: 'Member' },
		{ id: 'admin', value: 'Admin' },
		{ id: 'lineLeader', value: 'Line Leader' }
	];




	// fetch memberInfo
	var populate = {
		0: 'beneficiaries', 
		1: 'characterReferences', 
		2: 'education', 
		3: 'emergencyContact', 
		4: 'parent', 
		5: 'skills', 
		6: 'spouse', 
		7: 'workHistories',
		8: 'account',
		'trainingEvents': {
			'trainingEvent' : {
				0: 'training',
				'attendees': {
					0: 'member'
				}
			}
		}
	};
	$scope.loadingProperties.message = 'Loading member\'s info...';
	$scope.loadingProperties.promise = $http.post(httpMember.read, { id: $state.params.id, populate: populate })
	.success(function(data, status) {
		$scope.member = data;
	});





	$scope.backToMemberInfo = function() {
		$state.go('admin.memberInfo', { id: $scope.member.id });
	};




	// function for add/remove field
	$scope.addSkillField = function() {
		$scope.member.skills.push({ skill: '' });
	};
	$scope.removeSkillField = function(key) {
		$scope.member.skills.splice(key, 1);
	};

	$scope.addWorkHistoryField = function() {
		$scope.member.workHistories.push({ skill: '' });
	};
	$scope.removeWorkHistoryField = function(key) {
		$scope.member.workHistories.splice(key, 1);
	};

	$scope.addBeneficiaryField = function() {
		if($scope.member.beneficiaries.length < 2) {
			$scope.member.beneficiaries.push({ skill: '' });
		}
	};
	$scope.removeBeneficiaryField = function(key) {
		$scope.member.beneficiaries.splice(key, 1);
	};
	$scope.addCharacterReferenceField = function() {
		$scope.member.characterReferences.push({ name: '', occupation: '', company: '', mobile: '', email: '' });
	};
	$scope.removeCharacterReferenceField = function(key) {
		if($scope.member.characterReferences.length > 3) {
			$scope.member.characterReferences.splice(key, 1);
		}
	};
	$scope.removeTrainingEventField = function(key) {
		$scope.member.trainingEvents.splice(key, 1);
	};




	$scope.cancelEditConfirm = function() {
		var confirm = $mdDialog.confirm()
			.title('Cancel edit?')
			.textContent('This action will discard all unsaved changes!')
			.ok('Proceed')
			.cancel('Close');
		$mdDialog.show(confirm).then(function() {
			$scope.backToMemberInfo();
		});
	};





	$scope.submitMemberEditForm = function(form) {
		// check for errors first
		// set all fields to "touched"
		angular.forEach(form.$error, function(field) {
			angular.forEach(field, function(errorField) {
				errorField.$setTouched();
			});
		});

		// alert error message if errors detected
		// else submit the form
		if(!utilityFunctions.isEmptyObject(form.$error)) {
			var alert = $mdDialog.alert()
				.title('Errors detected!')
				.textContent('See and fix the errors to save changes!')
				.ok('Close')
			$mdDialog.show(alert);
		} else {
			$scope.loadingProperties.message = 'Saving changes...';
			$scope.loadingProperties.promise = $http.post(httpMember.update, { info: $scope.member })
			.success(function(data ,status) {
				var toast = $mdToast.simple()
					.textContent('Changes Saved!')
					.position('bottom right');
				$mdToast.show(toast);
				$state.go('admin.memberInfo', { id: $scope.member.id });
			});
		}
	}




	$scope.changePicture = function() {
		$state.go('admin.memberChangePicture', { id: $scope.member.id });
	};




	$scope.resetPassword = function() {
		$scope.loadingProperties.message = 'Resetting password...';
		$scope.loadingProperties.promise = $http.post(httpAccount.resetPassword, { type: 'member', info: $scope.member.account })
		.success(function(data, status) {
			var toast = $mdToast.simple()
				.textContent('Reset password successful!')
				.position('bottom right');
			$mdToast.show(toast);
		});
	};


	$scope.resetPasswordConfirm = function() {
		var confirm = $mdDialog.confirm()
			.title('Are you sure to reset this member\'s account password?')
			.textContent('This will reset the password equivalent to username!')
			.ok('OK')
			.cancel('Close');
		$mdDialog.show(confirm).then(function() {
			$scope.resetPassword();
		});
	};




	$scope.showTrainingScheduleList = function() {
		$mdDialog.show({
			templateUrl: DOMAIN+'app/modules/admin/manage-member/views/memberEdit/trainingScheduleList2.tpl.html',
			controller: 'trainingScheduleList2Controller',
			locals: {
				memberTrainingEvents: $scope.member.trainingEvents
			}
		}).then(function(response) {
			if(response) $scope.member.trainingEvents = response;
		});
	};





}]);