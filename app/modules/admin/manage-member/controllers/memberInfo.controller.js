angular.module('manageMemberModule')
.controller('memberInfoController', ['$scope', '$rootScope', '$http', '$state', '$mdDialog', '$mdToast', 'httpMember', 'httpAccount', 'utilityFunctions', 'schedulingServices', function($scope, $rootScope, $http, $state, $mdDialog, $mdToast, httpMember, httpAccount, utilityFunctions,schedulingServices) {
	console.log('memberInfoController initialized!');

	$rootScope.$emit('activeLinkValueUpdate', { value: 'manageMembers' });
	

	$scope.loadingProperties = {
		promise: null,
	};	



	// fetch memberInfo
	var populate = {
		0: 'beneficiaries', 
		1: 'characterReferences', 
		2: 'education', 
		3: 'emergencyContact', 
		4: 'parent', 
		5: 'skills', 
		6: 'spouse', 
		7: 'workHistories',
		8: 'account',
		'trainingEvents': {
			'trainingEvent' : {
				0: 'training',
				'attendees': {
					0: 'member'
				}
			}
		}
	};

	$scope.loadMemberInfo = function() {
		$scope.loadingProperties.message = 'Loading member\'s info...';
		$scope.loadingProperties.promise = $http.post(httpMember.read, { id: $state.params.id, populate: populate })
		.success(function(data, status) {
			$scope.member = data;
		});
	};

	

	$scope.backToMemberList = function() {
		$state.go('admin.memberList');
	};


	$scope.editMember = function() {
		$state.go('admin.memberEdit', { id: $scope.member.id });
	};



	$scope.changePicture = function() {
		$state.go('admin.memberChangePicture', { id: $scope.member.id });
	};



	$scope.resetPassword = function() {
		$scope.loadingProperties.message = 'Resetting password...';
		$scope.loadingProperties.promise = $http.post(httpAccount.resetPassword, { type: 'member', info: $scope.member.account })
		.success(function(data, status) {
			var toast = $mdToast.simple()
				.textContent('Reset password successful!')
				.position('bottom right');
			$mdToast.show(toast);
		});
	};


	$scope.resetPasswordConfirm = function() {
		var confirm = $mdDialog.confirm()
			.title('Are you sure to reset this member\'s account password?')
			.textContent('This will reset the password equivalent to username!')
			.ok('OK')
			.cancel('Close');
		$mdDialog.show(confirm).then(function() {
			$scope.resetPassword();
		});
	};






	$scope.showTrainingScheduleList = function() {
		$mdDialog.show({
			templateUrl: DOMAIN+'app/modules/admin/manage-member/views/memberInfo/trainingScheduleList.tpl.html',
			controller: 'trainingScheduleListController',
			locals: {
				member: $scope.member
			}
		}).then(function(response) {
			if(response == 'chosenScheduleSubmitted') $scope.loadMemberInfo();
		});
	};





	$scope.trainingEventInfo = function(trainingEvent) {
		// trainingEvent.start = moment(trainingEvent.start);
		// trainingEvent.end = moment(trainingEvent.end);
		trainingEvent.title = trainingEvent.training.name;
		trainingEvent.type = 'training';
		schedulingServices.showScheduleInfo(trainingEvent).then(function(response) {
			if(response == 'deleted') {
				$scope.loadMemberInfo();
			}
		});
	};




	$scope.loadMemberInfo();



}]);