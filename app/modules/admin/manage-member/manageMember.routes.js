angular.module('manageMemberModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('admin.memberList', {
			url: 'member-list/',
			templateUrl: DOMAIN+'app/modules/admin/manage-member/views/memberList.view.html',
			controller: 'memberListController'
		})
		.state('admin.memberInfo', {
			url: 'member-info/{id}',
			templateUrl: DOMAIN+'app/modules/admin/manage-member/views/memberInfo.view.html',
			controller: 'memberInfoController'
		})
		.state('admin.memberEdit', {
			url: 'member-edit/{id}',
			templateUrl: DOMAIN+'app/modules/admin/manage-member/views/memberEdit.view.html',
			controller: 'memberEditController'
		})
		.state('admin.addMember', {
			url: 'add-member',
			templateUrl: DOMAIN+'app/modules/admin/manage-member/views/addMember.view.html',
			controller: 'addMemberController'
		})
		.state('admin.memberChangePicture', {
			url: 'member-changePicture/{id}',
			templateUrl: DOMAIN+'app/modules/admin/manage-member/views/memberChangePicture.view.html',
			controller: 'memberChangePictureController'
		})
	; /*closing $stateProvider*/
}]);

				