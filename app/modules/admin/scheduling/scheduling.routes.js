angular.module('schedulingModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('admin.scheduling', {
			url: 'tools-scheduling',
			params: { action: null, eventInfo: null },
			templateUrl: DOMAIN+'app/modules/admin/scheduling/views/schedulingMain.view.html',
			controller: 'shchedulingMainController'
		})
	; /*closing $stateProvider*/
}]);

				