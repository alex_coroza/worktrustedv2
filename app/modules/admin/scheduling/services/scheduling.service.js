angular.module('schedulingModule')
.factory('schedulingServices', ['$q', '$state', '$mdDialog', function($q, $state, $mdDialog) {
	
	// use this fuction only if the scheduleInfo came from calendar
	this.scheduleAdapter = function(eventInfo, mode) {
		var eventInfoCopy = angular.copy(eventInfo);

		if(mode == 'display') {
			if(eventInfoCopy.end == null) {
				eventInfoCopy.end = eventInfoCopy.start;
			}
			eventInfoCopy.end = moment(eventInfoCopy.end).add(1, 'days');
		}

		else if(mode == 'save') {
			if(eventInfoCopy.end == null) {
			// use this because single day event sets 'end' to null
				eventInfoCopy.end = eventInfoCopy.start;
			} else {
				// subtract 1 day to end date because "allDay" option results to extra 1 day(occurs only when resizing or dragging multiple day events/schedules)
				eventInfoCopy.end = eventInfoCopy.end.subtract(1, 'days');
			}
		}

		return eventInfoCopy;
	};







	this.showAddSchedule = function() {
		var deferred = $q.defer();

		$mdDialog.show({
			templateUrl: DOMAIN+'app/modules/admin/scheduling/views/schedulingMain/addSchedule.tpl.html',
			controller: 'addScheduleController'
		})
		.then(function(answer) {
			deferred.resolve(answer);
		});

		return deferred.promise;
	};






	this.showScheduleInfo = function(eventInfo, adapter) {
		var templateUrl;
		var controller;
		var deferred = $q.defer();

		if(adapter == true) {
			eventInfo = this.scheduleAdapter(eventInfo, 'save');
		}

		if(eventInfo.type == 'membershipApplicantInterview') {
			templateUrl = DOMAIN+'app/modules/admin/scheduling/views/schedulingMain/membershipApplicantInterview/scheduleInfo.tpl.html';
			controller = 'membershipApplicantInterviewScheduleInfoController';
		} else if(eventInfo.type == 'training') {
			templateUrl = DOMAIN+'app/modules/admin/scheduling/views/schedulingMain/training/scheduleInfo.tpl.html';
			controller = 'trainingEventInfoController';
		}

		$mdDialog.show({
			templateUrl: templateUrl,
			controller: controller,
			locals: { eventInfo: eventInfo }
		})
		.then(function(response) {
			deferred.resolve(response);
		});

		return deferred.promise;
	};






	this.showScheduleEdit = function(eventInfo, adapter) {
		var templateUrl;
		var controller;
		var deferred = $q.defer();

		if(adapter == true) {
			eventInfo = this.scheduleAdapter(eventInfo, 'save');
		}

		if(eventInfo.type == 'membershipApplicantInterview') {
			templateUrl = DOMAIN+'app/modules/admin/scheduling/views/schedulingMain/membershipApplicantInterview/scheduleEdit.tpl.html';
			controller = 'membershipApplicantInterviewScheduleEditController';
		} else if(eventInfo.type == 'training') {
			templateUrl = DOMAIN+'app/modules/admin/scheduling/views/schedulingMain/training/scheduleEdit.tpl.html';
			controller = 'trainingEventEditController';
		}

		$mdDialog.show({
			templateUrl: templateUrl,
			controller: controller,
			locals: { eventInfo: eventInfo }
		})
		.then(function(response) {
			deferred.resolve(response);
		});

		return deferred.promise;
	};




	return this;

}]);





