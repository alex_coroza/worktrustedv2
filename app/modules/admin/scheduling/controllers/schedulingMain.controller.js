angular.module('schedulingModule')
.controller('shchedulingMainController', ['$scope', '$rootScope', '$http', '$compile', '$state', '$mdDialog', 'uiCalendarConfig', 'httpSchedule', 'schedulingServices', function($scope, $rootScope, $http, $compile, $state, $mdDialog, uiCalendarConfig, httpSchedule, schedulingServices) {
	console.log('shchedulingMainController initialized!');


	// BROADCAST/EMIT CATCHERS
	$rootScope.$on('calendarRefreshEvents', function(event, args) {
		$scope.reloadEvents();
	});


	$rootScope.$emit('activeLinkValueUpdate', { value: 'scheduling' });


	$scope.scheduleView = 'all';
	$scope.loadingProperties = {
		promise: null,
	};



	$scope.scheduleOptions = [
		{ value: 'all', name: 'All Events', icon: 'walang icon', color: 'walang icon' },
		{ value: 'membershipApplicantInterview', name: 'Membership Interviews', icon: 'mdi-checkbox-blank-circle', color: '#F44336' },
		{ value: 'training', name: 'Trainings', icon: 'mdi-checkbox-blank-circle', color: '#4CAF50' },
		{ value: 'sampleEvents', name: 'Sample Events', icon: 'mdi-checkbox-blank-circle', color: '#2196F3' },
	];
	

	$scope.eventSources = [

		// Membership Applicant Interviews
		{
			color: '#F44336',
			events: function(start, end, timezone, callback) {
				var options = { populate: {'applicantInterviews':['applicant']}, currentDate: true };
				$scope.loadingProperties.message = 'Fetching events/schedules...';
				$scope.loadingProperties.promise = $http.post(httpSchedule.enlistApplicantInterviewSchedules, options)
				.success(function(data, status) {
					angular.forEach(data, function(value, key) {
						value.allDay = true;
						value.title = 'Interview(Membership)'
						value.type = 'membershipApplicantInterview'
						value = schedulingServices.scheduleAdapter(value, 'display');
						data[key] = value;
					});
					callback(data);
				});
			}
		},


		// Trainings
		{
			color: '#4CAF50',
			events: function(start, end, timezone, callback) {
				var options = { 
					populate: {
						0: 'training', 
						'attendees': {
							0: 'member'
						}
					}, 
					currentDate: true 
				};
				$scope.loadingProperties.message = 'Fetching events/schedules...';
				$scope.loadingProperties.promise = $http.post(httpSchedule.enlistTrainingEvents, options)
				.success(function(data, status) {
					angular.forEach(data, function(value, key) {
						value.allDay = true;
						value.title = value.training.name;
						value.type = 'training';
						value = schedulingServices.scheduleAdapter(value, 'display');
						data[key] = value;
					});
					callback(data);
				});
			}
		},
		

		// Sample Events
		{
			color: '#2196F3',
			events: function(start, end, timezone, callback) {
				$scope.loadingProperties.message = 'Fetching events/schedules...';
				$scope.loadingProperties.promise = $http.post(httpSchedule.enlistSampleSchedules, {})
				.success(function(data, status) {
					angular.forEach(data, function(value, key) {
						value.allDay = true;
						value.type = 'sampleEvents'
						value = schedulingServices.scheduleAdapter(value, 'display');
						data[key] = value;
					});
					callback(data);
				});
			}
		}
	];






	// run this function uppon rendering events
	// add tooltip, buttons, etc to events upon render
	$scope.eventRender = function(eventInfo, element, view) {
		eventInfo = schedulingServices.scheduleAdapter(eventInfo, 'display');
		if(eventInfo.type == $scope.scheduleView || $scope.scheduleView == 'all') {
			var description = (eventInfo.description) ? eventInfo.description : '';
			element.prop('title', eventInfo.title+'\n'+description+'\n(Click for more info)');
		} else {
			element.attr('hidden', 'true');
		}

		$compile(element)($scope);
	};


	


	// fullcalendar adds 1 extra day to 'end' if the event is 'allDay = true'
	// so add 1 day in 'end' before displaying
	// and also subtract 1 day before saving
	$scope.scheduleAdapter = function(eventInfo, mode) {
		schedulingServices.scheduleAdapter(eventInfo, mode);
	};





	$scope.changeScheduleView = function(view, element) {
		uiCalendarConfig.calendars.mainCalendar.fullCalendar('rerenderEvents');
	};

	$scope.reloadEvents = function() {
		uiCalendarConfig.calendars.mainCalendar.fullCalendar('refetchEvents');
	};





	$scope.showAddSchedule = function() {
		schedulingServices.showAddSchedule();
	};


	// show dialog containing event info
	$scope.showEventInfo = function(eventInfo, jsEvent, view) {
		schedulingServices.showScheduleInfo(eventInfo, true);
	};



	$scope.saveScheduleChanges = function(eventInfo, delta, revertFunc) {
		if(eventInfo.type == 'membershipApplicantInterview') {
			eventInfo = schedulingServices.scheduleAdapter(eventInfo, 'save');
			$scope.loadingProperties.message = 'Saving...';
			$scope.loadingProperties.promise = $http.post(httpSchedule.saveApplicantInterviewSchedule, { info: eventInfo })
			.success(function(data, status) {
				// save change success
			});
		} else if(eventInfo.type == 'training') {
			eventInfo = schedulingServices.scheduleAdapter(eventInfo, 'save');
			$scope.loadingProperties.message = 'Saving...';
			$scope.loadingProperties.promise = $http.post(httpSchedule.saveTrainingEvent, { info: eventInfo })
			.success(function(data, status) {
				// save change success
			});
		}
	};






	$scope.uiConfig = {
		mainCalendar: {
			editable: true,
			header:{
				// left: 'month basicWeek basicDay agendaWeek agendaDay',
				left: 'title',
				right: 'today prev,next'
			},
			viewRender: $scope.changeScheduleView,
			// dayClick: $scope.alertEventOnClick,
			eventDrop: $scope.saveScheduleChanges,
			eventResize: $scope.saveScheduleChanges,
			eventRender: $scope.eventRender,
			eventClick: $scope.showEventInfo
		}
	};
	








	// INIT FUNCTIONS
	


}]);