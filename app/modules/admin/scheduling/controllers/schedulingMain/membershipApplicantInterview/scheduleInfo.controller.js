angular.module('schedulingModule')
.controller('membershipApplicantInterviewScheduleInfoController', ['$scope', '$rootScope', '$http', '$compile', '$state', '$mdDialog', '$mdToast', 'utilityFunctions', 'httpSchedule', 'schedulingServices', 'eventInfo', function($scope, $rootScope, $http, $compile, $state, $mdDialog, $mdToast, utilityFunctions, httpSchedule, schedulingServices, eventInfo) {
	console.log('membershipApplicantInterviewScheduleInfoController initialized!');

	$scope.loadingProperties = {
		promise: null,
	};


	$scope.eventInfo = eventInfo;


	if(!eventInfo.title) {
		var readOptions = {
			id: eventInfo.id,
			populate: {
				'applicantInterviews': [
					'applicant'
				] 
			}
		}

		$scope.loadingProperties.message = 'Loading event/schedule';
		$scope.loadingProperties.promise = $http.post(httpSchedule.readApplicantInterviewSchedule, readOptions)
		.success(function(data, status) {
			$scope.eventInfo = data;
			$scope.eventInfo.title = 'Interview(Membership)';
		});
	}



	$scope.dismiss = function() {
		$mdDialog.cancel();
	};





	$scope.deleteEvent = function() {
		var confirm = window.confirm('Delete this schedule/event?');

		if(confirm) {
			$scope.loadingProperties.message = 'Deleting event/schedule';
			$scope.loadingProperties.promise = $http.post(httpSchedule.deleteApplicantInterviewSchedule, { id: $scope.eventInfo.id })
			.success(function(data, status) {
				var toast = $mdToast.simple()
					.textContent('Event/Schedule deleted!')
					.position('bottom right');
				$mdToast.show(toast);
				$mdDialog.hide('deleted');
				$rootScope.$emit('calendarRefreshEvents', {});
			});
		}
	};




	$scope.editEvent = function() {
		schedulingServices.showScheduleEdit($scope.eventInfo);
	};







	$scope.applicantInfo = function(info) {
		$state.go('admin.membershipApplicantInfo', { id: info.id });
	};





}]);