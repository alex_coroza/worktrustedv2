angular.module('schedulingModule')
.controller('membershipApplicantInterviewScheduleEditController', ['$scope', '$rootScope', '$http', '$compile', '$state', '$mdDialog', '$mdToast', 'utilityFunctions', 'httpSchedule', 'eventInfo', function($scope, $rootScope, $http, $compile, $state, $mdDialog, $mdToast, utilityFunctions, httpSchedule, eventInfo) {
	console.log('membershipApplicantInterviewScheduleEditController initialized!');


	$scope.eventInfo = eventInfo;

	$scope.loadingProperties = {
		promise: null
	};

	$scope.dismiss = function() {
		$mdDialog.cancel();
	};










	$scope.submitForm = function(form) {
		// check for errors first
		// set all fields to "touched"
		angular.forEach(form.$error, function(field) {
			angular.forEach(field, function(errorField) {
				errorField.$setTouched();
			});
		});


		// alert error message if errors detected
		// else submit the form
		if(!utilityFunctions.isEmptyObject(form.$error)) {
			var toast = $mdToast.simple()
				.textContent('Errors Detected! See and fix the errors to save changes!')
				.position('bottom right');
			$mdToast.show(toast);
		} else {
			if($scope.eventInfo.type == 'membershipApplicantInterview') {
				$scope.saveChanges();
			}
		}
	};





	$scope.applicantInfo = function(applicantInfo) {
		$state.go('admin.membershipApplicantInfo', { id: applicantInfo.id });
	};



	$scope.deleteApplicant = function(applicantKey) {
		$scope.eventInfo.applicants.splice(applicantKey, 1);
	};


	$scope.saveChanges = function() {
		$scope.loadingProperties.message = 'Saving changes...';
		$scope.loadingProperties.promise = $http.post(httpSchedule.saveApplicantInterviewSchedule, { info: $scope.eventInfo })
		.success(function(data, status) {
			var toast = $mdToast.simple()
				.textContent('Changes Saved!')
				.position('bottom right');
			$mdToast.show(toast);
			$mdDialog.hide('saved');
			$rootScope.$emit('calendarRefreshEvents', {});
		});
	};



}]);