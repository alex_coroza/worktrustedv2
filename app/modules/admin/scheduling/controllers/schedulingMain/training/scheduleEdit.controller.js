angular.module('schedulingModule')
.controller('trainingEventEditController', ['$scope', '$rootScope', '$http', '$compile', '$state', '$mdDialog', '$mdToast', 'utilityFunctions', 'httpSchedule', 'httpTraining', 'regexPattern', 'eventInfo', function($scope, $rootScope, $http, $compile, $state, $mdDialog, $mdToast, utilityFunctions, httpSchedule, httpTraining, regexPattern, eventInfo) {
	console.log('trainingEventEditController initialized!');


	$scope.eventInfo = eventInfo;

	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();

	$scope.loadingProperties = {
		promise: null
	};

	$scope.dismiss = function() {
		$mdDialog.cancel();
	};




	// fetch list of trainings
	$scope.fetchTrainingOptions = function() {
		$scope.loadingProperties.message = 'Loading trainings...';
		$scope.loadingProperties.promise = $http.post(httpTraining.enlist, {})
		.success(function(data ,status) {
			$scope.trainingOptions = data;
		});
	};






	$scope.submitForm = function(form) {
		// check for errors first
		// set all fields to "touched"
		angular.forEach(form.$error, function(field) {
			angular.forEach(field, function(errorField) {
				errorField.$setTouched();
			});
		});


		// alert error message if errors detected
		// else submit the form
		if(!utilityFunctions.isEmptyObject(form.$error)) {
			var toast = $mdToast.simple()
				.textContent('Errors Detected! See and fix the errors to save changes!')
				.position('bottom right');
			$mdToast.show(toast);
		} else {
			if($scope.eventInfo.type == 'training') {
				$scope.saveChanges();
			}
		}
	};





	$scope.deleteAttendee = function(attendeeKey) {
		$scope.eventInfo.attendees.splice(attendeeKey, 1);
	};


	$scope.saveChanges = function() {
		$scope.loadingProperties.message = 'Saving changes...';
		$scope.loadingProperties.promise = $http.post(httpSchedule.saveTrainingEvent, { info: $scope.eventInfo })
		.success(function(data, status) {
			var toast = $mdToast.simple()
				.textContent('Changes Saved!')
				.position('bottom right');
			$mdToast.show(toast);
			$mdDialog.hide('saved');
			$rootScope.$emit('calendarRefreshEvents', {});
		});
	};



	$scope.memberInfo = function(info) {
		$state.go('admin.memberInfo', { id: info.id });
	};







	// INIT FUNCTIONS
	$scope.fetchTrainingOptions();




}]);