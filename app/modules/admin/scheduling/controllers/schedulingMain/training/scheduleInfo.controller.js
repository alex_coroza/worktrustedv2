angular.module('schedulingModule')
.controller('trainingEventInfoController', ['$scope', '$rootScope', '$http', '$compile', '$state', '$mdDialog', '$mdToast', 'utilityFunctions', 'httpSchedule', 'eventInfo', 'schedulingServices', function($scope, $rootScope, $http, $compile, $state, $mdDialog, $mdToast, utilityFunctions, httpSchedule, eventInfo, schedulingServices) {
	console.log('trainingEventInfoController initialized!');

	$scope.loadingProperties = {
		promise: null,
	};
	

	$scope.eventInfo = eventInfo;


	if(!eventInfo.title) {
		var readOptions = {
			id: eventInfo.id,
			populate: {
				0: 'training',
				'attendees': [
					'member'
				] 
			}
		}
		$scope.loadingProperties.message = 'Loading event/schedule';
		$scope.loadingProperties.promise = $http.post(httpSchedule.readTrainingEvent, readOptions)
		.success(function(data, status) {
			$scope.eventInfo = data;
			$scope.eventInfo.title = data.training.name;
		});
	}


	$scope.loadingProperties = {
		promise: null,
	};


	$scope.dismiss = function() {
		$mdDialog.cancel();
	};





	$scope.deleteEvent = function() {
		var confirm = window.confirm('Delete this training schedule/event?');

		if(confirm) {
			$scope.loadingProperties.message = 'Deleting event/schedule';
			$scope.loadingProperties.promise = $http.post(httpSchedule.deleteTrainingEvent, { id: $scope.eventInfo.id })
			.success(function(data, status) {
				var toast = $mdToast.simple()
					.textContent('Training Event/Schedule deleted!')
					.position('bottom right');
				$mdToast.show(toast);
				$mdDialog.hide('deleted');
				$rootScope.$emit('calendarRefreshEvents', {});
			});
		}
	};




	$scope.editEvent = function() {
		schedulingServices.showScheduleEdit($scope.eventInfo);
	};







	$scope.memberInfo = function(info) {
		$state.go('admin.memberInfo', { id: info.id });
	};





}]);