angular.module('schedulingModule')
.controller('addScheduleController', ['$scope', '$rootScope', '$http', '$compile', '$state', '$mdDialog', '$mdToast', 'regexPattern', 'utilityFunctions', 'httpSchedule', 'httpTraining', 'schedulingServices', 'trainingServices', function($scope, $rootScope, $http, $compile, $state, $mdDialog, $mdToast, regexPattern, utilityFunctions, httpSchedule, httpTraining, schedulingServices, trainingServices) {
	console.log('addScheduleController initialized!');


	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();

	$scope.scheduleView = '';

	$scope.loadingProperties = {
		promise: null,
	};

	$scope.scheduleOptions = [
		{ value: 'membershipApplicantInterview', name: 'Membership Interviews', icon: 'mdi-checkbox-blank-circle', color: '#F44336' },
		{ value: 'training', name: 'Training Events', icon: 'mdi-checkbox-blank-circle', color: '#4CAF50' }
	];



	$scope.scheduleViewChange = function() {
		if($scope.scheduleView == 'membershipApplicantInterview') {
			$scope.newSchedule = {
				start: null,
				end: null
			};
		}



		if($scope.scheduleView == 'training') {
			$scope.newSchedule = {
				start: null,
				end: null,
				requirements: []
			};

			// fetch list of trainings
			$scope.loadingProperties.message = 'Loading trainings...';
			$scope.loadingProperties.promise = $http.post(httpTraining.enlist, {})
			.success(function(data ,status) {
				$scope.trainingOptions = data;
			});
		}
	};






	$scope.submitMembershipApplicantScheduleForm = function() {
		$scope.loadingProperties.message = 'Saving changes...';
		$scope.loadingProperties.promise = $http.post(httpSchedule.saveApplicantInterviewSchedule, { info: $scope.newSchedule })
		.success(function(data, status) {
			$scope.newSchedule.id = data;
			$scope.newSchedule.type = 'membershipApplicantInterview';
			var toast = $mdToast.simple()
				.textContent('Changes Saved!')
				.position('bottom right')
				.action('View');
			$mdToast.show(toast).then(function(response) {
				if(response == 'ok') {
					schedulingServices.showScheduleInfo($scope.newSchedule).then(function(response) {
						if(response == 'deleted') {
							$state.go($state.current, {}, {reload: true});
						}
					});
				}
			});
			$mdDialog.hide('formSubmitted');
			$rootScope.$emit('calendarRefreshEvents', {});
		});
	};



	$scope.submitTrainingEventForm = function() {
		$scope.loadingProperties.message = 'Saving changes...';
		$scope.loadingProperties.promise = $http.post(httpSchedule.saveTrainingEvent, { info: $scope.newSchedule })
		.success(function(data ,status) {
			$scope.newSchedule.id = data;			
			$scope.newSchedule.type = 'training';
			var toast = $mdToast.simple()
				.textContent('Changes Saved!')
				.position('bottom right')
				.action('View');
			$mdToast.show(toast).then(function(response) {
				if(response == 'ok') {
					schedulingServices.showScheduleInfo($scope.newSchedule).then(function(response) {
						if(response == 'deleted') {
							$state.go($state.current, {}, {reload: true});
						}
					});
				}
			});
			$mdDialog.hide('formSubmitted');
			$rootScope.$emit('calendarRefreshEvents', {});
		});
	};







	$scope.submitForm = function(form) {
		// check for errors first
		// set all fields to "touched"
		angular.forEach(form.$error, function(field) {
			angular.forEach(field, function(errorField) {
				errorField.$setTouched();
			});
		});

		// alert error message if errors detected
		// else submit the form
		if(!utilityFunctions.isEmptyObject(form.$error)) {
			var toast = $mdToast.simple()
				.textContent('Errors Detected! See and fix the errors to save changes!')
				.position('bottom right');
			$mdToast.show(toast);
		} else {
			if($scope.scheduleView == 'membershipApplicantInterview') {
				$scope.submitMembershipApplicantScheduleForm();
			} else if($scope.scheduleView == 'training') {
				$scope.submitTrainingEventForm();
			}
		}
	}





	$scope.dismiss = function() {
		$mdDialog.cancel();
	};





	$scope.showAddTraining = function() {
		trainingServices.showAddTraining();
	};






}]);