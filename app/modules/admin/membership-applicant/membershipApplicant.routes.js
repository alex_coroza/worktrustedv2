angular.module('membershipApplicantModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('admin.membershipApplicants', {
			url: 'membership-applicants/',
			templateUrl: DOMAIN+'app/modules/admin/membership-applicant/views/membershipApplicantList.view.html',
			controller: 'membershiApplicantListController'
		})
		.state('admin.membershipApplicantInfo', {
			url: 'membership-applicant-info/{id}',
			templateUrl: DOMAIN+'app/modules/admin/membership-applicant/views/membershipApplicantInfo.view.html',
			controller: 'membershiApplicantInfoController'
		})
		.state('admin.membershipApplicantEdit', {
			url: 'membership-applicant-edit/{id}',
			templateUrl: DOMAIN+'app/modules/admin/membership-applicant/views/membershipApplicantEdit.view.html',
			controller: 'membershiApplicantEditController'
		})
		.state('admin.membershipApplicantChangePicture', {
			url: 'membership-applicant-changePicture/{id}',
			templateUrl: DOMAIN+'app/modules/admin/membership-applicant/views/membershipApplicantChangePicture.view.html',
			controller: 'membershipApplicantChangePictureController'
		})
	; /*closing $stateProvider*/
}]);

				