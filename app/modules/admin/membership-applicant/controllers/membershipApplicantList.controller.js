angular.module('membershipApplicantModule')
.controller('membershiApplicantListController', ['$scope', '$rootScope', '$timeout', '$http', '$state', '$stateParams', '$mdDialog', 'httpApplicant', function($scope, $rootScope, $timeout, $http, $state, $stateParams, $mdDialog, httpApplicant) {
	console.log('membershiApplicantListController initialized!');

	$rootScope.$emit('activeLinkValueUpdate', { value: 'membershipApplicants' });


	$scope.searchKeyword = '';
	$scope.applicantListView = 'newApplicants';
	$scope.applicants = [];

	$scope.loadingProperties = {
		promise: null,
	};

	// populate value for fetching applicants
	var populate = {
		0: 'beneficiaries', 
		1: 'characterReferences', 
		2: 'education', 
		3: 'emergencyContact', 
		4: 'parent', 
		5: 'skills', 
		6: 'spouse', 
		7: 'workHistories',
		'interview': {
			0: 'interview'
		}
	};



	$scope.reloadData = function() {
		$scope.loadApplicants();
	};



	$scope.loadApplicants = function() {
		var options = {
			populate: populate
		};

		// view condition
		if($scope.applicantListView == 'newApplicants') options.newApplicants = true;
		else if($scope.applicantListView == 'applicantsForInterview') options.applicantsForInterview = true;

		// search condition
		if($scope.searchKeyword.trim() != '') options.search = $scope.searchKeyword;

		$scope.loadingProperties.promise = $http.post(httpApplicant.enlist, options)
		.success(function(data ,status) {
			$scope.applicants = data;
		});
	};



	$scope.applicantInfo = function(info) {
		$state.go('admin.membershipApplicantInfo', { id: info.id });
	};


	$scope.rejectApplicant = function(applicantId) {
		$scope.loadingProperties.promise = $http.post(httpApplicant.permanentDelete, { id: applicantId, deletePhoto: true })
		.success(function(data, status) {
			$scope.reloadData();
		});
	};


	$scope.showRejectConfirm = function(applicant) {
		var confirm = $mdDialog.confirm()
			.title('Reject applicant?')
			.textContent('This will delete/remove '+applicant.firstName+'\'s application for membership.')
			.ariaLabel('Lucky day')
			.ok('OK!')
			.cancel('Cancel');
		$mdDialog.show(confirm).then(function() {
			$scope.rejectApplicant(applicant.id);
		}, function() {
			// cancel, no action
		});
	};








	// watch for $scope.searchKeyword
	// update list based on searchKeyword
	var searchDelay;
	$scope.$watch('searchKeyword', function(newValue, oldValue) {
		if(newValue.trim() != oldValue.trim()) {
			// fetch new records
			if(searchDelay) $timeout.cancel(searchDelay);

			searchDelay = $timeout(function() {
				$scope.loadApplicants();
			}, 1000);
		}
	});




	// fetch all applicants on page load
	$scope.loadApplicants();




}]);