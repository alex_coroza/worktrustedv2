angular.module('membershipApplicantModule')
.controller('membershiApplicantInfoController', ['$scope', '$rootScope', '$http', '$state', '$mdDialog', '$mdToast', 'httpApplicant', 'httpMember', 'httpAccount', 'utilityFunctions', function($scope, $rootScope, $http, $state, $mdDialog, $mdToast, httpApplicant, httpMember, httpAccount, utilityFunctions) {
	console.log('membershiApplicantInfoController initialized!');

	$rootScope.$emit('activeLinkValueUpdate', { value: 'membershipApplicants' });



	$scope.loadingProperties = {
		promise: null,
	};



	// fetch applicantInfo
	var populate = {
		0: 'beneficiaries', 
		1: 'characterReferences', 
		2: 'education', 
		3: 'emergencyContact', 
		4: 'parent', 
		5: 'skills', 
		6: 'spouse', 
		7: 'workHistories',
		'interview': {
			0: 'interview'
		}
	};

	$scope.loadMembershipApplicantInfo = function() {
		$scope.loadingProperties.message = 'Loading applicant\'s info...';
		$scope.loadingProperties.promise = $http.post(httpApplicant.read, { id: $state.params.id, populate: populate })
		.success(function(data, status) {
			$scope.applicant = data;
		});
	};

	

	$scope.backToMembershipApplicants = function() {
		$state.go('admin.membershipApplicants');
	};


	$scope.editApplicant = function() {
		$state.go('admin.membershipApplicantEdit', { id: $scope.applicant.id });
	};



	$scope.rejectApplicant = function() {
		$scope.loadingProperties.promise = $http.post(httpApplicant.permanentDelete, { id: $scope.applicant.id, deletePhoto: true })
		.success(function(data, status) {
			var toast = $mdToast.simple()
				.textContent($scope.applicant.firstName+'\'s application deleted!')
				.position('bottom right');
			$mdToast.show(toast);
			$scope.backToMembershipApplicants();
		});
	};



	$scope.showRejectConfirm = function() {
		var confirm = $mdDialog.confirm()
			.title('Reject applicant?')
			.textContent('This will delete/remove '+$scope.applicant.firstName+'\'s application for membership.')
			.ariaLabel('Lucky day')
			.ok('OK!')
			.cancel('Cancel');
		$mdDialog.show(confirm).then(function() {
			$scope.rejectApplicant();
		}, function() {
			// cancel, no action
		});
	};




	$scope.showInterviewScheduleList = function() {
		$mdDialog.show({
			templateUrl: DOMAIN+'app/modules/admin/membership-applicant/views/membershipApplicantInfo/interviewScheduleList.tpl.html',
			controller: 'interviewScheduleListController',
			locals: {
				applicant: $scope.applicant
			}
		}).then(function(response) {
			if(response == 'chosenScheduleSubmitted') $scope.loadMembershipApplicantInfo();
		});
	};



	$scope.acceptApplicant = function() {
		var confirm = $mdDialog.confirm()
			.title('Add this applicant as member?')
			.textContent('An account will be automatically created upon accepting this applicant.')
			.ok('Proceed')
			.cancel('Cancel');
		$mdDialog.show(confirm).then(function() {
			// create member
			var memberId;
			$scope.loadingProperties.message = 'Creating member record/info...';
			$scope.loadingProperties.promise = $http.post(httpMember.create, { info: $scope.applicant })
			.then(function(response) {
				memberId = response.data;

				// create account for newly created member
				var accountInfo = {
					memberId: memberId
				};

				$scope.loadingProperties.message = 'Creating account...';
				return $scope.loadingProperties.promise = $http.post(httpAccount.create, { type: 'member', info: accountInfo });
			}).then(function(response) {
				// delete applicant record
				$scope.loadingProperties.message = 'Deleting applicant\'s record...';
				return $scope.loadingProperties.promise = $http.post(httpApplicant.permanentDelete, { id: $scope.applicant.id });
			}).then(function(response) {
				var toast = $mdToast.simple()
					.textContent($scope.applicant.firstName+' '+$scope.applicant.lastName+' was successfully added to members!')
					.position('bottom right')
					.action('View');
				$mdToast.show(toast).then(function(response) {
					if(response == 'ok') $state.go('admin.memberInfo', { id: memberId });
					else $state.go('admin.membershipApplicants');
				});
			});
		});
	};



	$scope.changePicture = function() {
		$state.go('admin.membershipApplicantChangePicture', { id: $scope.applicant.id });
	};






	$scope.loadMembershipApplicantInfo();





}]);