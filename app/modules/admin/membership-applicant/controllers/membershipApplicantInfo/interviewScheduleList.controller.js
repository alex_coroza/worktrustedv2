angular.module('membershipApplicantModule')
.controller('interviewScheduleListController', ['$scope', '$rootScope', '$http', '$state', '$mdDialog', '$mdToast', 'dataStorage', 'httpSchedule', 'httpApplicant', 'utilityFunctions', 'applicant', 'schedulingServices', function($scope, $rootScope, $http, $state, $mdDialog, $mdToast, dataStorage, httpSchedule, httpApplicant, utilityFunctions, applicant, schedulingServices) {
	console.log('interviewScheduleListController initialized!');


	$scope.interviewSchedules = [''];
	$scope.selectedSchedule = '';
	$scope.applicant = applicant;


	$scope.loadingProperties = {
		promise: null,
	};


	$scope.loadingProperties.message = 'Loading interview schedules...';
	$scope.loadingProperties.promise = $http.post(httpSchedule.enlistApplicantInterviewSchedules, { populate: { 'applicantInterviews': ['applicant'] }, upcoming: true, orderBy: ['start', 'asc'] })
	.success(function(data, status) {
		$scope.interviewSchedules = data;
	});




	$scope.addEvent = function() {
		schedulingServices.showAddSchedule();
	};



	// submit the chosen interview event
	$scope.chooseEvent = function() {
		$scope.applicant.interview = {};
		$scope.applicant.interview.scheduleApplicantInterviewId = $scope.selectedSchedule;
		$scope.loadingProperties.message = 'Submitting...';
		$scope.loadingProperties.promise = $http.post(httpApplicant.update, { info: $scope.applicant })
		.success(function(data, status) {
			var toast = $mdToast.simple()
				.textContent('Changes Saved!')
				.position('bottom right');
			$mdToast.show(toast);
			$mdDialog.hide('chosenScheduleSubmitted');
		});
	}



	$scope.dismiss = function() {
		$mdDialog.cancel();
	};




}]);