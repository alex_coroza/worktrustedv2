angular.module('membershipApplicantModule')
.controller('membershipApplicantChangePictureController', ['$scope', '$rootScope', '$http', '$state', '$mdToast', '$mdDialog', 'Upload', 'utilityFunctions', 'httpApplicant', function($scope, $rootScope, $http, $state, $mdToast, $mdDialog, Upload, utilityFunctions, httpApplicant) {
	console.log('membershipApplicantChangePictureController initialized!');

	$rootScope.$emit('activeLinkValueUpdate', { value: 'membershipApplicants' });
	
	$scope.pictureFile = '';
	$scope.pictureFileError = 'no picture selected';


	$scope.loadingProperties = {
		promise: null,
	};


	// fetch applicantInfo
	$scope.loadingProperties.message = 'Loading applicant\'s info...';
	$scope.loadingProperties.promise = $http.post(httpApplicant.read, { id: $state.params.id, populate: ['beneficiaries', 'characterReferences', 'education', 'emergencyContact', 'parent', 'skills', 'spouse', 'workHistories'] })
	.success(function(data, status) {
		$scope.applicant = data;
	});



	$scope.backToMembershipApplicantInfo = function() {
		$state.go('admin.membershipApplicantInfo', { id: $scope.applicant.id });
	};



	$scope.selectPicture = function(files) {
		// start file validation here
		if(files && files.length) {
			var alert;
			var pictureFile = files[0];
			var acceptedFiletypes = ['png', 'jpg', 'jpeg', 'PNG', 'JPG', 'JPEG'];
			var sizeLimit = 512000; // 500KB
			var fileType = pictureFile.name.split('.').pop();
			$scope.pictureFileError = '';

			if(!utilityFunctions.inArray(fileType, acceptedFiletypes)) {
				$scope.pictureFileError = 'Not a valid image!';
				alert = $mdDialog.alert()
					.title('Errors detected!')
					.textContent($scope.pictureFileError)
					.ok('Close')
			}
			if(pictureFile.size > sizeLimit) {
				$scope.pictureFileError = 'Picture file must be 500KB and below!';
				alert = $mdDialog.alert()
					.title('Errors detected!')
					.textContent($scope.pictureFileError)
					.ok('Close')
			}
			if($scope.pictureFileError == '') {
				$scope.pictureFile = pictureFile;
			} else {
				$mdDialog.show(alert);
			}
		}
	};



	$scope.uploadPicture = function() {
		$scope.loadingProperties.message = 'Uploading photo...';
		$scope.loadingProperties.promise = Upload.upload({
			url: DOMAIN+'applicant/uploadProfilePicture',
			method: 'POST',
			file: $scope.pictureFile
		}).then(function (response) {
			// update picture name in database, also delete prev picture file
			$scope.loadingProperties.message = 'Assigning uploaded photo...';
			return $scope.loadingProperties.promise = $http.post(httpApplicant.applicantPictureUpdate, { info: $scope.applicant, newPictureName: response.data.newFileName });
		}, function (response) {
			// error
			console.log(response);
			// console.log('Error status: ' + response.status);
		}, function (evt) {
			var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			console.log('progress: ' + progressPercentage + '% ');
		}).then(function(response) {
			var toast = $mdToast.simple()
				.textContent('Changes Saved!')
				.position('bottom right');
			$mdToast.show(toast);
			$scope.backToMembershipApplicantInfo();
		});
	};




	$scope.uploadBase64 = function() {
		$scope.loadingProperties.message = 'Uploading photo...';
		$scope.loadingProperties.promise = $http.post(httpApplicant.uploadBase64ProfilePicture, { base64File: $scope.pictureFile })
		.then(function(response) {
			$scope.loadingProperties.message = 'Assigning uploaded photo...';
			return $scope.loadingProperties.promise = $http.post(httpApplicant.applicantPictureUpdate, { info: $scope.applicant, newPictureName: response.data.newFileName });
		}).then(function(response) {
			var toast = $mdToast.simple()
				.textContent('Changes Saved!')
				.position('bottom right');
			$mdToast.show(toast);
			$scope.backToMembershipApplicantInfo();
		});
	};




	$scope.submit = function() {
		if(($scope.pictureFile !== null && typeof $scope.pictureFile === 'object') || $scope.pictureFile == '') {
			$scope.uploadPicture();
		} else {
			$scope.uploadBase64();			
		}
	};





}]);