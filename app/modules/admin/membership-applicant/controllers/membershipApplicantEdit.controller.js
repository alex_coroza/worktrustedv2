angular.module('membershipApplicantModule')
.controller('membershiApplicantEditController', ['$scope', '$rootScope', '$http', '$state', '$mdDialog', '$mdToast', 'regexPattern', 'utilityFunctions', 'httpApplicant', function($scope, $rootScope, $http, $state, $mdDialog, $mdToast, regexPattern, utilityFunctions, httpApplicant) {
	console.log('membershiApplicantEditController initialized!');

	$rootScope.$emit('activeLinkValueUpdate', { value: 'membershipApplicants' });

	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();

	$scope.loadingProperties = {
		promise: null,
	};

	$scope.civilStatusOptions = ['Single', 'Married', 'Separated', 'Divorced', 'Widowed'];
	$scope.genderOptions = ['Male', 'Female'];
	$scope.attainmentOptions = ['Elementary Graduate', 'High School Graduate', 'Vocational Graduate', 'College Graduate', 'Vocational Undergraduate', 'College Undergraduate'];
	$scope.relationOptions = ['Father', 'Mother', 'Brother', 'Sister', 'Child']; /*spouse is separated because it needs to be dynamic(hide if single)*/




	// fetch applicantInfo
	var populate = {
		0: 'beneficiaries', 
		1: 'characterReferences', 
		2: 'education', 
		3: 'emergencyContact', 
		4: 'parent', 
		5: 'skills', 
		6: 'spouse', 
		7: 'workHistories',
		'interview': {
			0: 'interview'
		}
	};
	$scope.loadingProperties.message = 'Loading applicant\'s info...';
	$scope.loadingProperties.promise = $http.post(httpApplicant.read, { id: $state.params.id, populate: populate })
	.success(function(data, status) {
		$scope.applicant = data;
	});





	$scope.backToMembershipApplicantInfo = function() {
		$state.go('admin.membershipApplicantInfo', { id: $scope.applicant.id });
	};




	// function for add/remove field
	$scope.addSkillField = function() {
		$scope.applicant.skills.push({ skill: '' });
	};
	$scope.removeSkillField = function(key) {
		if($scope.applicant.skills.length > 1) {
			$scope.applicant.skills.splice(key, 1);
		}
	};

	$scope.addWorkHistoryField = function() {
		$scope.applicant.workHistories.push({ skill: '' });
	};
	$scope.removeWorkHistoryField = function(key) {
		if($scope.applicant.workHistories.length > 1) {
			$scope.applicant.workHistories.splice(key, 1);
		}
	};

	$scope.addBeneficiaryField = function() {
		if($scope.applicant.beneficiaries.length < 2) {
			$scope.applicant.beneficiaries.push({ skill: '' });
		}
	};
	$scope.removeBeneficiaryField = function(key) {
		if($scope.applicant.beneficiaries.length > 1) {
			$scope.applicant.beneficiaries.splice(key, 1);
		}
	};

	$scope.addCharacterReferenceField = function() {
		$scope.applicant.characterReferences.push({ skill: '' });
	};
	$scope.removeCharacterReferenceField = function(key) {
		if($scope.applicant.characterReferences.length > 3) {
			$scope.applicant.characterReferences.splice(key, 1);
		}
	};




	$scope.cancelEditConfirm = function() {
		var confirm = $mdDialog.confirm()
			.title('Cancel edit?')
			.textContent('This action will discard all unsaved changes!')
			.ok('Proceed')
			.cancel('Close');
		$mdDialog.show(confirm).then(function() {
			$scope.backToMembershipApplicantInfo();
		});
	};





	$scope.submitMembershipApplicantEditForm = function(form) {
		// check for errors first
		// set all fields to "touched"
		angular.forEach(form.$error, function(field) {
			angular.forEach(field, function(errorField) {
				errorField.$setTouched();
			});
		});

		// alert error message if errors detected
		// else submit the form
		if(!utilityFunctions.isEmptyObject(form.$error)) {
			var alert = $mdDialog.alert()
				.title('Errors detected!')
				.textContent('See and fix the errors to save changes!')
				.ok('Close')
			$mdDialog.show(alert);
		} else {
			$scope.loadingProperties.message = 'Saving changes...';
			$scope.loadingProperties.promise = $http.post(httpApplicant.update, { info: $scope.applicant })
			.success(function(data ,status) {
				var toast = $mdToast.simple()
					.textContent('Changes Saved!')
					.position('bottom right');
				$mdToast.show(toast);
				$state.go('admin.membershipApplicantInfo', { id: $scope.applicant.id });
			});
		}
	}




	$scope.changePicture = function() {
		$state.go('admin.membershipApplicantChangePicture', { id: $scope.applicant.id });
	};





}]);