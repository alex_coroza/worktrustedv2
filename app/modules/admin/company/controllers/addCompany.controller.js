angular.module('companyModule')
.controller('addCompanyController', ['$q', '$scope', '$rootScope', '$http', '$state', '$mdDialog', '$mdToast', 'regexPattern', 'httpCompany', 'httpAccount', 'utilityFunctions', function($q, $scope, $rootScope, $http, $state, $mdDialog, $mdToast, regexPattern, httpCompany, httpAccount, utilityFunctions) {
	console.log('addCompanyController initialized!');

	$rootScope.$emit('activeLinkValueUpdate', { value: 'companies' });
	
	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();


	$scope.company = {
		photo: 'default-avatar.jpg'
	};

	$scope.account = {};


	$scope.loadingProperties = {
		promise: null,
	};	




	$scope.cancelAddConfirm = function() {
		var confirm = $mdDialog.confirm()
			.title('Discard changes?')
			.textContent('This action will discard all unsaved changes!')
			.ok('Proceed')
			.cancel('Close');
		$mdDialog.show(confirm).then(function() {
			$state.go('admin.companyList');
		});
	};



	$scope.submitCompanyAddForm = function(form) {
		// check for errors first
		// set all fields to "touched"
		angular.forEach(form.$error, function(field) {
			angular.forEach(field, function(errorField) {
				errorField.$setTouched();
			});
		});

		// alert error message if errors detected
		// else submit the form
		if(!utilityFunctions.isEmptyObject(form.$error)) {
			var alert = $mdDialog.alert()
				.title('Errors detected!')
				.textContent('See and fix the errors to save changes!')
				.ok('Close')
			$mdDialog.show(alert);
		} else {
			// company to database
			$scope.loadingProperties.message = 'Adding company to database...';
			$scope.loadingProperties.promise = $http.post(httpCompany.save, { info: $scope.company })
			.then(function(response) {
				$scope.account.companyId = response.data;
				// create account
				$scope.loadingProperties.message = 'Creating account...';
				return $scope.loadingProperties.promise = $http.post(httpAccount.create, { type: 'company', info: $scope.account });
			}).then(function(response) {
				var toast = $mdToast.simple()
					.textContent('Changes saved!')
					.position('bottom right')
					.action('View');
				$mdToast.show(toast).then(function(response) {
					if(response == 'ok') {
						$state.go('admin.companyInfo', { id: $scope.account.companyId });
					} else {
						$state.go('admin.companyList');
					}
				});
			}).catch(function(errorMessage) {
				var toast = $mdToast.simple()
					.textContent(errorMessage)
					.position('bottom right');
				$mdToast.show(toast);
			});
		}
	}




}]);