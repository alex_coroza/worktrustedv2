angular.module('companyModule')
.controller('companyInfoController', ['$scope', '$rootScope', '$http', '$state', '$mdDialog', '$mdToast', 'httpCompany', 'httpAccount', 'utilityFunctions', function($scope, $rootScope, $http, $state, $mdDialog, $mdToast, httpCompany, httpAccount, utilityFunctions) {
	console.log('companyInfoController initialized!');

	$rootScope.$emit('activeLinkValueUpdate', { value: 'companies' });
	

	$scope.company;


	$scope.loadingProperties = {
		promise: null,
	};	


	// fetch companyInfo
	var populate = {
		account: {},
		jobOpenings: {
			applicants: {}
		}
	};


	$scope.loadingProperties.message = 'Loading company profile info...';
	$scope.loadingProperties.promise = $http.post(httpCompany.read, { id: $state.params.id, populate: populate })
	.success(function(data, status) {
		$scope.company = data;
	});



	$scope.backToCompanyList = function() {
		$state.go('admin.companyList');
	};




	$scope.changePicture = function() {
		$state.go('admin.companyChangePicture', { id: $scope.company.id });
	};
	



	$scope.resetPassword = function() {
		$scope.loadingProperties.message = 'Resetting password...';
		$scope.loadingProperties.promise = $http.post(httpAccount.resetPassword, { type: 'company', info: $scope.company.account })
		.success(function(data, status) {
			var toast = $mdToast.simple()
				.textContent('Reset password successful!')
				.position('bottom right');
			$mdToast.show(toast);
		});
	};



	$scope.resetPasswordConfirm = function() {
		var confirm = $mdDialog.confirm()
			.title('Are you sure to reset this company\'s account password?')
			.textContent('This will reset the password equivalent to username!')
			.ok('OK')
			.cancel('Close');
		$mdDialog.show(confirm).then(function() {
			$scope.resetPassword();
		});
	};




	$scope.editCompany = function() {
		$state.go('admin.companyEdit', { id: $scope.company.id });
	};





}]);