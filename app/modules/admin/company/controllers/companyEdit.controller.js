angular.module('companyModule')
.controller('companyEditController', ['$scope', '$rootScope', '$http', '$state', '$mdDialog', '$mdToast', 'regexPattern', 'httpCompany', 'httpAccount', 'utilityFunctions', function($scope, $rootScope, $http, $state, $mdDialog, $mdToast, regexPattern, httpCompany, httpAccount, utilityFunctions) {
	console.log('companyEditController initialized!');

	$rootScope.$emit('activeLinkValueUpdate', { value: 'companies' });
	
	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();


	$scope.company;


	$scope.loadingProperties = {
		promise: null,
	};	


	// fetch companyInfo
	var populate = {
		account: {},
		jobOpenings: {
			applicants: {}
		}
	};


	$scope.loadingProperties.message = 'Loading company profile info...';
	$scope.loadingProperties.promise = $http.post(httpCompany.read, { id: $state.params.id, populate: populate })
	.success(function(data, status) {
		$scope.company = data;
	});




	$scope.changePicture = function() {
		$state.go('admin.companyChangePicture', { id: $scope.company.id });
	};





	$scope.resetPassword = function() {
		$scope.loadingProperties.message = 'Resetting password...';
		$scope.loadingProperties.promise = $http.post(httpAccount.resetPassword, { type: 'company', info: $scope.company.account })
		.success(function(data, status) {
			var toast = $mdToast.simple()
				.textContent('Reset password successful!')
				.position('bottom right');
			$mdToast.show(toast);
		});
	};



	$scope.resetPasswordConfirm = function() {
		var confirm = $mdDialog.confirm()
			.title('Are you sure to reset this company\'s account password?')
			.textContent('This will reset the password equivalent to username!')
			.ok('OK')
			.cancel('Close');
		$mdDialog.show(confirm).then(function() {
			$scope.resetPassword();
		});
	};




	$scope.cancelEditConfirm = function() {
		var confirm = $mdDialog.confirm()
			.title('Cancel edit?')
			.textContent('This action will discard all unsaved changes!')
			.ok('Proceed')
			.cancel('Close');
		$mdDialog.show(confirm).then(function() {
			$state.go('admin.companyInfo', { id: $scope.company.id });
		});
	};



	$scope.submitCompanyEditForm = function(form) {
		// check for errors first
		// set all fields to "touched"
		angular.forEach(form.$error, function(field) {
			angular.forEach(field, function(errorField) {
				errorField.$setTouched();
			});
		});

		// alert error message if errors detected
		// else submit the form
		if(!utilityFunctions.isEmptyObject(form.$error)) {
			var alert = $mdDialog.alert()
				.title('Errors detected!')
				.textContent('See and fix the errors to save changes!')
				.ok('Close')
			$mdDialog.show(alert);
		} else {
			$scope.loadingProperties.message = 'Saving changes...';
			$scope.loadingProperties.promise = $http.post(httpCompany.save, { info: $scope.company })
			.success(function(data ,status) {
				var toast = $mdToast.simple()
					.textContent('Changes Saved!')
					.position('bottom right');
				$mdToast.show(toast);
				$state.go('admin.companyInfo', { id: $scope.company.id });
			});
		}
	}




}]);