angular.module('companyModule')
.controller('companyListController', ['$scope', '$rootScope', '$timeout', '$http', '$state', '$stateParams', '$mdDialog', 'httpCompany', function($scope, $rootScope, $timeout, $http, $state, $stateParams, $mdDialog, httpCompany) {
	console.log('companyListController initialized!');

	$rootScope.$emit('activeLinkValueUpdate', { value: 'companies' });


	$scope.searchKeyword = '';
	$scope.companies = [];

	$scope.loadingProperties = {
		promise: null,
	};



	$scope.reloadData = function() {
		$scope.loadCompanies();
	};



	$scope.loadCompanies = function() {
		var options = {
			populate: {}
		};

		// search condition
		if($scope.searchKeyword.trim() != '') options.search = $scope.searchKeyword;

		$scope.loadingProperties.promise = $http.post(httpCompany.enlist, options)
		.success(function(data ,status) {
			$scope.companies = data;
		});
	};




	$scope.companyInfo = function(info) {
		$state.go('admin.companyInfo', { id: info.id });
	};





	// watch for $scope.searchKeyword
	// update list based on searchKeyword
	var searchDelay;
	$scope.$watch('searchKeyword', function(newValue, oldValue) {
		if(newValue.trim() != oldValue.trim()) {
			// fetch new records
			if(searchDelay) $timeout.cancel(searchDelay);

			searchDelay = $timeout(function() {
				$scope.loadCompanies();
			}, 1000);
		}
	});




	$scope.addCompany = function() {
		$state.go('admin.addCompany');
	};










	// fetch all members on page load
	$scope.loadCompanies();







}]);