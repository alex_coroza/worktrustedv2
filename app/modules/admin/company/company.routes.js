angular.module('companyModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('admin.companyList', {
			url: 'company-list/',
			templateUrl: DOMAIN+'app/modules/admin/company/views/companyList.view.html',
			controller: 'companyListController'
		})
		.state('admin.companyInfo', {
			url: 'company-info/{id}',
			templateUrl: DOMAIN+'app/modules/admin/company/views/companyInfo.view.html',
			controller: 'companyInfoController'
		})
		.state('admin.companyEdit', {
			url: 'company-edit/{id}',
			templateUrl: DOMAIN+'app/modules/admin/company/views/companyEdit.view.html',
			controller: 'companyEditController'
		})
		.state('admin.addCompany', {
			url: 'add-company',
			templateUrl: DOMAIN+'app/modules/admin/company/views/addCompany.view.html',
			controller: 'addCompanyController'
		})
		.state('admin.companyChangePicture', {
			url: 'company-changePicture/{id}',
			templateUrl: DOMAIN+'app/modules/admin/company/views/companyChangePicture.view.html',
			controller: 'companyChangePictureController'
		})
	; /*closing $stateProvider*/
}]);

				