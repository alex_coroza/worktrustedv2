angular.module('jobsModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('admin.jobOpeningList', {
			url: 'job-openings/',
			templateUrl: DOMAIN+'app/modules/admin/jobs/views/jobOpeningList.view.html',
			controller: 'jobOpeningListController'
		})
		.state('admin.addJobOpening', {
			url: 'add-job-opening/',
			templateUrl: DOMAIN+'app/modules/admin/jobs/views/addJobOpening.view.html',
			controller: 'addJobOpeningController'
		})
		.state('admin.jobOpeningInfo', {
			url: 'job-opening/{id}',
			templateUrl: DOMAIN+'app/modules/admin/jobs/views/jobOpeningInfo.view.html',
			controller: 'jobOpeningInfoController'
		})
		.state('admin.jobOpeningEdit', {
			url: 'job-opening-edit/{id}',
			templateUrl: DOMAIN+'app/modules/admin/jobs/views/jobOpeningEdit.view.html',
			controller: 'jobOpeningEditController'
		})
	; /*closing $stateProvider*/
}]);

				