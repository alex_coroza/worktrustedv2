angular.module('jobsModule')
.controller('addJobOpeningController', ['$scope', '$rootScope', '$http', '$state', '$mdDialog', '$mdToast', 'httpJob', 'httpCompany', 'utilityFunctions', 'formUtilities', 'regexPattern', 'user', function($scope, $rootScope, $http, $state, $mdDialog, $mdToast, httpJob, httpCompany, utilityFunctions, formUtilities, regexPattern, user) {
	console.log('addJobOpeningController initialized!');

	$scope.user = user;

	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();

	$scope.industryTypeOptions = [
		'Accounting',
		'Human Resources',
		'Media',
		'Communications',
		'Arts',
		'Construction',
		'Real Estate',
		'Computer - Hardware',
		'Computer - Software',
		'Computer - Other',
		'Education',
		'Engineering',
		'Healthcare - Doctor',
		'Healthcare - Pharmacy',
		'Healthcare - Medical Support',
		'Hotel and Restaurant',
		'Manufacturing',
		'Sales and Marketing',
		'Services',
		'Others'
	];

	$scope.loadingProperties = {
		promise: null,
	};

	$scope.companies = [];
	$scope.statusOptions = ['Posted', 'Pending', 'Expired'];
	$scope.employmentTypeOptions = ['Regular', 'Contractual'];
	$scope.noSalaryRange = false;



	$scope.jobOpening = {
		status: 'Pending',
		validityDate: new Date()
	};




	$scope.loadCompanies = function() {
		$scope.loadingProperties.message = 'Loading companies...';
		$scope.loadingProperties.promise = $http.post(httpCompany.enlist, {})
		.success(function(data ,status) {
			$scope.companies = data;
		});
	};




	$scope.saveJobOpening = function() {
		// no salaryRange condition
		if($scope.noSalaryRange) {
			$scope.jobOpening.minSalary = 0;
			$scope.jobOpening.maxSalary = 0;
		}

		$scope.loadingProperties.message = 'Saving changes...';
		$scope.loadingProperties.promise = $http.post(httpJob.saveJobOpening, { info: $scope.jobOpening })
		.success(function(data, status) {
			var jobOpeningId = data;
			var toast = $mdToast.simple()
				.textContent('Changes saved!')
				.position('bottom right')
				.action('View');
			$mdToast.show(toast).then(function(response) {
				if(response == 'ok') {
					$state.go('admin.jobOpeningInfo', { id: jobOpeningId });
				} else {
					$state.go('admin.jobOpeningList');
				}
			});
		});
	};





	$scope.submitForm = function(form) {
		formUtilities.formErrorChecker(form);

		if(!utilityFunctions.isEmptyObject(form.$error)) {
			var toast = $mdToast.simple()
				.textContent('Errors detected!')
				.position('bottom right');
			$mdToast.show(toast);
		} else {
			$scope.saveJobOpening();
		}
	};



	$scope.cancelAdd = function() {
		var confirm = $mdDialog.confirm()
			.title('Discard changes?')
			.textContent('This action will discard all unsaved changes and will take you back to job opening list!')
			.ok('Proceed')
			.cancel('Close');
		$mdDialog.show(confirm).then(function() {
			$state.go('admin.jobOpeningList');
		});
	};




	$scope.loadCompanies();



}]);