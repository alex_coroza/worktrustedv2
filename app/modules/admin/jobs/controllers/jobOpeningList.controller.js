angular.module('jobsModule')
.controller('jobOpeningListController', ['$scope', '$rootScope', '$http', '$timeout', '$state', '$mdDialog', '$mdToast', 'httpJob', 'utilityFunctions', 'user', function($scope, $rootScope, $http, $timeout, $state, $mdDialog, $mdToast, httpJob, utilityFunctions, user) {
	console.log('jobOpeningListController initialized!');

	$rootScope.$emit('activeLinkValueUpdate', { value: 'jobs' });

	$scope.user = user;
	$scope.jobOpenings = [];

	$scope.filters = {
		companyId: 'All',
		status: 'All',
		search: '',
		populate: {
			applicants: {},
			company: {}
		}
	};

	$scope.loadingProperties = {
		promise: null,
	};



	$scope.reloadData = function() {
		$scope.loadJobOpenings();
	};



	$scope.loadJobOpenings = function() {
		// unset filters on default value
		if($scope.filters.search == '') delete $scope.filters.search;
		if($scope.filters.companyId == 'All') delete $scope.filters.companyId;
		if($scope.filters.status == 'All') delete $scope.filters.status;
		if($scope.filters.employmentType == 'All') delete $scope.filters.employmentType;
		if($scope.filters.industryType == 'All') delete $scope.filters.industryType;
		if($scope.filters.expectedSalary == '') delete $scope.filters.expectedSalary;

		var processedFilters = angular.copy($scope.filters);

		if(!$scope.filters.companyId) $scope.filters.companyId = 'All';
		if(!$scope.filters.status) $scope.filters.status = 'All';
		if(!$scope.filters.employmentType) $scope.filters.employmentType = 'All';
		if(!$scope.filters.industryType) $scope.filters.industryType = 'All';

		$scope.loadingProperties.promise = $http.post(httpJob.enlistJobOpenings, processedFilters)
		.success(function(data ,status) {
			$scope.jobOpenings = data;
		});
	};




	$scope.jobOpeningInfo = function(info) {
		$state.go('admin.jobOpeningInfo', { id: info.id });
	};




	// update list based on search
	var searchDelay;
	$scope.searchChange = function() {
		// fetch new records
		if(searchDelay) $timeout.cancel(searchDelay);

		searchDelay = $timeout(function() {
			$scope.loadJobOpenings();
		}, 1000);
	};




	$scope.addJobOpening = function() {
		$state.go('admin.addJobOpening');
	};





	// display advanced search dialog
	$scope.showAdvancedSearch = function() {
		$mdDialog.show({
			templateUrl: DOMAIN+'app/modules/admin/jobs/views/jobOpeningList/advancedSearch.tpl.html',
			controller: 'advancedSearchController',
			locals: { filters: $scope.filters }
		})
		.then(function(response) {
			$scope.filters = response;
			$scope.loadJobOpenings();
		});
	};










	// fetch all members on page load
	$scope.loadJobOpenings();





}]);