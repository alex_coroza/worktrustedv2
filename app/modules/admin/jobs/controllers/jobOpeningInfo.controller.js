angular.module('jobsModule')
.controller('jobOpeningInfoController', ['$scope', '$rootScope', '$http', '$timeout', '$state', '$mdDialog', '$mdToast', 'httpJob', 'utilityFunctions', 'user', function($scope, $rootScope, $http, $timeout, $state, $mdDialog, $mdToast, httpJob, utilityFunctions, user) {
	console.log('jobOpeningInfoController initialized!');

	$rootScope.$emit('activeLinkValueUpdate', { value: 'jobs' });

	$scope.user = user;
	$scope.jobOpening;

	$scope.loadingProperties = {
		promise: null,
	};


	$scope.loadJobOpening = function() {
		var readOptions = {
			id: $state.params.id, 
			populate: {
				applicants: {
					member: {}
				},
				company: {}
			}
		};

		$scope.loadingProperties.message = 'Loading job opening\'s info...';
		$scope.loadingProperties.promise = $http.post(httpJob.readJobOpening, readOptions)
		.success(function(data, status) {
			$scope.jobOpening = data;
		});
	};




	$scope.backToJobOpeningList = function() {
		$state.go('admin.jobOpeningList');
	};



	$scope.memberInfo = function(member) {
		$state.go('admin.memberInfo', { id: member.id });
	};



	$scope.jobOpeningEdit = function() {
		$state.go('admin.jobOpeningEdit', { id: $scope.jobOpening.id });
	};




	$scope.loadJobOpening();





}]);