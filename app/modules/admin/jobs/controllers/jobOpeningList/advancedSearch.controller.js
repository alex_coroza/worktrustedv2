angular.module('jobsModule')
.controller('advancedSearchController', ['$scope', '$rootScope', '$http', '$state', '$mdDialog', '$mdToast', 'httpCompany', 'utilityFunctions', 'formUtilities', 'regexPattern', 'filters', function($scope, $rootScope, $http, $state, $mdDialog, $mdToast, httpCompany, utilityFunctions, formUtilities, regexPattern, filters) {
	console.log('advancedSearchController initialized!');

	$scope.filters = angular.copy(filters);

	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();

	$scope.industryOptions = [
		'Accounting',
		'Human Resources',
		'Media',
		'Communications',
		'Arts',
		'Construction',
		'Real Estate',
		'Computer - Hardware',
		'Computer - Software',
		'Computer - Other',
		'Education',
		'Engineering',
		'Healthcare - Doctor',
		'Healthcare - Pharmacy',
		'Healthcare - Medical Support',
		'Hotel and Restaurant',
		'Manufacturing',
		'Sales and Marketing',
		'Services',
		'Others'
	];

	$scope.statusOptions = ['All', 'Posted', 'Pending', 'Expired'];
	$scope.employmentTypeOptions = ['All', 'Regular', 'Contractual'];
	$scope.industryTypeOptions = {};

	$scope.companies = [
		{ id: 'All', name: 'All' }
	];


	$scope.loadingProperties = {
		promise: null
	};


	$scope.clearFilters = function() {
		$scope.filters.search = '';
		$scope.filters.companyId = 'All';
		$scope.filters.status = 'All';
		$scope.filters.employmentType = 'All';
		$scope.filters.industryType = 'All';
		$scope.filters.expectedSalary = '';
	};



	$scope.dismiss = function() {
		$mdDialog.cancel();
	};



	$scope.search = function(form) {
		formUtilities.formErrorChecker(form);

		if(!utilityFunctions.isEmptyObject(form.$error)) {
			var toast = $mdToast.simple()
				.textContent('Errors detected!')
				.position('bottom right');
			$mdToast.show(toast);
		} else {
			$mdDialog.hide($scope.filters);
		}
	};




	$scope.loadCompanies = function() {
		$scope.loadingProperties.message = 'Loading companies...';
		$scope.loadingProperties.promise = $http.post(httpCompany.enlist, {})
		.success(function(data ,status) {
			$scope.companies = $scope.companies.concat(data);
		});
	};




	$scope.loadCompanies();





}]);