angular.module('jobsModule')
.controller('jobOpeningEditController', ['$scope', '$rootScope', '$http', '$state', '$mdDialog', '$mdToast', 'httpJob', 'utilityFunctions', 'formUtilities', 'regexPattern', 'user', function($scope, $rootScope, $http, $state, $mdDialog, $mdToast, httpJob, utilityFunctions, formUtilities, regexPattern, user) {
	console.log('jobOpeningEditController initialized!');

	$scope.user = user;

	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();

	$scope.industryTypeOptions = [
		'Accounting',
		'Human Resources',
		'Media',
		'Communications',
		'Arts',
		'Construction',
		'Real Estate',
		'Computer - Hardware',
		'Computer - Software',
		'Computer - Other',
		'Education',
		'Engineering',
		'Healthcare - Doctor',
		'Healthcare - Pharmacy',
		'Healthcare - Medical Support',
		'Hotel and Restaurant',
		'Manufacturing',
		'Sales and Marketing',
		'Services',
		'Others'
	];

	$scope.loadingProperties = {
		promise: null,
	};

	$scope.statusOptions = ['Posted', 'Pending', 'Expired'];
	$scope.employmentTypeOptions = ['Regular', 'Contractual'];
	$scope.noSalaryRange = false;



	
	$scope.loadingProperties.message = 'Loadning job opening\'s info...';
	var readOptions = {
		id: $state.params.id,
		populate: {
			applicants: {
				member: {}
			}
		}
	};
	$scope.loadingProperties.promise = $http.post(httpJob.readJobOpening, readOptions)
	.success(function(data, status) {
		$scope.jobOpening = data;
		console.log(data);

		// check salarRange status
		if($scope.jobOpening.minSalary == 0 && $scope.jobOpening.maxSalary == 0) {
			$scope.noSalaryRange = true;
		}
	});



	$scope.saveJobOpening = function() {
		// no salaryRange condition
		if($scope.noSalaryRange) {
			$scope.jobOpening.minSalary = 0;
			$scope.jobOpening.maxSalary = 0;
		}

		$scope.loadingProperties.message = 'Saving changes...';
		$scope.loadingProperties.promise = $http.post(httpJob.saveJobOpening, { info: $scope.jobOpening })
		.success(function(data, status) {
			var jobOpeningId = data;
			var toast = $mdToast.simple()
				.textContent('Changes saved!')
				.position('bottom right')
			$mdToast.show(toast);
			$state.go('admin.jobOpeningInfo', { id: jobOpeningId });
		});
	};





	$scope.submitForm = function(form) {
		formUtilities.formErrorChecker(form);

		if(!utilityFunctions.isEmptyObject(form.$error)) {
			var toast = $mdToast.simple()
				.textContent('Errors detected!')
				.position('bottom right');
			$mdToast.show(toast);
		} else {
			$scope.saveJobOpening();
		}
	};



	$scope.cancelEdit = function() {
		var confirm = $mdDialog.confirm()
			.title('Discard changes?')
			.textContent('This action will discard all unsaved changes and will take you back to job opening list!')
			.ok('Proceed')
			.cancel('Close');
		$mdDialog.show(confirm).then(function() {
			$state.go('admin.jobOpeningInfo', { id: $state.params.id });
		});
	};



}]);