angular.module('customValidate.service', []);

angular.module('customValidate.service')
.factory('inputValidate', [function() {
	this.alpha = function(sample) {
		if(sample.match(/^[a-zA-ZñÑ ]+$/) || sample == '')
			return true;
		else
			return false;
	};

	this.numeric = function(sample) {
		if(sample.match(/^[0-9]+$/) || sample == '')
			return true;
		else
			return false;
	};

	this.alphanumeric = function(sample) {
		if(sample.match(/^[a-zA-Z0-9ñÑ ]+$/) || sample == '')
			return true;
		else
			return false;
	};

	this.required = function(sample) {
		if(sample.trim() != '')
			return true;
		else
			return false;
	};

	this.email = function(sample) {
		if(sample.match(/^[\w\.\-]+@([\w\-]+\.)+[a-z]+$/i) || sample == '')
			return true;
		else
			return false;
	};


	return this;
}]);
