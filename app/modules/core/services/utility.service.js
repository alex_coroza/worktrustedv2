angular.module('utility.service', []);

// checks if there is currently running http request
angular.module('utility.service')
.factory('isLoading', ['$http', function($http) {
	
	this.exec = function() {
		return $http.pendingRequests.length !== 0;
	};

	return this;

}]);


// sets of utility functions
angular.module('utility.service')
.factory('utilityFunctions', [function() {
	
	// Returns a shuffled object/array
	this.suffleArray = function(param) {
		var sourceArray = param.slice(0);
		var result;
		for (var n = 0; n < sourceArray.length - 1; n++) {
			var k = n + Math.floor(Math.random() * (sourceArray.length - n));

			var temp = sourceArray[k];
			sourceArray[k] = sourceArray[n];
			sourceArray[n] = temp;
		}
		return sourceArray;
	};

	// the same as in_array() function in php, for arrays only, not applicable for objects
	this.inArray = function(needle, haystack) {
		var length = haystack.length;
		for(var i = 0; i < length; i++) {
			if(haystack[i] == needle) return true;
		}
		return false;
	};

	// determines wether an object is empty or not
	this.isEmptyObject = function(obj) {
		for(var prop in obj) {
			if(obj.hasOwnProperty(prop))
				return false;
		}
		return true;
	};

	return this;

}]);





// set of regex for input validation
angular.module('utility.service')
.factory('regexPattern', [function() {
	
	this.alpha = function() {
		return /^[a-zA-ZñÑ ]+$/;
	};

	this.alphanumeric = function() {
		return /^[a-zA-Z0-9ñÑ ]+$/;
	};

	this.numeric = function() {
		return /^[0-9 ]+$/;
	};

	this.alphanumericSymbol = function() {
		return /^[a-zA-Z0-9ñÑ ,-._#&!?]+$/;
	};

	return this;

}]);



// store data using this then you can retrieve that data from another controller from this service
angular.module('utility.service')
.factory('dataStorage', [function() {
	this.data = false;

	this.store = function(data) {
		this.data = data;
		return true;
	};

	this.fetch = function() {
		var copy = angular.copy(this.data);
		this.data = false;
		return copy;
	};


	return this;
}]);





// utility functions for form error handling
angular.module('utility.service')
.factory('formUtilities', [function() {

	// toggles custom error for ng-messages
	// returns the form with new error statuses
	this.inputErrorToggle = function(form, inputName, errorType, mode) {
		if(mode == 'errorOn') {
			form[inputName].$error[errorType] = true;
			form[inputName].$invalid = true;
			form[inputName].$touched = true;
		} else if(mode == 'errorOff') {
			form[inputName].$error[errorType] = false;
			form[inputName].$invalid = false;
			form[inputName].$valid = true;
		}
	};



	// check for form errors
	// set all fields to "touched"
	this.formErrorChecker = function(form) {
		angular.forEach(form.$error, function(field) {
			angular.forEach(field, function(errorField) {
				errorField.$setTouched();
			});
		});
	};



	return this;
}]);



