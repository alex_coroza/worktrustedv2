angular.module('http.service', [])


.factory('httpApplicant', [function() {
	this.enlist = DOMAIN+'applicant/enlist';
	this.read = DOMAIN+'applicant/read';
	this.create = DOMAIN+'applicant/create';
	this.update = DOMAIN+'applicant/update';
	this.delete = DOMAIN+'applicant/delete';
	this.permanentDelete = DOMAIN+'applicant/permanentDelete';
	this.applicantPictureUpdate = DOMAIN+'applicant/applicantPictureUpdate';
	this.uploadBase64ProfilePicture = DOMAIN+'applicant/uploadBase64ProfilePicture';
	this.addInterviewSchedule = DOMAIN+'applicant/addInterviewSchedule';

	return this;
}])



.factory('httpMember', [function() {
	this.enlist = DOMAIN+'member/enlist';
	this.read = DOMAIN+'member/read';
	this.create = DOMAIN+'member/create';
	this.update = DOMAIN+'member/update';
	this.delete = DOMAIN+'member/delete';
	this.permanentDelete = DOMAIN+'member/permanentDelete';
	this.memberPictureUpdate = DOMAIN+'member/memberPictureUpdate';
	this.uploadBase64ProfilePicture = DOMAIN+'member/uploadBase64ProfilePicture';
	this.addInterviewSchedule = DOMAIN+'member/addInterviewSchedule';

	return this;
}])



.factory('httpJob', [function() {
	this.enlistJobOpenings = DOMAIN+'job/enlistJobOpenings';
	this.saveJobOpening = DOMAIN+'job/saveJobOpening';
	this.readJobOpening = DOMAIN+'job/readJobOpening';


	this.enlistJobApplicants = DOMAIN+'job/enlistJobApplicants';
	this.saveJobApplicant = DOMAIN+'job/saveJobApplicant';
	this.readJobApplicant = DOMAIN+'job/readJobApplicant';
	this.deleteJobApplicant = DOMAIN+'job/deleteJobApplicant';
	// this.create = DOMAIN+'job/create';
	// this.update = DOMAIN+'job/update';
	// this.permanentDelete = DOMAIN+'job/permanentDelete';

	return this;
}])




.factory('httpCompany', [function() {
	this.enlist = DOMAIN+'company/enlist';
	this.read = DOMAIN+'company/read';
	this.save = DOMAIN+'company/save';
	this.delete = DOMAIN+'company/delete';
	this.permanentDelete = DOMAIN+'company/permanentDelete';
	this.companyPictureUpdate = DOMAIN+'company/companyPictureUpdate';
	this.uploadBase64ProfilePicture = DOMAIN+'company/uploadBase64ProfilePicture';

	return this;
}])




.factory('httpAccount', [function() {
	this.login = DOMAIN+'account/login';
	this.logout = DOMAIN+'account/logout';
	this.fetchLoggedUser = DOMAIN+'account/fetchLoggedUser';
	this.save = DOMAIN+'account/save';
	this.create = DOMAIN+'account/create';
	this.resetPassword = DOMAIN+'account/resetPassword';
	this.checkUserName = DOMAIN+'account/checkUserName';
	this.checkPassword = DOMAIN+'account/checkPassword';

	return this;
}])




.factory('httpFeedback', [function() {
	this.enlist = DOMAIN+'feedback/enlist';
	this.read = DOMAIN+'feedback/read';
	this.create = DOMAIN+'feedback/create';
	this.update = DOMAIN+'feedback/update';
	this.delete = DOMAIN+'feedback/delete';
	this.permanentDelete = DOMAIN+'feedback/permanentDelete';

	return this;
}])





.factory('httpSchedule', [function() {
	// schedule for applicantInterview
	this.enlistApplicantInterviewSchedules = DOMAIN+'schedule/enlistApplicantInterviewSchedules';
	this.readApplicantInterviewSchedule = DOMAIN+'schedule/readApplicantInterviewSchedule';
	this.saveApplicantInterviewSchedule = DOMAIN+'schedule/saveApplicantInterviewSchedule';
	this.deleteApplicantInterviewSchedule = DOMAIN+'schedule/deleteApplicantInterviewSchedule';

	// schedule for trainings
	this.enlistTrainingEvents = DOMAIN+'schedule/enlistTrainingEvents';
	this.readTrainingEvent = DOMAIN+'schedule/readTrainingEvent';
	this.saveTrainingEvent = DOMAIN+'schedule/saveTrainingEvent';
	this.deleteTrainingEvent = DOMAIN+'schedule/deleteTrainingEvent';

	// sample events
	this.enlistSampleSchedules = DOMAIN+'schedule/enlistSampleSchedules';

	return this;
}])





.factory('httpTraining', [function() {
	this.enlist = DOMAIN+'training/enlist';
	this.read = DOMAIN+'training/read';
	this.create = DOMAIN+'training/create';
	this.update = DOMAIN+'training/update';
	this.delete = DOMAIN+'training/delete';
	this.permanentDelete = DOMAIN+'training/permanentDelete';
	// this.memberPictureUpdate = DOMAIN+'member/memberPictureUpdate';
	// this.uploadBase64ProfilePicture = DOMAIN+'member/uploadBase64ProfilePicture';
	// this.addInterviewSchedule = DOMAIN+'member/addInterviewSchedule';

	return this;
}])







; /*closing semi-colon*/
