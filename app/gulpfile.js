/* File: gulpfile.js */

// grab our gulp packages
var gulp  = require('gulp');
var gutil = require('gulp-util');
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var sass = require('gulp-sass');


gulp.task('default', function() {
	console.log('compileSass -> compiles sass/scss files');
	console.log('watchCompileSass -> auto compile sass/scss files');
	console.log('concatMinifyModules -> concats and minifies js file (still experimental)');
});






// ADMIN MODULE FILES
gulp.task('concatMinifyModulesAdmin', function () {
	gulp.src([
		'modules/admin/**/*.js',
		'modules/core/**/*.js'
	])
	.pipe(uglify())
	.pipe(concat('adminModules.js'))
	.pipe(gulp.dest(''));
});


gulp.task('concatMinifyAdminVendors', function () {
	gulp.src([
		// core js files
		'assets/bower_components/jquery/dist/jquery.min.js',
		'assets/bower_components/angular/angular.js',
		'assets/bower_components/angular-aria/angular-aria.min.js',
		'assets/bower_components/angular-messages/angular-messages.min.js',
		'assets/bower_components/angular-animate/angular-animate.min.js',
		'assets/bower_components/angular-material/angular-material.js',
		'assets/bower_components/angular-ui-router/release/angular-ui-router.js',

		// 3rd party utility scripts
		'assets/bower_components/moment/moment.js',
		'assets/bower_components/angular-moment/angular-moment.js',

		// files for ng-file-upload
		'assets/bower_components/ng-file-upload/ng-file-upload.min.js',

		// files for ng-camera 
		'assets/bower_components/webcamjs/webcam.min.js',
		'assets/bower_components/ng-camera/dist/ng-camera.js',

		// files for angular ui calendar
		'assets/bower_components/angular-ui-calendar/src/calendar.js',
		'assets/bower_components/fullcalendar/dist/fullcalendar.min.js',

		// other 3rd party scripts
		'assets/bower_components/angular-busy/dist/angular-busy.js'
	])
	.pipe(uglify())
	.pipe(concat('adminVendors.js'))
	.pipe(gulp.dest(''));
});






// LINE LEADER MODULE FILES
gulp.task('concatMinifyModulesLineLeader', function () {
	gulp.src([
		'modules/lineLeader/**/*.js',
		'modules/core/**/*.js'
	])
	.pipe(uglify())
	.pipe(concat('lineLeaderModules.js'))
	.pipe(gulp.dest(''));
});


gulp.task('concatMinifyLineLeaderVendors', function () {
	gulp.src([
		// core js files
		'assets/bower_components/jquery/dist/jquery.min.js',
		'assets/bower_components/angular/angular.js',
		'assets/bower_components/angular-aria/angular-aria.min.js',
		'assets/bower_components/angular-messages/angular-messages.min.js',
		'assets/bower_components/angular-animate/angular-animate.min.js',
		'assets/bower_components/angular-material/angular-material.js',
		'assets/bower_components/angular-ui-router/release/angular-ui-router.js',

		// 3rd party utility scripts
		'assets/bower_components/moment/moment.js',
		'assets/bower_components/angular-moment/angular-moment.js',

		// files for ng-file-upload
		'assets/bower_components/ng-file-upload/ng-file-upload.min.js',

		// files for ng-camera 
		'assets/bower_components/webcamjs/webcam.min.js',
		'assets/bower_components/ng-camera/dist/ng-camera.js',

		// files for angular ui calendar
		'assets/bower_components/angular-ui-calendar/src/calendar.js',
		'assets/bower_components/fullcalendar/dist/fullcalendar.min.js',

		// other 3rd party scripts
		'assets/bower_components/angular-busy/dist/angular-busy.js'
	])
	.pipe(uglify())
	.pipe(concat('lineLeaderVendors.js'))
	.pipe(gulp.dest(''));
});










// MEMBER MODULE FILES
gulp.task('concatMinifyModulesMember', function () {
	gulp.src([
		'modules/member/**/*.js',
		'modules/core/**/*.js'
	])
	.pipe(uglify())
	.pipe(concat('memberModules.js'))
	.pipe(gulp.dest(''));
});


gulp.task('concatMinifyMemberVendors', function () {
	gulp.src([
		// core js files
		'assets/bower_components/jquery/dist/jquery.min.js',
		'assets/bower_components/angular/angular.js',
		'assets/bower_components/angular-aria/angular-aria.min.js',
		'assets/bower_components/angular-messages/angular-messages.min.js',
		'assets/bower_components/angular-animate/angular-animate.min.js',
		'assets/bower_components/angular-material/angular-material.js',
		'assets/bower_components/angular-ui-router/release/angular-ui-router.js',

		// 3rd party utility scripts
		'assets/bower_components/moment/moment.js',
		'assets/bower_components/angular-moment/angular-moment.js',

		// files for ng-file-upload
		'assets/bower_components/ng-file-upload/ng-file-upload.min.js',

		// files for ng-camera 
		'assets/bower_components/webcamjs/webcam.min.js',
		'assets/bower_components/ng-camera/dist/ng-camera.js',

		// files for angular ui calendar
		'assets/bower_components/angular-ui-calendar/src/calendar.js',
		'assets/bower_components/fullcalendar/dist/fullcalendar.min.js',

		// other 3rd party scripts
		'assets/bower_components/angular-busy/dist/angular-busy.js'
	])
	.pipe(uglify())
	.pipe(concat('memberVendors.js'))
	.pipe(gulp.dest(''));
});







// COMPANY MODULE FILES
gulp.task('concatMinifyModulesCompany', function () {
	gulp.src([
		'modules/company/**/*.js',
		'modules/core/**/*.js'
	])
	.pipe(uglify())
	.pipe(concat('companyModules.js'))
	.pipe(gulp.dest(''));
});


gulp.task('concatMinifyCompanyVendors', function () {
	gulp.src([
		// core js files
		'assets/bower_components/jquery/dist/jquery.min.js',
		'assets/bower_components/angular/angular.js',
		'assets/bower_components/angular-aria/angular-aria.min.js',
		'assets/bower_components/angular-messages/angular-messages.min.js',
		'assets/bower_components/angular-animate/angular-animate.min.js',
		'assets/bower_components/angular-material/angular-material.js',
		'assets/bower_components/angular-ui-router/release/angular-ui-router.js',

		// 3rd party utility scripts
		'assets/bower_components/moment/moment.js',
		'assets/bower_components/angular-moment/angular-moment.js',

		// files for ng-file-upload
		'assets/bower_components/ng-file-upload/ng-file-upload.min.js',

		// files for ng-camera 
		'assets/bower_components/webcamjs/webcam.min.js',
		'assets/bower_components/ng-camera/dist/ng-camera.js',

		// files for angular ui calendar
		'assets/bower_components/angular-ui-calendar/src/calendar.js',
		'assets/bower_components/fullcalendar/dist/fullcalendar.min.js',

		// other 3rd party scripts
		'assets/bower_components/angular-busy/dist/angular-busy.js'
	])
	.pipe(uglify())
	.pipe(concat('companyVendors.js'))
	.pipe(gulp.dest(''));
});













// GUEST MODULE FILES
gulp.task('concatMinifyModulesGuest', function () {
	gulp.src([
		'modules/guest/**/*.js',
		'modules/core/**/*.js'
	])
	.pipe(uglify())
	.pipe(concat('guestModules.js'))
	.pipe(gulp.dest(''));
});


gulp.task('concatMinifyGuestVendors', function () {
	gulp.src([
		// core js files
		'assets/bower_components/angular/angular.js',
		'assets/bower_components/angular-messages/angular-messages.min.js',
		'assets/bower_components/angular-animate/angular-animate.min.js',
		'assets/bower_components/angular-ui-router/release/angular-ui-router.js',
		'assets/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',

		// ui.boostrap extensions
		'assets/bower_components/angular-aside/dist/js/angular-aside.min.js',

		// files from angular strap for scrollspy only
		'assets/bower_components/angular-strap/dist/modules/debounce.min.js',
		'assets/bower_components/angular-strap/dist/modules/dimensions.min.js',
		'assets/bower_components/angular-strap/dist/modules/scrollspy.min.js',

		// files for ng-file-upload
		'assets/bower_components/ng-file-upload/ng-file-upload.min.js',

		// other 3rd party scripts
		'assets/bower_components/angular-busy/dist/angular-busy.js'
	])
	.pipe(uglify())
	.pipe(concat('guestVendors.js'))
	.pipe(gulp.dest(''));
});










// compile scss files for guest.css
gulp.task('compileGuestCss', function() {
	gulp.src(['assets/css/sass/guest/*.scss', 'assets/css/sass/core/bootstrapMods.scss', 'assets/css/sass/core/core.scss'])
		.pipe(sass())
		.pipe(concat('guest.css'))
		.pipe(gulp.dest('assets/css/'));;
});


// compile scss files for admin.css
gulp.task('compileAdminCss', function() {
	gulp.src(['assets/css/sass/admin/*.scss', 'assets/css/sass/core/core.scss'])
		.pipe(sass())
		.pipe(concat('admin.css'))
		.pipe(gulp.dest('assets/css/'));;
});


// compile scss files for member.css
gulp.task('compileMemberCss', function() {
	gulp.src(['assets/css/sass/admin/*.scss', 'assets/css/sass/core/core.scss'])
		.pipe(sass())
		.pipe(concat('member.css'))
		.pipe(gulp.dest('assets/css/'));;
});


// compile scss files for lineLeader.css
gulp.task('compileLineLeaderCss', function() {
	gulp.src(['assets/css/sass/admin/*.scss', 'assets/css/sass/core/core.scss'])
		.pipe(sass())
		.pipe(concat('lineLeader.css'))
		.pipe(gulp.dest('assets/css/'));;
});


// compile scss files for company.css
gulp.task('compileCompanyCss', function() {
	gulp.src(['assets/css/sass/admin/*.scss', 'assets/css/sass/core/core.scss'])
		.pipe(sass())
		.pipe(concat('company.css'))
		.pipe(gulp.dest('assets/css/'));;
});
















// WATCH

// auto compile sass on saving
gulp.task('watchCompileSass', function() {
	gulp.watch('assets/css/sass/**/*.scss', ['compileGuestCss', 'compileAdminCss', 'compileMemberCss', 'compileLineLeaderCss', 'compileCompanyCss']);
});



// auto compile guest and admin modules
gulp.task('watchCompileModules', function() {
	gulp.watch('modules/**/*.js', ['concatMinifyModulesAdmin', 'concatMinifyModulesGuest']);
});
