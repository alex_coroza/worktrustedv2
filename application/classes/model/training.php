<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Training extends Model {


	public function read($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();
		
		$training = ORM::factory('orm_training', $options->id);
		$trainingInfo = new stdClass();
		$trainingInfo->id = $training->id;
		$trainingInfo->name = $training->name;
		$trainingInfo->description = $training->description;

		$populateManager = new Etc_PopulateManager();

		if($populateManager->check('trainingEvents', $options->populate)) {
			$trainingEventModel = new Model_Schedule_TrainingEvent();
			$trainingEventOptions = (object)array('trainingId' => $training->id, 'populate' => $populateManager->currentPopulate);
			$trainingInfo->trainingEvents = $trainingEventModel->enlist($trainingEventOptions);
		}

		return $trainingInfo;
	}





	public function enlist($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$formatString = new Etc_FormatString();
		$trainingOrm = ORM::factory('orm_training');

		// order by
		if(isset($options->orderBy)) {
			$trainingOrm->order_by($options->orderBy[0], $options->orderBy[1]);
		}

		// search
		if(isset($options->search)) {
			$options->search = explode(' ', $formatString->multiFormat($options->search, array('spaceTrim', 'alphaNumericOnly')));
			$trainingOrm->where_open();
			foreach ($options->search as $key => $word) {
				$trainingOrm
				->or_where_open()
				->or_where('name', 'LIKE', "%$word%")
				->or_where('description', 'LIKE', "%$word%")
				->or_where_close();
			}
			$trainingOrm->where_close();
		}

		// limit and offset
		if(isset($options->currentPage) && isset($options->limit)) {
			$options->offset = $options->limit * ($options->currentPage - 1);
			$trainingOrm->limit($options->limit)->offset($options->offset);
		}
		
		$results = array();
		foreach ($trainingOrm->find_all() as $key => $training) {
			$readOptions = (object)array('id' => $training->id, 'populate' => $options->populate);
			$results[] = $this->read($readOptions);
		}

		return $results;
	}






	public function save($info) {
		$id = isset($info->id) ? $info->id:null;

		$formatString = new Etc_FormatString();
		$training = ORM::factory('orm_training', $id);
		$training->name = $formatString->multiFormat($info->name, array('spaceTrim', 'alphaNumericOnly', 'upperFirstWord'));
		$training->description = $formatString->multiFormat($info->description, array('spaceTrim', 'alphaNumericSymbolOnly'));
		$training->save();

		return (int)$training->id;
	}




	public function permanentDelete($id) {
		$training = ORM::factory('orm_training', $id);
		$training->delete();
	}





}