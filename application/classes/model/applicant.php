<?php defined('SYSPATH') OR die('No direct access allowed.');


class Model_Applicant extends Model {


	// fetch list of membership applicants
	// supports pagination, limit, and search
	public function enlist($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$formatString = new Etc_FormatString();
		$applicantOrm = ORM::factory('orm_applicant');

		// choose 1 only(applicantsForInterview or newApplicants only)
		// applicants for interview only(must order the results by interview startDate)
		// new applicants only(applicants that are not yet set for an interview)
		if(isset($options->applicantsForInterview)) {
			$applicantOrm
			->join('applicant_interview')->on('applicant_interview.applicantId', '=', 'orm_applicant.id')
			->join('schedule_applicant_interviews')->on('applicant_interview.scheduleApplicantInterviewId', '=', 'schedule_applicant_interviews.id')
			->order_by('schedule_applicant_interviews.start');
		} else if(isset($options->newApplicants)) {
			$applicantOrm->where('id', 'NOT IN', DB::select('applicantId')->from('applicant_interview'));
		}

		// order by
		if(isset($options->orderBy)) {
			$applicantOrm->order_by($options->orderBy[0], $options->orderBy[1]);
		}

		// search
		if(isset($options->search)) {
			$options->search = explode(' ', $formatString->multiFormat($options->search, array('spaceTrim', 'alphaNumericSymbolOnly')));
			$applicantOrm->where_open();
			foreach ($options->search as $key => $word) {
				$applicantOrm
				->or_where_open()
				->or_where('firstName', 'LIKE', "%$word%")
				->or_where('middleName', 'LIKE', "%$word%")
				->or_where('lastName', 'LIKE', "%$word%")
				->or_where('applicationDate', 'LIKE', "%$word%")
				->or_where('gender', 'LIKE', "%$word%")
				->or_where('civilStatus', 'LIKE', "%$word%")
				->or_where('tags', 'LIKE', "%$word%")
				->or_where_close();
			}
			$applicantOrm->where_close();
		}

		// limit and offset
		if(isset($options->currentPage) && isset($options->limit)) {
			$options->offset = $options->limit * ($options->currentPage - 1);
			$applicantOrm->limit($options->limit)->offset($options->offset);
		}

		$results = array();
		foreach ($applicantOrm->find_all() as $key => $applicant) {
			$readOptions = (object)array('id' => $applicant->id, 'populate' => $options->populate);
			$results[] = $this->read($readOptions);
		}
		
		return $results;
	}






	// returns an applicants's full info (almost all fields in applicant table)
	public function read($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$utility = new Etc_Utility();

		$applicant = ORM::factory('orm_applicant', $options->id);
		
		// getting basic info
		$applicantInfo = new stdClass();
		$applicantInfo->id = $applicant->id;
		$applicantInfo->applicationDate = $applicant->applicationDate;
		$applicantInfo->firstName = $applicant->firstName;
		$applicantInfo->middleName = $applicant->middleName;
		$applicantInfo->lastName = $applicant->lastName;
		$applicantInfo->birthDate = $applicant->birthDate;
		$applicantInfo->age = $utility->computeAge($applicant->birthDate);
		$applicantInfo->gender = $applicant->gender;
		$applicantInfo->civilStatus = $applicant->civilStatus;
		$applicantInfo->street = $applicant->street;
		$applicantInfo->barangay = $applicant->barangay;
		$applicantInfo->city = $applicant->city;
		$applicantInfo->province = $applicant->province;
		$applicantInfo->height = $applicant->height;
		$applicantInfo->weight = $applicant->weight;
		$applicantInfo->religion = $applicant->religion;
		$applicantInfo->mobile = $applicant->mobile;
		$applicantInfo->email = $applicant->email;
		$applicantInfo->photo = $applicant->photo;
		$applicantInfo->tags = explode("|", $applicant->tags);
		$applicantInfo->tags = (count($applicantInfo->tags) == 1 && $applicantInfo->tags[0] == '') ? array():$applicantInfo->tags;

		$populateManager = new Etc_PopulateManager();

		// populate applicant_beneficiaries if specified
		if($populateManager->check('beneficiaries', $options->populate)) {
			$applicantBeneficiaryModel = new Model_Applicant_Beneficiary();
			$beneficiaryOptions = (object)array('applicantId' => $options->id);
			$applicantInfo->beneficiaries = $applicantBeneficiaryModel->enlist($beneficiaryOptions);
		}

		// populate applicant_character_references if specified 
		if($populateManager->check('characterReferences', $options->populate)) {
			$applicantCharacterReferenceModel = new Model_Applicant_CharacterReference();
			$characterReferenceOptions = (object)array('applicantId' => $options->id);
			$applicantInfo->characterReferences = $applicantCharacterReferenceModel->enlist($characterReferenceOptions);
		}

		// populate applicant_education if specified
		if($populateManager->check('education', $options->populate)) {
			$applicantEducationModel = new Model_Applicant_Education();
			$educReadOptions = (object)array('applicantId' => $options->id);
			$applicantInfo->education = $applicantEducationModel->read($educReadOptions);
			if(!isset($applicantInfo->education->applicantId)) unset($applicantInfo->education);
		}

		// populate applicant_emergency_contact if specified
		if($populateManager->check('emergencyContact', $options->populate)) {
			$applicantEmergencyContactModel = new Model_Applicant_EmergencyContact();
			$emergencyContactReadOptions = (object)array('applicantId' => $options->id);
			$applicantInfo->emergencyContact = $applicantEmergencyContactModel->read($emergencyContactReadOptions);
			if(!isset($applicantInfo->emergencyContact->applicantId)) unset($applicantInfo->emergencyContact);	
		}

		// populate applicant_parent if specified
		if($populateManager->check('parent', $options->populate)) {
			$applicantParentModel = new Model_Applicant_Parent();
			$parentReadOptions = (object)array('applicantId' => $options->id);
			$applicantInfo->parent = $applicantParentModel->read($parentReadOptions);
			if(!isset($applicantInfo->parent->applicantId)) unset($applicantInfo->parent);
		}

		// populate applicant_skills if specified
		if($populateManager->check('skills', $options->populate)) {
			$applicantSkillModel = new Model_Applicant_Skill();
			$skillOptions = (object)array('applicantId' => $options->id);
			$applicantInfo->skills = $applicantSkillModel->enlist($skillOptions);
		}

		// populate applicant_spouse if specified
		if($populateManager->check('spouse', $options->populate)) {
			$applicantSpouseModel = new Model_Applicant_Spouse();
			$spouseReadOptions = (object)array('applicantId' => $options->id);
			$applicantInfo->spouse = $applicantSpouseModel->read($spouseReadOptions);
			if(!isset($applicantInfo->spouse->applicantId)) unset($applicantInfo->spouse);
		}

		// populate applicant_work_histories if specified
		if($populateManager->check('workHistories', $options->populate)) {
			$applicantWorkHistoryModel = new Model_Applicant_WorkHistory();
			$workHistoryOptions = (object)array('applicantId' => $options->id);
			$applicantInfo->workHistories = $applicantWorkHistoryModel->enlist($workHistoryOptions);
		}

		// populate applicant_interview if specified
		if($populateManager->check('interview', $options->populate)) {
			$interviewModel = new Model_Applicant_interview();
			$interviewReadOptions = (object)array('applicantId' => $options->id, 'populate' => $populateManager->currentPopulate);
			$applicantInfo->interview = $interviewModel->read($interviewReadOptions);
			if(!isset($applicantInfo->interview->applicantId)) unset($applicantInfo->interview);
		}

		return $applicantInfo;
	}




	// use for either create or update
	public function save($applicantInfo) {
		$applicantId = isset($applicantInfo->id) ? $applicantInfo->id:null;

		// saving applicant's basic info
		$formatString = new Etc_FormatString();
		$applicant = ORM::factory('orm_applicant', $applicantId);
		$applicant->applicationDate = isset($applicantInfo->applicationDate) ? date('Y-m-d H:i:s', strtotime($applicantInfo->applicationDate)) : date('Y-m-d H:i:s', time());
		$applicant->firstName = $formatString->multiFormat($applicantInfo->firstName, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$applicant->middleName = !isset($applicantInfo->middleName) ? '' : $formatString->multiFormat($applicantInfo->middleName, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$applicant->lastName = $formatString->multiFormat($applicantInfo->lastName, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$applicant->birthDate = date('Y-m-d', strtotime($applicantInfo->birthDate));
		$applicant->gender = $formatString->multiFormat($applicantInfo->gender, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$applicant->civilStatus = $formatString->multiFormat($applicantInfo->civilStatus, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$applicant->street = !isset($applicantInfo->street) ? '' : $formatString->multiFormat($applicantInfo->street, array('spaceTrim', 'alphaNumericSymbolOnly', 'upperFirstWord'));
		$applicant->barangay = $formatString->multiFormat($applicantInfo->barangay, array('spaceTrim', 'alphaNumericSymbolOnly', 'upperFirstWord'));
		$applicant->city = $formatString->multiFormat($applicantInfo->city, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$applicant->province = $formatString->multiFormat($applicantInfo->province, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$applicant->height = $formatString->multiFormat($applicantInfo->height, array('noSpace', 'numericOnly'));
		$applicant->weight = $formatString->multiFormat($applicantInfo->weight, array('noSpace', 'numericOnly'));
		$applicant->religion = $formatString->multiFormat($applicantInfo->religion, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$applicant->mobile = $formatString->multiFormat($applicantInfo->mobile, array('noSpace', 'numericOnly'));
		$applicant->email = $formatString->multiFormat($applicantInfo->email, array('noSpace', 'emailCharsOnly'));
		$applicant->tags = implode("|", $applicantInfo->tags);
		$applicant->photo = $applicantInfo->photo;
		$applicant->save();

		$applicantId = $applicant->id;


		// saving applicant_beneficiaries
		if(isset($applicantInfo->beneficiaries)) {
			DB::delete('applicant_beneficiaries')->where('applicantId', '=', $applicantId)->execute();

			$applicantBeneficiaryModel = new Model_Applicant_Beneficiary();
			foreach ($applicantInfo->beneficiaries as $beneficiary) {
				$beneficiary->applicantId = $applicantId;
				$applicantBeneficiaryModel->save($beneficiary);
			}
		}


		// saving applicant character references
		if(isset($applicantInfo->characterReferences)) {
			DB::delete('applicant_character_references')->where('applicantId', '=', $applicantId)->execute();

			$applicantCharacterReferenceModel = new Model_Applicant_CharacterReference();
			foreach ($applicantInfo->characterReferences as $characterReference) {
				$characterReference->applicantId = $applicantId;
				$applicantCharacterReferenceModel->save($characterReference);
			}
		}


		// saving applicant education info
		if(isset($applicantInfo->education)) {
			$applicantEducationModel = new Model_Applicant_Education();
			$applicantInfo->education->applicantId = $applicantId;
			$applicantEducationModel->save($applicantInfo->education);
		}


		// saving applicant emergencyContact info
		if(isset($applicantInfo->emergencyContact)) {
			$applicantEmergencyContactModel = new Model_Applicant_EmergencyContact();
			$applicantInfo->emergencyContact->applicantId = $applicantId;
			$applicantEmergencyContactModel->save($applicantInfo->emergencyContact);
		}


		// saving applicant interview
		if(isset($applicantInfo->interview)) {
			$applicantInterviewModel = new Model_Applicant_Interview();
			$applicantInfo->interview->applicantId = $applicantId;
			$applicantInterviewModel->save($applicantInfo->interview);
		}


		// saving applicant parent
		if(isset($applicantInfo->parent)) {
			$applicantParentModel = new Model_Applicant_Parent();
			$applicantInfo->parent->applicantId = $applicantId;
			$applicantParentModel->save($applicantInfo->parent);
		}
		


		// saving applicant skill
		if(isset($applicantInfo->skills)) {
			DB::delete('applicant_skills')->where('applicantId', '=', $applicantId)->execute();

			$applicantSkillModel = new Model_Applicant_Skill();
			foreach ($applicantInfo->skills as $skill) {
				$skill->applicantId = $applicantId;
				$applicantSkillModel->save($skill);
			}
		}


		// saving applicant spouse
		if(($applicantInfo->civilStatus != 'Single' && $applicantInfo->civilStatus != 'Divorced') && isset($applicantInfo->spouse)) {
			$applicantSpouseModel = new Model_Applicant_Spouse();
			$applicantInfo->spouse->applicantId = $applicantId;
			$applicantSpouseModel->save($applicantInfo->spouse);
		} else {
			DB::delete('applicant_spouse')->where('applicantId', '=', $applicantId)->execute();
		}
		


		// saving applicant work histories
		if(isset($applicantInfo->workHistories)) {
			DB::delete('applicant_work_histories')->where('applicantId', '=', $applicantId)->execute();

			$applicantWorkHistoryModel = new Model_Applicant_WorkHistory();
			foreach ($applicantInfo->workHistories as $workHistory) {
				$workHistory->applicantId = $applicantId;
				$applicantWorkHistoryModel->save($workHistory);
			}
		}
	}




	// updates applicants picture name in db
	// also deletes previous picture file
	public function applicantPictureUpdate($info, $newPictureName) {
		if(file_exists($_SERVER['DOCUMENT_ROOT'].'/worktrustedV2/app/assets/profilePhotos/'.$info->photo)) {
			unlink($_SERVER['DOCUMENT_ROOT'].'/worktrustedV2/app/assets/profilePhotos/'.$info->photo);
		}
		$applicant = ORM::factory('orm_applicant', $info->id);
		$applicant->photo = $newPictureName;
		$applicant->save();
	}



	public function uploadProfilePicture($file) {
		$fileUpload = new Etc_FileUpload($file, array('jpg', 'png', 'jpeg','JPEG', 'JPG','PNG'), '512000', 'profilePhotos/');
		$fileUpload->uploadFile();
		echo json_encode($fileUpload->uploadInfoResponse());
	}



	public function uploadBase64ProfilePicture($base64File) {
		list($type, $base64File) = explode(';', $base64File);
		list(, $base64File)      = explode(',', $base64File);
		$base64File = base64_decode($base64File);
		$fileName = rand(0, 10000000).'_'.rand(0, 10000000).'_'.rand(0, 10000000).'.jpeg';
		file_put_contents($_SERVER['DOCUMENT_ROOT'].'/worktrustedV2/app/assets/profilePhotos/'.$fileName, $base64File);
		return array('newFileName' => $fileName);
	}





	// remove/reject applicant
	// use this also for rejecting an applicant in an interview, deleting applicant will also delete applicantInterview
	// updates status to "deleted" since we have to keep all records
	public function delete($applicantId) {
		$applicant = ORM::factory('orm_applicant', $applicantId);
		$applicant->status = 'deleted';
		$applicant->save();
	}




	// permanent delete applicant entry from database
	public function permanentDelete($applicantId, $deletePhoto = false) {
		$applicant = ORM::factory('orm_applicant', $applicantId);

		if($deletePhoto) {
			if(file_exists($_SERVER['DOCUMENT_ROOT'].'/worktrustedV2/app/assets/profilePhotos/'.$applicant->photo)) {
				unlink($_SERVER['DOCUMENT_ROOT'].'/worktrustedV2/app/assets/profilePhotos/'.$applicant->photo);
			}
		}
		$applicant->delete();
	}




}