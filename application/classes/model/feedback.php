<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Feedback extends Model {


	// return list of feedbacks
	// with pagination support
	public function enlist($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$formatString = new Etc_FormatString();
		$feedbackOrm = ORM::factory('orm_feedback');

		// order by
		if(isset($options->orderBy)) {
			$feedbackOrm->order_by($options->orderBy[0], $options->orderBy[1]);
		}

		// search
		if(isset($options->search)) {
			$options->search = explode(' ', $formatString->multiFormat($options->search, array('spaceTrim', 'alphaNumericOnly')));
			$feedbackOrm->where_open();
			foreach ($options->search as $key => $word) {
				$feedbackOrm
				->or_where_open()
				->or_where('name', 'LIKE', "%$word%")
				->or_where('subject', 'LIKE', "%$word%")
				->or_where('datetime', 'LIKE', "%$word%")
				->or_where_close();
			}
			$feedbackOrm->where_close();
		}

		// limit and offset
		if(isset($options->currentPage) && isset($options->limit)) {
			$options->offset = $options->limit * ($options->currentPage - 1);
			$feedbackOrm->limit($options->limit)->offset($options->offset);
		}

		$results = array();
		foreach ($feedbackOrm->find_all() as $feedback) {
			$readOptions = (object)array('id' => $feedback->id, 'populate' => $options->populate);
			$results[] = $this->read($readOptions);
		}
		return $results;
	}


	// return feedback info
	public function read($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$feedback = ORM::factory('orm_feedback', $options->id);
		$feedbackInfo = new stdClass();
		$feedbackInfo->id = $feedback->id;
		$feedbackInfo->name = $feedback->name;
		$feedbackInfo->email = $feedback->email;
		$feedbackInfo->subject = $feedback->subject;
		$feedbackInfo->message = $feedback->message;
		$feedbackInfo->datetime = $feedback->datetime;
		
		return $feedbackInfo;
	}


	// save feedback
	// use for either update or create
	public function save($feedbackInfo) {
		$formatString = new Etc_FormatString();
		$feedbackId = isset($feedbackInfo->id) ? $feedbackInfo->id:null;
		$feedback = ORM::factory('orm_feedback', $feedbackId);
		$feedback->name = $formatString->multiFormat($feedbackInfo->name, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$feedback->email = $formatString->multiFormat($feedbackInfo->email, array('noSpace', 'emailCharsOnly'));
		$feedback->subject = !isset($feedbackInfo->subject) ? '' : $formatString->multiFormat($feedbackInfo->subject, array('spaceTrim', 'alphaNumericOnly', 'upperFirstWord'));
		$feedback->message = $formatString->multiFormat($feedbackInfo->message, array('spaceTrim', 'alphaNumericSymbolOnly	'));
		$feedback->datetime = date('Y-m-d h:m:s', time());
		$feedback->save();
	}


	// delete a feedback
	// update status to "deleted"
	public function delete($feedbackId) {
		$feedback = ORM::factory('orm_feedback', $feedbackId);
		$feedback->status = 'deleted';
		$feedback->save();
	}


	// permanent delete a feedback
	public function permanentDelete($feedbackId) {
		$feedback = ORM::factory('orm_feedback', $feedbackId)->delete();
	}














}