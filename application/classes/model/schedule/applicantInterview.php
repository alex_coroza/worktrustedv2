<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Schedule_ApplicantInterview extends Model {


	public function read($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$schedule = ORM::factory('orm_scheduleApplicantInterview', $options->id);
		$scheduleInfo = new stdClass();
		$scheduleInfo->id = $schedule->id;
		$scheduleInfo->start = $schedule->start;
		$scheduleInfo->end = $schedule->end;
		
		$populateManager = new Etc_PopulateManager();

		if($populateManager->check('applicantInterviews', $options->populate)) {
			$applicantInterviewModel = new Model_Applicant_Interview();
			$applicantOptions = (object)array('scheduleApplicantInterviewId' => $options->id, 'populate' => $populateManager->currentPopulate);
			$scheduleInfo->applicants = $applicantInterviewModel->enlist($applicantOptions);
		}

		return $scheduleInfo;
	}






	public function enlist($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$formatString = new Etc_FormatString();
		$scheduleOrm = ORM::factory('orm_scheduleApplicantInterview');


		// order by
		if(isset($options->orderBy)) {
			$scheduleOrm->order_by($options->orderBy[0], $options->orderBy[1]);
		}


		// search
		if(isset($options->search)) {
			$options->search = explode(' ', $formatString->multiFormat($options->search, array('spaceTrim', 'alphaNumericOnly')));
			$scheduleOrm->where_open();
			foreach ($options->search as $key => $word) {
				$scheduleOrm
				->or_where_open()
				->or_where('start', 'LIKE', "%$word%")
				->or_where('end', 'LIKE', "%$word%")
				->or_where_close();
			}
			$scheduleOrm->where_close();
		}


		// monthYear condition
		if(isset($options->monthYear)) {
			$year = date('Y', strtotime($options->monthYear));
			$month = date('m', strtotime($options->monthYear));
			$scheduleOrm
			->where_open()
			->where(DB::expr('MONTH(start)'), '=', $month)
			->where(DB::expr('YEAR(start)'), '=', $year)
			->where_close();
		}


		// "upcoming events" condition
		if(isset($options->upcoming)) {
			$scheduleOrm->where('start', '>=', date('Y-m-d', time()));
		}


		// limit and offset
		if(isset($options->currentPage) && isset($options->limit)) {
			$options->offset = $options->limit * ($options->currentPage - 1);
			$scheduleOrm->limit($options->limit)->offset($options->offset);
		}


		
		$results = array();
		foreach ($scheduleOrm->find_all() as $key => $schedule) {
			$readOptions = (object)array('id' => $schedule->id, 'populate' => $options->populate);
			$results[] = $this->read($readOptions);
		}

		return $results;
	}






	public function save($info) {
		$id = isset($info->id) ? $info->id:null;

		$formatString = new Etc_FormatString();
		$schedule = ORM::factory('orm_scheduleApplicantInterview', $id);
		$schedule->start = date('Y-m-d', strtotime($info->start));
		$schedule->end = date('Y-m-d', strtotime($info->end));
		$schedule->save();

		$id = $schedule->id;

		// saving attendees/applicants
		if(isset($info->applicants)) {
			DB::delete('applicant_interview')->where('scheduleApplicantInterviewId', '=', $id)->execute();

			$applicantInterviewModel = new Model_Applicant_Interview();
			foreach ($info->applicants as $applicant) {
				$applicant->scheduleApplicantInterviewId = $id;
				$applicantInterviewModel->save($applicant);
			}
		}

		return (int)$id;
	}




	public function delete($id) {
		$schedule = ORM::factory('orm_scheduleApplicantInterview', $id);
		$schedule->delete();
	}




}