<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Schedule_TrainingEvent extends Model {


	public function read($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$trainingEvent = ORM::factory('orm_trainingEvent', $options->id);
		$trainingEventInfo = new stdClass();
		$trainingEventInfo->id = $trainingEvent->id;
		$trainingEventInfo->trainingId = $trainingEvent->trainingId;
		$trainingEventInfo->start = $trainingEvent->start;
		$trainingEventInfo->end = $trainingEvent->end;
		$trainingEventInfo->location = $trainingEvent->location;
		$trainingEventInfo->slots = $trainingEvent->slots;
		$trainingEventInfo->requirements = explode('|', $trainingEvent->requirements);
		$trainingEventInfo->additionalNotes = $trainingEvent->additionalNotes;
		
		$populateManager = new Etc_PopulateManager();

		if($populateManager->check('training', $options->populate)) {
			$trainingModel = new Model_Training();
			$trainingReadOptions = (object)array('id' => $trainingEvent->trainingId);
			$trainingEventInfo->training = $trainingModel->read($trainingReadOptions);
			if(!isset($trainingEventInfo->training->id)) unset($trainingEventInfo->training);
		}

		if($populateManager->check('attendees', $options->populate)) {
			$memberTrainingEventModel = new Model_Member_TrainingEvent();
			$attendeeOptions = (object)array('trainingEventId' => $trainingEvent->id, 'populate' => $populateManager->currentPopulate);
			$trainingEventInfo->attendees = $memberTrainingEventModel->enlist($attendeeOptions);
		}

		return $trainingEventInfo;
	}




	public function enlist($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();


		$formatString = new Etc_FormatString();
		$trainingEventOrm = ORM::factory('orm_trainingEvent');

		// order by
		if(isset($options->orderBy)) {
			$trainingEventOrm->order_by($options->orderBy[0], $options->orderBy[1]);
		}

		// search
		if(isset($options->search)) {
			$options->search = explode(' ', $formatString->multiFormat($options->search, array('spaceTrim', 'alphaNumericOnly')));
			$trainingEventOrm->where_open();
			foreach ($options->search as $key => $word) {
				$trainingEventOrm
				->or_where_open()
				->or_where('additionalNotes', 'LIKE', "%$word%")
				->or_where('location', 'LIKE', "%$word%")
				->or_where('start', 'LIKE', "%$word%")
				->or_where('end', 'LIKE', "%$word%")
				->or_where_close();
			}
			$trainingEventOrm->where_close();
		}


		// monthYear condition
		if(isset($options->monthYear)) {
			$year = date('Y', strtotime($options->monthYear));
			$month = date('m', strtotime($options->monthYear));
			$trainingEventOrm
			->where_open()
			->where(DB::expr('MONTH(start)'), '=', $month)
			->where(DB::expr('YEAR(start)'), '=', $year)
			->where_close();
		}


		// currentData condition(data within previous, current, and next month)
		if(isset($options->currentData)) {
			$prevMonthDate = date('Y-m-01', strtotime('-1 month'));
			$nextMonthDate = date('Y-m-t', strtotime('+1 month'));
			$trainingEventOrm
			->where('start', 'BETWEEN', array($prevMonthDate, $nextMonthDate));
		}


		// "upcoming events" condition
		if(isset($options->upcoming)) {
			$trainingEventOrm->where('start', '>=', date('Y-m-d', time()));
		}
		

		// enlist from training
		if(isset($options->trainingId)) {
			$trainingEventOrm->where('trainingId', '=', $options->trainingId);
		}


		// limit and offset
		if(isset($options->currentPage) && isset($options->limit)) {
			$options->offset = $options->limit * ($options->currentPage - 1);
			$trainingEventOrm->limit($options->limit)->offset($options->offset);
		}


		$results = array();
		foreach ($trainingEventOrm->find_all() as $key => $trainingEvent) {
			$readOptions = (object)array('id' => $trainingEvent->id, 'populate' => $options->populate);
			$results[] = $this->read($readOptions);
		}

		return $results;
	}






	public function save($info) {
		$id = isset($info->id) ? $info->id:null;

		$formatString = new Etc_FormatString();
		$trainingEvent = ORM::factory('orm_trainingEvent', $id);
		$trainingEvent->trainingId = $formatString->multiFormat($info->trainingId, array('spaceTrim', 'numericOnly'));
		$trainingEvent->start = date('Y-m-d', strtotime($info->start));
		$trainingEvent->end = date('Y-m-d', strtotime($info->end));
		$trainingEvent->location = $formatString->multiFormat($info->location, array('spaceTrim', 'alphaNumericSymbolOnly', 'upperFirstWord'));
		$trainingEvent->slots = $formatString->multiFormat($info->slots, array('spaceTrim', 'numericOnly'));
		$trainingEvent->requirements = !isset($info->requirements) ? '' : implode('|', $info->requirements);
		$trainingEvent->additionalNotes = !isset($info->additionalNotes) ? '' : $formatString->multiFormat($info->additionalNotes, array('spaceTrim', 'alphaNumericSymbolOnly'));
		$trainingEvent->save();

		$id = $trainingEvent->id;

		// saving trainingEventAttendees
		if(isset($info->attendees)) {
			DB::delete('member_training_events')->where('trainingEventId', '=', $id)->execute();
			$memberTrainingEventModel = new Model_Member_TrainingEvent();
			foreach ($info->attendees as $key => $attendee) {
				$attendee->trainingEventId = $id;
				$memberTrainingEventModel->save($attendee);
			}
		}

		return (int)$id;
	}





	public function delete($id) {
		$trainingEvent = ORM::factory('orm_trainingEvent', $id);
		$trainingEvent->delete();
	}









}