<?php defined('SYSPATH') OR die('No direct access allowed.');


class Model_Job_Applicant extends Model {



	// fetch list of jobApplicants
	// supports pagination, limit, and search
	public function enlist($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$formatString = new Etc_FormatString();
		$jobApplicantOrm = ORM::factory('orm_jobApplicant');

		// enlist by memberId
		if(isset($options->memberId)) {
			$jobApplicantOrm->where('memberId', '=', $options->memberId);
		}

		// enlist by jobOpeningId
		if(isset($options->jobOpeningId)) {
			$jobApplicantOrm->where('jobOpeningId', '=', $options->jobOpeningId);
		}

		// enlist by status
		if(isset($options->status)) {
			$jobApplicantOrm->where('orm_jobApplicant.status', '=', $options->status);
		}

		// enlist by jobOpening's companyid
		if(isset($options->companyId)) {
			$jobApplicantOrm
			->join('job_openings')
			->on('orm_jobApplicant.jobOpeningId', '=', 'job_openings.id')
			->where('job_openings.companyId', '=', $options->companyId);
		}
		
		// order by
		if(isset($options->orderBy)) {
			$jobApplicantOrm->order_by($options->orderBy[0], $options->orderBy[1]);
		}

		// search
		if(isset($options->search)) {
			$options->search = explode(' ', $formatString->multiFormat($options->search, array('spaceTrim', 'alphaNumericSymbolOnly')));
			$jobApplicantOrm->where_open();
			foreach ($options->search as $key => $word) {
				$jobApplicantOrm
				->or_where_open()
				->or_where('orm_jobApplicant.date', 'LIKE', "%$word%")
				->or_where_close();
			}
			$jobApplicantOrm->where_close();
		}

		// limit and offset
		if(isset($options->currentPage) && isset($options->limit)) {
			$options->offset = $options->limit * ($options->currentPage - 1);
			$jobApplicantOrm->limit($options->limit)->offset($options->offset);
		}

		$results = array();
		foreach ($jobApplicantOrm->find_all() as $key => $applicant) {
			$readOptions = (object)array('id' => $applicant->id, 'populate' => $options->populate);
			$results[] = $this->read($readOptions);
		}
		
		return $results;
	}






	// returns an jobApplicant's full info (almost all fields in member table)
	public function read($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$utility = new Etc_Utility();

		$jobApplicant = ORM::factory('orm_jobApplicant', $options->id);
		
		// getting basic info
		$jobApplicantInfo = new stdClass();
		$jobApplicantInfo->id = $jobApplicant->id;
		$jobApplicantInfo->jobOpeningId = $jobApplicant->jobOpeningId;
		$jobApplicantInfo->memberId = $jobApplicant->memberId;
		$jobApplicantInfo->date = $jobApplicant->date;
		$jobApplicantInfo->status = $jobApplicant->status;
		

		$populateManager = new Etc_PopulateManager();

		// populate jobApplicant's jobOpening if specified
		if($populateManager->check('jobOpening', $options->populate)) {
			$jobOpeningModel = new Model_Job_Opening();
			$readOptions = (object)array('id' => $jobApplicantInfo->jobOpeningId, 'populate' => $populateManager->currentPopulate);
			$jobApplicantInfo->jobOpening = $jobOpeningModel->read($readOptions);
			if(!isset($jobApplicantInfo->jobOpening->id)) unset($jobApplicantInfo->jobOpening);
		}


		// populate jobApplicant's member if specified
		if($populateManager->check('member', $options->populate)) {
			$memberModel = new Model_Member();
			$readOptions = (object)array('id' => $jobApplicantInfo->memberId, 'populate' => $populateManager->currentPopulate);
			$jobApplicantInfo->member = $memberModel->read($readOptions);
			if(!isset($jobApplicantInfo->member->id)) unset($jobApplicantInfo->member);
		}


		return $jobApplicantInfo;
	}







	// use for either create or update
	public function save($jobApplicantInfo) {
		$jobApplicantId = isset($jobApplicantInfo->id) ? $jobApplicantInfo->id:null;

		// saving jobApplicant's basic info
		$formatString = new Etc_FormatString();
		$jobApplicant = ORM::factory('orm_jobApplicant', $jobApplicantId);
		$jobApplicant->jobOpeningId = $formatString->multiFormat($jobApplicantInfo->jobOpeningId, array('spaceTrim', 'numericOnly'));
		$jobApplicant->memberId = $formatString->multiFormat($jobApplicantInfo->memberId, array('spaceTrim', 'numericOnly'));
		$jobApplicant->date = isset($jobApplicantInfo->date) ? date('Y-m-d H:i:s', strtotime($jobApplicantInfo->date)) : date('Y-m-d H:i:s', time());
		$jobApplicant->status = $formatString->multiFormat($jobApplicantInfo->status, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$jobApplicant->save();

		$jobApplicantId = $jobApplicant->id;

		return (int)$jobApplicantId;
	}





	public function delete($id) {
		ORM::factory('orm_jobApplicant', $id)->delete();
	}











}