<?php defined('SYSPATH') OR die('No direct access allowed.');


class Model_Job_Opening extends Model {


	// fetch list of jobOpenings
	// supports pagination, limit, and search
	public function enlist($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$formatString = new Etc_FormatString();
		$jobOpeningOrm = ORM::factory('orm_jobopening');

		// enlist by companyId
		if(isset($options->companyId)) {
			$jobOpeningOrm->where('companyId', '=', $options->companyId);
		}

		// enlist by employmentType
		if(isset($options->employmentType)) {
			$jobOpeningOrm->where('employmentType', '=', $options->employmentType);
		}

		// enlist by status
		if(isset($options->status)) {
			$jobOpeningOrm->where('status', '=', $options->status);
		}

		// enlist by expectedSalary
		if(isset($options->expectedSalary)) {
			$jobOpeningOrm->where($options->expectedSalary, 'BETWEEN', DB::expr('minSalary AND maxSalary'));
		}

		// enlist by industryType
		if(isset($options->industryType)) {
			$jobOpeningOrm->where('industryType', '=', $options->industryType);
		}

		// jobOpenings within valid date only
		if(isset($options->validDateOnly)) {
			$jobOpeningOrm->where('validityDate', '>=', date('Y-m-d', time()));
		}
		
		// order by
		if(isset($options->orderBy)) {
			$jobOpeningOrm->order_by($options->orderBy[0], $options->orderBy[1]);
		}

		// search
		if(isset($options->search)) {
			$options->search = explode(' ', $formatString->multiFormat($options->search, array('spaceTrim', 'alphaNumericSymbolOnly')));
			$jobOpeningOrm->where_open();
			foreach ($options->search as $key => $word) {
				$jobOpeningOrm
				->or_where_open()
				->or_where('title', 'LIKE', "%$word%")
				->or_where('description', 'LIKE', "%$word%")
				->or_where_close();
			}
			$jobOpeningOrm->where_close();
		}

		// limit and offset
		if(isset($options->currentPage) && isset($options->limit)) {
			$options->offset = $options->limit * ($options->currentPage - 1);
			$jobOpeningOrm->limit($options->limit)->offset($options->offset);
		}

		$results = array();
		foreach ($jobOpeningOrm->find_all() as $key => $jobOpening) {
			$readOptions = (object)array('id' => $jobOpening->id, 'populate' => $options->populate);
			$results[] = $this->read($readOptions);
		}
		
		return $results;
	}






	// returns an jobOpening's full info (almost all fields in member table)
	public function read($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$utility = new Etc_Utility();

		$jobOpeningOrm = ORM::factory('orm_jobOpening')->where('id', '=', $options->id);

		if(isset($options->companyId)) {
			$jobOpeningOrm->where('companyId', '=', $options->companyId);
		}

		$jobOpening = $jobOpeningOrm->find();
		
		// getting basic info
		$jobOpeningInfo = new stdClass();
		$jobOpeningInfo->id = $jobOpening->id;
		$jobOpeningInfo->companyId = $jobOpening->companyId;
		$jobOpeningInfo->title = $jobOpening->title;
		$jobOpeningInfo->description = $jobOpening->description;
		$jobOpeningInfo->date = $jobOpening->date;
		$jobOpeningInfo->minSalary = $jobOpening->minSalary;
		$jobOpeningInfo->maxSalary = $jobOpening->maxSalary;
		$jobOpeningInfo->employmentType = $jobOpening->employmentType;
		$jobOpeningInfo->industryType = $jobOpening->industryType;
		$jobOpeningInfo->slots = $jobOpening->slots;
		$jobOpeningInfo->validityDate = $jobOpening->validityDate;
		$jobOpeningInfo->status = $jobOpening->status;
		

		$populateManager = new Etc_PopulateManager();

		// populate jobOpening's company if specified
		if($populateManager->check('company', $options->populate)) {
			$companyModel = new Model_Company();
			$readOptions = (object)array('id' => $jobOpeningInfo->companyId, 'populate' => $populateManager->currentPopulate);
			$jobOpeningInfo->company = $companyModel->read($readOptions);
			if(!isset($jobOpeningInfo->company->id)) unset($jobOpeningInfo->company);
		}

		// populate jobOpening's applicants if specified
		if($populateManager->check('applicants', $options->populate)) {
			$jobApplicantModel = new Model_Job_Applicant();
			$enlistOptions = (object)array('jobOpeningId' => $jobOpeningInfo->id, 'populate' => $populateManager->currentPopulate);
			$jobOpeningInfo->applicants = $jobApplicantModel->enlist($enlistOptions);
		}


		return $jobOpeningInfo;
	}




	// use for either create or update
	public function save($jobOpeningInfo) {
		$jobOpeningId = isset($jobOpeningInfo->id) ? $jobOpeningInfo->id:null;

		// saving jobOpening's basic info
		$formatString = new Etc_FormatString();
		$jobOpening = ORM::factory('orm_jobOpening', $jobOpeningId);
		$jobOpening->companyId = $formatString->multiFormat($jobOpeningInfo->companyId, array('spaceTrim', 'numericOnly'));
		$jobOpening->title = $formatString->multiFormat($jobOpeningInfo->title, array('spaceTrim', 'alphaNumericOnly', 'upperFirstWord'));
		$jobOpening->description = $formatString->multiFormat($jobOpeningInfo->description, array('spaceTrim', 'alphaNumericSymbolOnly', 'lowerCase'));
		$jobOpening->date = isset($jobOpeningInfo->date) ? date('Y-m-d', strtotime($jobOpeningInfo->date)) :  date('Y-m-d H:i:s', time());
		$jobOpening->minSalary = !isset($jobOpeningInfo->minSalary) ? 0 : $formatString->multiFormat($jobOpeningInfo->minSalary, array('spaceTrim', 'numericOnly'));
		$jobOpening->maxSalary = !isset($jobOpeningInfo->maxSalary) ? 0 : $formatString->multiFormat($jobOpeningInfo->maxSalary, array('spaceTrim', 'numericOnly'));
		$jobOpening->employmentType = $formatString->multiFormat($jobOpeningInfo->employmentType, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$jobOpening->industryType = $formatString->multiFormat($jobOpeningInfo->industryType, array('spaceTrim', 'upperFirstWord'));
		$jobOpening->slots = $formatString->multiFormat($jobOpeningInfo->slots, array('spaceTrim', 'numericOnly'));
		$jobOpening->validityDate = date('Y-m-d', strtotime($jobOpeningInfo->validityDate));
		$jobOpening->status = $formatString->multiFormat($jobOpeningInfo->status, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$jobOpening->save();

		$jobOpeningId = $jobOpening->id;

		return (int)$jobOpeningId;
	}


}