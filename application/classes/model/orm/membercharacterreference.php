<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Orm_MemberCharacterReference extends ORM {

	protected $_table_name = 'member_character_references';

	protected $_table_columns = array(
		'id' => NULL,
		'memberId' => NULL,
		'name' => NULL,
		'occupation' => NULL,
		'company' => NULL,
		'mobile' => NULL,
		'email' => NULL
	);
	
}