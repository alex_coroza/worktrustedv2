<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Orm_TrainingEvent extends ORM {

	protected $_table_name = 'training_events';

	protected $_table_columns = array(
		'id' => NULL,
		'trainingId' => NULL,
		'start' => NULL,
		'end' => NULL,
		'location' => NULL,
		'slots' => NULL,
		'requirements' => NULL,
		'additionalNotes' => NULL
	);
	
}