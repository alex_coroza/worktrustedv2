<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Orm_MemberParent extends ORM {

	protected $_table_name = 'member_parent';
	protected $_primary_key = 'memberId';

	protected $_table_columns = array(
		'memberId' => NULL,
		'motherName' => NULL,
		'motherOccupation' => NULL,
		'fatherName' => NULL,
		'fatherOccupation' => NULL
	);
	
}