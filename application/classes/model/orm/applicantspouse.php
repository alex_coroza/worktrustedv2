<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Orm_ApplicantSpouse extends ORM {

	protected $_table_name = 'applicant_spouse';
	protected $_primary_key = 'applicantId';

	protected $_table_columns = array(
		'applicantId' => NULL,
		'firstName' => NULL,
		'middleName' => NULL,
		'lastName' => NULL,
		'birthDate' => NULL,
		'occupation' => NULL,
		'mobile' => NULL,
		'email' => NULL
	);
	
}