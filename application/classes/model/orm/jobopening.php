<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Orm_JobOpening extends ORM {

	protected $_table_name = 'job_openings';

	protected $_table_columns = array(
		'id' => NULL,
		'companyId' => NULL,
		'title' => NULL,
		'description' => NULL,
		'date' => NULL,
		'minSalary' => NULL,
		'maxSalary' => NULL,
		'employmentType' => NULL,
		'industryType' => NULL,
		'slots' => NULL,
		'validityDate' => NULL,
		'status' => NULL
	);
	
}