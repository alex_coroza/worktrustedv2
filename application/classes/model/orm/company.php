<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Orm_Company extends ORM {

	protected $_table_name = 'companies';

	protected $_table_columns = array(
		'id' => NULL,
		'name' => NULL,
		'description' => NULL,
		'partnershipDate' => NULL,
		'website' => NULL,
		'photo' => NULL
	);
	
}