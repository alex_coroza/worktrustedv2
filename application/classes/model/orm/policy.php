<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Orm_Policy extends ORM {

	protected $_table_name = 'policies';

	protected $_table_columns = array(
		'id' => NULL,
		'code' => NULL,
		'description' => NULL,
		'actions' => NULL
	);
	
}