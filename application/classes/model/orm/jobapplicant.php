<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Orm_JobApplicant extends ORM {

	protected $_table_name = 'job_applicants';

	protected $_table_columns = array(
		'id' => NULL,
		'jobOpeningId' => NULL,
		'memberId' => NULL,
		'date' => NULL,
		'status' => NULL
	);
	
}