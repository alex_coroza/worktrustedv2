<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Orm_ApplicantEducation extends ORM {

	protected $_table_name = 'applicant_education';
	protected $_primary_key = 'applicantId';

	protected $_table_columns = array(
		'applicantId' => NULL,
		'attainment' => NULL,
		'startYear' => NULL,
		'endYear' => NULL,
		'course' => NULL
	);
	
}