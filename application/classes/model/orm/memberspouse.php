<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Orm_MemberSpouse extends ORM {

	protected $_table_name = 'member_spouse';
	protected $_primary_key = 'memberId';

	protected $_table_columns = array(
		'memberId' => NULL,
		'firstName' => NULL,
		'middleName' => NULL,
		'lastName' => NULL,
		'birthDate' => NULL,
		'occupation' => NULL,
		'mobile' => NULL,
		'email' => NULL
	);
	
}