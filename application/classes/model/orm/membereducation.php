<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Orm_MemberEducation extends ORM {

	protected $_table_name = 'member_education';
	protected $_primary_key = 'memberId';

	protected $_table_columns = array(
		'memberId' => NULL,
		'attainment' => NULL,
		'startYear' => NULL,
		'endYear' => NULL,
		'course' => NULL
	);
	
}