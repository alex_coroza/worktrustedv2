<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Orm_MemberWorkHistory extends ORM {

	protected $_table_name = 'member_work_histories';

	protected $_table_columns = array(
		'id' => NULL,
		'memberId' => NULL,
		'company' => NULL,
		'position' => NULL,
		'startDate' => NULL,
		'endDate' => NULL
	);
	
}