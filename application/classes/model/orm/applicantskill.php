<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Orm_ApplicantSkill extends ORM {

	protected $_table_name = 'applicant_skills';

	protected $_table_columns = array(
		'id' => NULL,
		'applicantId' => NULL,
		'skill' => NULL
	);
	
}