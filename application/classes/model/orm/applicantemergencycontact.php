<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Orm_ApplicantEmergencyContact extends ORM {

	protected $_table_name = 'applicant_emergency_contact';
	protected $_primary_key = 'applicantId';

	protected $_table_columns = array(
		'applicantId' => NULL,
		'name' => NULL,
		'relation' => NULL,
		'mobile' => NULL,
		'email' => NULL
	);
	
}