<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Orm_MemberBeneficiary extends ORM {

	protected $_table_name = 'member_beneficiaries';

	protected $_table_columns = array(
		'id' => NULL,
		'memberId' => NULL,
		'name' => NULL,
		'birthDate' => NULL,
		'relation' => NULL
	);
	
}