<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Orm_MemberViolation extends ORM {

	protected $_table_name = 'member_violations';

	protected $_table_columns = array(
		'id' => NULL,
		'memberId' => NULL,
		'policyId' => NULL,
		'level' => NULL,
		'date' => NULL,
		'comment' => NULL
	);
	
}