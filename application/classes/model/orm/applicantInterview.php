<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Orm_ApplicantInterview extends ORM {

	protected $_table_name = 'applicant_interview';

	protected $_table_columns = array(
		'id' => NULL,
		'applicantId' => NULL,
		'scheduleApplicantInterviewId' => NULL
	);
	
}