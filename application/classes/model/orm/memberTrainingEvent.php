<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Orm_MemberTrainingEvent extends ORM {

	protected $_table_name = 'member_training_events';

	protected $_table_columns = array(
		'id' => NULL,
		'memberId' => NULL,
		'trainingEventId' => NULL,
		'remark' => NULL
	);
	
}