<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Orm_ApplicantParent extends ORM {

	protected $_table_name = 'applicant_parent';
	protected $_primary_key = 'applicantId';

	protected $_table_columns = array(
		'applicantId' => NULL,
		'motherName' => NULL,
		'motherOccupation' => NULL,
		'fatherName' => NULL,
		'fatherOccupation' => NULL
	);
	
}