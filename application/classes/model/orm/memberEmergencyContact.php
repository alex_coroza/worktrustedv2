<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Orm_MemberEmergencyContact extends ORM {

	protected $_table_name = 'member_emergency_contact';
	protected $_primary_key = 'memberId';

	protected $_table_columns = array(
		'memberId' => NULL,
		'name' => NULL,
		'relation' => NULL,
		'mobile' => NULL,
		'email' => NULL
	);
	
}