<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Orm_Feedback extends ORM {

	protected $_table_name = 'feedbacks';

	protected $_table_columns = array(
		'id' => NULL,
		'name' => NULL,
		'email' => NULL,
		'subject' => NULL,
		'message' => NULL,
		'datetime' => NULL
	);
	
}