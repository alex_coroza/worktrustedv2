<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Orm_ApplicantCharacterReference extends ORM {

	protected $_table_name = 'applicant_character_references';
	
	protected $_table_columns = array(
		'id' => NULL,
		'applicantId' => NULL,
		'name' => NULL,
		'occupation' => NULL,
		'company' => NULL,
		'mobile' => NULL,
		'email' => NULL
	);
	
}