<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Orm_Training extends ORM {

	protected $_table_name = 'trainings';

	protected $_table_columns = array(
		'id' => NULL,
		'name' => NULL,
		'description' => NULL
	);
	
}