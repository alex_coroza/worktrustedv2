<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Orm_CompanyAccount extends ORM {

	protected $_table_name = 'company_accounts';
	protected $_primary_key = 'userName';

	protected $_table_columns = array(
		'userName' => NULL,
		'companyId' => NULL,
		'password' => NULL,
		'salt' => NULL,
		'status' => NULL
	);
	
}