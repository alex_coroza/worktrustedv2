<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Orm_ApplicantBeneficiary extends ORM {

	protected $_table_name = 'applicant_beneficiaries';

	protected $_table_columns = array(
		'id' => NULL,
		'applicantId' => NULL,
		'name' => NULL,
		'birthDate' => NULL,
		'relation' => NULL
	);
	
}