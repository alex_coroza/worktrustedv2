<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Orm_ApplicantWorkHistory extends ORM {

	protected $_table_name = 'applicant_work_histories';

	protected $_table_columns = array(
		'id' => NULL,
		'applicantId' => NULL,
		'company' => NULL,
		'position' => NULL,
		'startDate' => NULL,
		'endDate' => NULL
	);
	
}