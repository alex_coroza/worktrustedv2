<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Orm_Applicant extends ORM {

	protected $_table_name = 'applicants';

	protected $_table_columns = array(
		'id' => NULL,
		'applicationDate' => NULL,
		'firstName' => NULL,
		'middleName' => NULL,
		'lastName' => NULL,
		'birthDate' => NULL,
		'gender' => NULL,
		'civilStatus' => NULL,
		'street' => NULL,
		'barangay' => NULL,
		'city' => NULL,
		'province' => NULL,
		'religion' => NULL,
		'weight' => NULL,
		'height' => NULL,
		'mobile' => NULL,
		'email' => NULL,
		'photo' => NULL,
		'tags' => NULL
	);
	
}