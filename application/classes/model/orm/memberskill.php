<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Orm_MemberSkill extends ORM {

	protected $_table_name = 'member_skills';

	protected $_table_columns = array(
		'id' => NULL,
		'memberId' => NULL,
		'skill' => NULL
	);
	
}