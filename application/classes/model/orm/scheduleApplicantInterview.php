<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Orm_ScheduleApplicantInterview extends ORM {

	protected $_table_name = 'schedule_applicant_interviews';

	protected $_table_columns = array(
		'id' => NULL,
		'start' => NULL,
		'end' => NULL
	);
	
}