<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Orm_MemberAccount extends ORM {

	protected $_table_name = 'member_accounts';
	protected $_primary_key = 'userName';

	protected $_table_columns = array(
		'userName' => NULL,
		'memberId' => NULL,
		'password' => NULL,
		'salt' => NULL,
		'type' => NULL,
		'status' => NULL
	);
	
}