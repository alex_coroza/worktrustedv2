<?php defined('SYSPATH') OR die('No direct access allowed.');


class Model_Member extends Model {


	// fetch list of members
	// supports pagination, limit, and search
	public function enlist($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$formatString = new Etc_FormatString();
		$memberOrm = ORM::factory('orm_member');

		// fetch by account type
		if(isset($options->accountType)) {
			$memberOrm
			->join('member_accounts')
			->on('orm_member.id', '=', 'member_accounts.memberId')
			->where('member_accounts.type', '=', $options->accountType);
		}

		// order by
		if(isset($options->orderBy)) {
			$memberOrm->order_by($options->orderBy[0], $options->orderBy[1]);
		}

		// search
		if(isset($options->search)) {
			$options->search = explode(' ', $formatString->multiFormat($options->search, array('spaceTrim', 'alphaNumericSymbolOnly')));
			$memberOrm->where_open();
			foreach ($options->search as $key => $word) {
				$memberOrm
				->or_where_open()
				->or_where('firstName', 'LIKE', "%$word%")
				->or_where('middleName', 'LIKE', "%$word%")
				->or_where('lastName', 'LIKE', "%$word%")
				->or_where('membershipDate', 'LIKE', "%$word%")
				->or_where('birthDate', 'LIKE', "%$word%")
				->or_where('gender', 'LIKE', "%$word%")
				->or_where('civilStatus', 'LIKE', "%$word%")
				->or_where('tags', 'LIKE', "%$word%")
				->or_where_close();
			}
			$memberOrm->where_close();
		}

		// limit and offset
		if(isset($options->currentPage) && isset($options->limit)) {
			$options->offset = $options->limit * ($options->currentPage - 1);
			$memberOrm->limit($options->limit)->offset($options->offset);
		}

		$results = array();
		foreach ($memberOrm->find_all() as $key => $member) {
			$readOptions = (object)array('id' => $member->id, 'populate' => $options->populate);
			$results[] = $this->read($readOptions);
		}
		
		return $results;
	}






	// returns an members's full info (almost all fields in member table)
	public function read($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$utility = new Etc_Utility();

		$member = ORM::factory('orm_member', $options->id);
		
		// getting basic info
		$memberInfo = new stdClass();
		$memberInfo->id = $member->id;
		$memberInfo->membershipDate = $member->membershipDate;
		$memberInfo->firstName = $member->firstName;
		$memberInfo->middleName = $member->middleName;
		$memberInfo->lastName = $member->lastName;
		$memberInfo->birthDate = $member->birthDate;
		$memberInfo->age = $utility->computeAge($member->birthDate);
		$memberInfo->gender = $member->gender;
		$memberInfo->civilStatus = $member->civilStatus;
		$memberInfo->street = $member->street;
		$memberInfo->barangay = $member->barangay;
		$memberInfo->city = $member->city;
		$memberInfo->province = $member->province;
		$memberInfo->height = $member->height;
		$memberInfo->weight = $member->weight;
		$memberInfo->religion = $member->religion;
		$memberInfo->mobile = $member->mobile;
		$memberInfo->email = $member->email;
		$memberInfo->photo = $member->photo;
		$memberInfo->tags = explode("|", $member->tags);
		$memberInfo->tags = (count($memberInfo->tags) == 1 && $memberInfo->tags[0] == '') ? array():$memberInfo->tags;

		$populateManager = new Etc_PopulateManager();

		// populate member's accountType if specified
		if($populateManager->check('account', $options->populate)) {
			$accountModel = new Model_Member_Account();
			$accountOptions = (object)array('memberId' => $options->id);
			$memberInfo->account = $accountModel->read($accountOptions);
			if(!isset($memberInfo->account->memberId)) unset($memberInfo->account);
		}

		// populate member_beneficiaries if specified
		if($populateManager->check('beneficiaries', $options->populate)) {
			$memberBeneficiaryModel = new Model_Member_Beneficiary();
			$beneficiaryOptions = (object)array('memberId' => $options->id);
			$memberInfo->beneficiaries = $memberBeneficiaryModel->enlist($beneficiaryOptions);
		}

		// populate member_character_references if specified 
		if($populateManager->check('characterReferences', $options->populate)) {
			$memberCharacterReferenceModel = new Model_Member_CharacterReference();
			$characterReferenceOptions = (object)array('memberId' => $options->id);
			$memberInfo->characterReferences = $memberCharacterReferenceModel->enlist($characterReferenceOptions);
		}

		// populate member_education if specified
		if($populateManager->check('education', $options->populate)) {
			$memberEducationModel = new Model_Member_Education();
			$educReadOptions = (object)array('memberId' => $options->id);
			$memberInfo->education = $memberEducationModel->read($educReadOptions);
			if(!isset($memberInfo->education->memberId)) unset($memberInfo->education);
		}

		// populate member_emergency_contact if specified
		if($populateManager->check('emergencyContact', $options->populate)) {
			$memberEmergencyContactModel = new Model_Member_EmergencyContact();
			$emergencyContactReadOptions = (object)array('memberId' => $options->id);
			$memberInfo->emergencyContact = $memberEmergencyContactModel->read($emergencyContactReadOptions);
			if(!isset($memberInfo->emergencyContact->memberId)) unset($memberInfo->emergencyContact);
		}

		// populate member_parent if specified
		if($populateManager->check('parent', $options->populate)) {
			$memberParentModel = new Model_Member_Parent();
			$parentReadOptions = (object)array('memberId' => $options->id);
			$memberInfo->parent = $memberParentModel->read($parentReadOptions);
			if(!isset($memberInfo->parent->memberId)) unset($memberInfo->parent);
		}

		// populate member_skills if specified
		if($populateManager->check('skills', $options->populate)) {
			$memberSkillModel = new Model_Member_Skill();
			$skillOptions = (object)array('memberId' => $options->id);
			$memberInfo->skills = $memberSkillModel->enlist($skillOptions);
		}

		// populate member_spouse if specified
		if($populateManager->check('spouse', $options->populate)) {
			$memberSpouseModel = new Model_Member_Spouse();
			$spouseReadOptions = (object)array('memberId' => $options->id);
			$memberInfo->spouse = $memberSpouseModel->read($spouseReadOptions);
			if(!isset($memberInfo->spouse->memberId)) unset($memberInfo->spouse);
		}

		// populate member_trainingEvents if specified
		if($populateManager->check('trainingEvents', $options->populate)) {
			$memberTrainingEventModel = new Model_Member_TrainingEvent();
			$trainingEventReadOptions = (object)array('memberId' => $options->id, 'populate' => $populateManager->currentPopulate);
			$memberInfo->trainingEvents = $memberTrainingEventModel->enlist($trainingEventReadOptions);
		}

		// populate member_work_histories if specified
		if($populateManager->check('workHistories', $options->populate)) {
			$memberWorkHistoryModel = new Model_Member_WorkHistory();
			$workHistoryOptions = (object)array('memberId' => $options->id);
			$memberInfo->workHistories = $memberWorkHistoryModel->enlist($workHistoryOptions);
		}

		// populate jobApplications if specified
		if($populateManager->check('jobApplications', $options->populate)) {
			$jobApplicantModel = new Model_Job_Applicant();
			$options = (object)array('memberId' => $options->id, 'populate' => $populateManager->currentPopulate);
			$memberInfo->jobApplications = $jobApplicantModel->enlist($options);
		}

		return $memberInfo;
	}




	// use for either create or update
	public function save($memberInfo) {
		$memberId = isset($memberInfo->id) ? $memberInfo->id:null;

		// saving member's basic info
		$formatString = new Etc_FormatString();
		$member = ORM::factory('orm_member', $memberId);
		$member->membershipDate = isset($memberInfo->membershipDate) ? date('Y-m-d H:i:s', strtotime($memberInfo->membershipDate)) : date('Y-m-d H:i:s', time());
		$member->firstName = $formatString->multiFormat($memberInfo->firstName, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$member->middleName = !isset($memberInfo->middleName) ? '' : $formatString->multiFormat($memberInfo->middleName, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$member->lastName = $formatString->multiFormat($memberInfo->lastName, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$member->birthDate = date('Y-m-d', strtotime($memberInfo->birthDate));
		$member->gender = $formatString->multiFormat($memberInfo->gender, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$member->civilStatus = $formatString->multiFormat($memberInfo->civilStatus, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$member->street = !isset($memberInfo->street) ? '' : $formatString->multiFormat($memberInfo->street, array('spaceTrim', 'alphaNumericSymbolOnly', 'upperFirstWord'));
		$member->barangay = $formatString->multiFormat($memberInfo->barangay, array('spaceTrim', 'alphaNumericSymbolOnly', 'upperFirstWord'));
		$member->city = $formatString->multiFormat($memberInfo->city, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$member->province = $formatString->multiFormat($memberInfo->province, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$member->height = $formatString->multiFormat($memberInfo->height, array('noSpace', 'numericOnly'));
		$member->weight = $formatString->multiFormat($memberInfo->weight, array('noSpace', 'numericOnly'));
		$member->religion = $formatString->multiFormat($memberInfo->religion, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$member->mobile = $formatString->multiFormat($memberInfo->mobile, array('noSpace', 'numericOnly'));
		$member->email = $formatString->multiFormat($memberInfo->email, array('noSpace', 'emailCharsOnly'));
		$member->tags = implode("|", $memberInfo->tags);
		$member->photo = $memberInfo->photo;
		$member->save();

		$memberId = $member->id;


		// saving member account info
		if(isset($memberInfo->account)) {
			$accountModel = new Model_Member_Account();
			$memberInfo->account->memberId = $memberId;
			$accountModel->save($memberInfo->account);
		}


		// saving member_beneficiaries
		if(isset($memberInfo->beneficiaries)) {
			DB::delete('member_beneficiaries')->where('memberId', '=', $memberId)->execute();

			$memberBeneficiaryModel = new Model_Member_Beneficiary();
			foreach ($memberInfo->beneficiaries as $beneficiary) {
				$beneficiary->memberId = $memberId;
				$memberBeneficiaryModel->save($beneficiary);
			}
		}


		// saving member character references
		if(isset($memberInfo->characterReferences)) {
			DB::delete('member_character_references')->where('memberId', '=', $memberId)->execute();

			$memberCharacterReferenceModel = new Model_Member_CharacterReference();
			foreach ($memberInfo->characterReferences as $characterReference) {
				$characterReference->memberId = $memberId;
				$memberCharacterReferenceModel->save($characterReference);
			}
		}


		// saving member education info
		if(isset($memberInfo->education)) {
			$memberEducationModel = new Model_Member_Education();
			$memberInfo->education->memberId = $memberId;
			$memberEducationModel->save($memberInfo->education);
		}


		// saving member emergencyContact info
		if(isset($memberInfo->emergencyContact)) {
			$memberEmergencyContactModel = new Model_Member_EmergencyContact();
			$memberInfo->emergencyContact->memberId = $memberId;
			$memberEmergencyContactModel->save($memberInfo->emergencyContact);
		}


		// saving member parent
		if(isset($memberInfo->parent)) {
			$memberParentModel = new Model_Member_Parent();
			$memberInfo->parent->memberId = $memberId;
			$memberParentModel->save($memberInfo->parent);
		}
		


		// saving member skill
		if(isset($memberInfo->skills)) {
			DB::delete('member_skills')->where('memberId', '=', $memberId)->execute();

			$memberSkillModel = new Model_Member_Skill();
			foreach ($memberInfo->skills as $skill) {
				$skill->memberId = $memberId;
				$memberSkillModel->save($skill);
			}
		}


		// saving member spouse
		if($memberInfo->civilStatus != 'Single' && $memberInfo->civilStatus != 'Divorced') {
			$memberSpouseModel = new Model_Member_Spouse();
			$memberInfo->spouse->memberId = $memberId;
			$memberSpouseModel->save($memberInfo->spouse);
		} else {
			DB::delete('member_spouse')->where('memberId', '=', $memberId)->execute();
		}


		// saving member trainingEvents
		if(isset($memberInfo->trainingEvents)) {
			DB::delete('member_training_events')->where('memberId', '=', $memberId)->execute();

			$memberTrainingEventModel = new Model_Member_TrainingEvent();
			foreach ($memberInfo->trainingEvents as $trainingEvent) {
				$trainingEvent->memberId = $memberId;
				$memberTrainingEventModel->save($trainingEvent);
			}
		}


		// saving member work histories
		if(isset($memberInfo->workHistories)) {
			DB::delete('member_work_histories')->where('memberId', '=', $memberId)->execute();

			$memberWorkHistoryModel = new Model_Member_WorkHistory();
			foreach ($memberInfo->workHistories as $workHistory) {
				$workHistory->memberId = $memberId;
				$memberWorkHistoryModel->save($workHistory);
			}
		}

		return (int)$memberId;
	}




	// updates members picture name in db
	// also deletes previous picture file
	public function memberPictureUpdate($info, $newPictureName) {
		if(file_exists($_SERVER['DOCUMENT_ROOT'].'/worktrustedV2/app/assets/profilePhotos/'.$info->photo) && $info->photo != 'default-avatar.jpg') {
			unlink($_SERVER['DOCUMENT_ROOT'].'/worktrustedV2/app/assets/profilePhotos/'.$info->photo);
		}
		$member = ORM::factory('orm_member', $info->id);
		$member->photo = $newPictureName;
		$member->save();
	}



	public function uploadProfilePicture($file) {
		$fileUpload = new Etc_FileUpload($file, array('jpg', 'png', 'jpeg','JPEG', 'JPG','PNG'), '512000', 'profilePhotos/');
		$fileUpload->uploadFile();
		echo json_encode($fileUpload->uploadInfoResponse());
	}



	public function uploadBase64ProfilePicture($base64File) {
		list($type, $base64File) = explode(';', $base64File);
		list(, $base64File)      = explode(',', $base64File);
		$base64File = base64_decode($base64File);
		$fileName = rand(0, 10000000).'_'.rand(0, 10000000).'_'.rand(0, 10000000).'.jpeg';
		file_put_contents($_SERVER['DOCUMENT_ROOT'].'/worktrustedV2/app/assets/profilePhotos/'.$fileName, $base64File);
		return array('newFileName' => $fileName);
	}





	// permanent delete member entry from database
	public function permanentDelete($memberId) {
		$member = ORM::factory('orm_member', $memberId);
		if(file_exists($_SERVER['DOCUMENT_ROOT'].'/worktrustedV2/app/assets/profilePhotos/'.$member->photo)) {
			unlink($_SERVER['DOCUMENT_ROOT'].'/worktrustedV2/app/assets/profilePhotos/'.$member->photo);
		}
		$member->delete();
	}




}