<?php defined('SYSPATH') OR die('No direct access allowed.');


class Model_Applicant_Spouse extends Model {



	public function read($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$utility = new Etc_Utility();
		$spouse = ORM::factory('orm_applicantSpouse')->where('applicantId', '=', $options->applicantId)->find();
		$spouseInfo = new stdClass();
		$spouseInfo->applicantId = $spouse->applicantId;
		$spouseInfo->firstName = $spouse->firstName;
		$spouseInfo->middleName = $spouse->middleName;
		$spouseInfo->lastName = $spouse->lastName;
		$spouseInfo->birthDate = $spouse->birthDate;
		$spouseInfo->age = $utility->computeAge($spouse->birthDate);
		$spouseInfo->occupation = $spouse->occupation;
		$spouseInfo->mobile = $spouse->mobile;
		$spouseInfo->email = $spouse->email;

		return $spouseInfo;
	}







	public function save($info) {
		$formatString = new Etc_FormatString();
		$applicantSpouse = ORM::factory('orm_applicantSpouse')->where('applicantId', '=', $info->applicantId)->find();
		$applicantSpouse->applicantId = $formatString->multiFormat($info->applicantId, array('noSpace', 'numericOnly'));
		$applicantSpouse->firstName = $formatString->multiFormat($info->firstName, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$applicantSpouse->middleName = $formatString->multiFormat($info->middleName, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$applicantSpouse->lastName = $formatString->multiFormat($info->lastName, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$applicantSpouse->birthDate = date('Y-m-d', strtotime($info->birthDate));
		$applicantSpouse->occupation = $formatString->multiFormat($info->occupation, array('spaceTrim', 'alphaNumericOnly', 'upperFirstWord'));
		$applicantSpouse->mobile = $formatString->multiFormat($info->mobile, array('noSpace', 'numericOnly'));
		$applicantSpouse->email = !isset($info->email) ? '' : $formatString->multiFormat($info->email, array('noSpace', 'emailCharsOnly'));
		$applicantSpouse->save();
	}


}