<?php defined('SYSPATH') OR die('No direct access allowed.');


class Model_Applicant_Education extends Model {



	public function read($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$education = ORM::factory('orm_applicantEducation')->where('applicantId', '=', $options->applicantId)->find();
		$educationInfo = new stdClass();
		$educationInfo->applicantId = $education->applicantId;
		$educationInfo->attainment = $education->attainment;
		$educationInfo->startYear = $education->startYear;
		$educationInfo->endYear = $education->endYear;
		$educationInfo->course = $education->course;

		return $educationInfo;
	}







	public function save($info) {
		$formatString = new Etc_FormatString();
		$applicantEducation = ORM::factory('orm_applicantEducation')->where('applicantId', '=', $info->applicantId)->find();
		$applicantEducation->applicantId = $formatString->multiFormat($info->applicantId, array('noSpace', 'numericOnly'));
		$applicantEducation->attainment = $formatString->multiFormat($info->attainment, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$applicantEducation->startYear = $formatString->multiFormat($info->startYear, array('noSpace', 'numericOnly'));
		$applicantEducation->endYear = $formatString->multiFormat($info->endYear, array('noSpace', 'numericOnly'));
		$applicantEducation->course = ($info->attainment == 'High School Graduate') ? '' : $formatString->multiFormat($info->course, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$applicantEducation->save();
	}


}