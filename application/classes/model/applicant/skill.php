<?php defined('SYSPATH') OR die('No direct access allowed.');


class Model_Applicant_Skill extends Model {


	public function enlist($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$skillOrm = ORM::factory('orm_applicantSkill');

		// order by
		if(isset($options->orderBy)) {
			$skillOrm->order_by($options->orderBy[0], $options->orderBy[1]);
		}

		// enlist from applicant
		if(isset($options->applicantId)) {
			$skillOrm->where('applicantId', '=', $options->applicantId);
		}

		$skillArray = array();
		foreach ($skillOrm->find_all() as $skill) {
			$readOptions = (object)array('id' => $skill->id, 'populate' => $options->populate);
			$skillArray[] = $this->read($readOptions);
		}

		return $skillArray;
	}





	public function read($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$skill = ORM::factory('orm_applicantSkill')->where('id', '=', $options->id)->find();
		$skillInfo = new stdClass();
		$skillInfo->id = $skill->id;
		$skillInfo->applicantId = $skill->applicantId;
		$skillInfo->skill = $skill->skill;

		return $skillInfo;
	}







	public function save($info) {
		$formatString = new Etc_FormatString();
		$applicantSkillModel = ORM::factory('orm_applicantSkill');
		$applicantSkillModel->applicantId = $formatString->multiFormat($info->applicantId, array('noSpace', 'numericOnly'));
		$applicantSkillModel->skill = $formatString->multiFormat($info->skill, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$applicantSkillModel->save();
	}


}