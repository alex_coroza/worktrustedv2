<?php defined('SYSPATH') OR die('No direct access allowed.');


class Model_Applicant_EmergencyContact extends Model {



	public function read($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$emergencyContact = ORM::factory('orm_applicantEmergencyContact')->where('applicantId', '=', $options->applicantId)->find();
		$emergencyContactInfo = new stdClass();
		$emergencyContactInfo->applicantId = $emergencyContact->applicantId;
		$emergencyContactInfo->name = $emergencyContact->name;
		$emergencyContactInfo->relation = $emergencyContact->relation;
		$emergencyContactInfo->mobile = $emergencyContact->mobile;
		$emergencyContactInfo->email = $emergencyContact->email;

		return $emergencyContactInfo;
	}







	public function save($info) {
		$formatString = new Etc_FormatString();
		$applicantEmergencyContact = ORM::factory('orm_applicantEmergencyContact')->where('applicantId', '=', $info->applicantId)->find();
		$applicantEmergencyContact->applicantId = $formatString->multiFormat($info->applicantId, array('noSpace', 'numericOnly'));
		$applicantEmergencyContact->name = $formatString->multiFormat($info->name, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$applicantEmergencyContact->relation = $formatString->multiFormat($info->relation, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$applicantEmergencyContact->mobile = $formatString->multiFormat($info->mobile, array('noSpace', 'numericOnly'));
		$applicantEmergencyContact->email = !isset($info->email) ? '' : $formatString->multiFormat($info->email, array('noSpace', 'emailCharsOnly'));
		$applicantEmergencyContact->save();
	}


}