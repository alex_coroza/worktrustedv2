<?php defined('SYSPATH') OR die('No direct access allowed.');


class Model_Applicant_WorkHistory extends Model {


	public function enlist($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$workHistoryOrm = ORM::factory('orm_applicantWorkHistory');

		// order by
		if(isset($options->orderBy)) {
			$workHistoryOrm->order_by($options->orderBy[0], $options->orderBy[1]);
		}

		// enlist from applicant
		if(isset($options->applicantId)) {
			$workHistoryOrm->where('applicantId', '=', $options->applicantId);
		}

		$workHistoryArray = array();
		foreach ($workHistoryOrm->find_all() as $workHistory) {
			$readOptions = (object)array('id' => $workHistory->id, 'populate' => $options->populate);
			$workHistoryArray[] = $this->read($readOptions);
		}

		return $workHistoryArray;
	}




	public function read($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$workHistory = ORM::factory('orm_applicantWorkHistory')->where('id', '=', $options->id)->find();
		$workHistoryInfo = new stdClass();
		$workHistoryInfo->id = $workHistory->id;
		$workHistoryInfo->applicantId = $workHistory->applicantId;
		$workHistoryInfo->company = $workHistory->company;
		$workHistoryInfo->position = $workHistory->position;
		$workHistoryInfo->startDate = $workHistory->startDate;
		$workHistoryInfo->endDate = $workHistory->endDate;

		return $workHistoryInfo;
	}




	public function save($info) {
		$formatString = new Etc_FormatString();
		$workHistoryModel = ORM::factory('orm_applicantWorkHistory');
		$workHistoryModel->applicantId = $formatString->multiFormat($info->applicantId, array('noSpace', 'numericOnly'));
		$workHistoryModel->company = $formatString->multiFormat($info->company, array('spaceTrim', 'alphaNumericOnly', 'upperFirstWord'));
		$workHistoryModel->position = $formatString->multiFormat($info->position, array('spaceTrim', 'alphaNumericOnly', 'upperFirstWord'));
		$workHistoryModel->startDate = date('Y-m-d', strtotime($info->startDate));
		$workHistoryModel->endDate = date('Y-m-d', strtotime($info->endDate));
		$workHistoryModel->save();
	}



}