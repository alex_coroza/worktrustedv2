<?php defined('SYSPATH') OR die('No direct access allowed.');


class Model_Applicant_Beneficiary extends Model {


	public function enlist($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$beneficiaryOrm = ORM::factory('orm_applicantBeneficiary');

		// order by
		if(isset($options->orderBy)) {
			$beneficiaryOrm->order_by($options->orderBy[0], $options->orderBy[1]);
		}

		// enlist from applicant
		if(isset($options->applicantId)) {
			$beneficiaryOrm->where('applicantId', '=', $options->applicantId);
		}

		$beneficiaryArray = array();
		foreach ($beneficiaryOrm->find_all() as $beneficiary) {
			$readOptions = (object)array('id' => $beneficiary->id, 'populate' => $options->populate);
			$beneficiaryArray[] = $this->read($readOptions);
		}

		return $beneficiaryArray;
	}




	public function read($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$utility = new Etc_Utility();
		$beneficiary = ORM::factory('orm_applicantBeneficiary')->where('id', '=', $options->id)->find();
		$beneficiaryInfo = new stdClass();
		$beneficiaryInfo->id = $beneficiary->id;
		$beneficiaryInfo->applicantId = $beneficiary->applicantId;
		$beneficiaryInfo->name = $beneficiary->name;
		$beneficiaryInfo->birthDate = $beneficiary->birthDate;
		$beneficiaryInfo->age = $utility->computeAge($beneficiary->birthDate);
		$beneficiaryInfo->relation = $beneficiary->relation;

		return $beneficiaryInfo;
	}







	public function save($info) {
		$formatString = new Etc_FormatString();
		$applicantBeneficiaryModel = ORM::factory('orm_applicantBeneficiary');
		$applicantBeneficiaryModel->applicantId = $formatString->multiFormat($info->applicantId, array('noSpace', 'numericOnly'));
		$applicantBeneficiaryModel->name = $formatString->multiFormat($info->name, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$applicantBeneficiaryModel->birthDate = date('Y-m-d', strtotime($info->birthDate));
		$applicantBeneficiaryModel->relation = $formatString->multiFormat($info->relation, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$applicantBeneficiaryModel->save();
	}


}