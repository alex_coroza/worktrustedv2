<?php defined('SYSPATH') OR die('No direct access allowed.');


class Model_Applicant_Parent extends Model {



	public function read($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$parent = ORM::factory('orm_applicantParent')->where('applicantId', '=', $options->applicantId)->find();
		$parentInfo = new stdClass();
		$parentInfo->applicantId = $parent->applicantId;
		$parentInfo->motherName = $parent->motherName;
		$parentInfo->motherOccupation = $parent->motherOccupation;
		$parentInfo->fatherName = $parent->fatherName;
		$parentInfo->fatherOccupation = $parent->fatherOccupation;

		return $parentInfo;
	}







	public function save($info) {
		$formatString = new Etc_FormatString();
		$applicantParent = ORM::factory('orm_applicantParent')->where('applicantId', '=', $info->applicantId)->find();
		$applicantParent->applicantId = $formatString->multiFormat($info->applicantId, array('noSpace', 'numericOnly'));
		$applicantParent->motherName = $formatString->multiFormat($info->motherName, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$applicantParent->motherOccupation = $formatString->multiFormat($info->motherOccupation, array('spaceTrim', 'alphaNumericOnly', 'upperFirstWord'));
		$applicantParent->fatherName = $formatString->multiFormat($info->fatherName, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$applicantParent->fatherOccupation = $formatString->multiFormat($info->fatherOccupation, array('spaceTrim', 'alphaNumericOnly', 'upperFirstWord'));
		$applicantParent->save();
	}


}