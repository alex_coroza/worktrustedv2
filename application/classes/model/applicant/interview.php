<?php defined('SYSPATH') OR die('No direct access allowed.');


class Model_Applicant_Interview extends Model {


	public function enlist($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$interviewOrm = ORM::factory('orm_applicantInterview');

		// order by
		if(isset($options->orderBy)) {
			$interviewOrm->order_by($options->orderBy[0], $options->orderBy[1]);
		}

		// enlist from schedule
		if(isset($options->scheduleApplicantInterviewId)) {
			$interviewOrm->where('scheduleApplicantInterviewId', '=', $options->scheduleApplicantInterviewId);
		}

		$results = array();
		foreach ($interviewOrm->find_all() as $interview) {
			$readOptions = (object)array('applicantId' => $interview->applicantId, 'scheduleApplicantInterviewId' => $options->scheduleApplicantInterviewId, 'populate' => $options->populate);
			$results[] = $this->read($readOptions);
		}

		return $results;
	}




	public function read($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$interviewOrm = ORM::factory('orm_applicantinterview');

		if(isset($options->id)) {
			$interviewOrm->where('id', '=', $options->id);
		}

		if(isset($options->applicantId)) {
			$interviewOrm->where('applicantId', '=', $options->applicantId);
		}

		$interview = $interviewOrm->find();
		$interviewInfo = new stdClass();
		$interviewInfo->id = $interview->id;
		$interviewInfo->applicantId = $interview->applicantId;
		$interviewInfo->scheduleApplicantInterviewId = $interview->scheduleApplicantInterviewId;

		$populateManager = new Etc_PopulateManager();

		if($populateManager->check('applicant', $options->populate)) {
			$applicantModel = new Model_Applicant();
			$applicantReadOptions = (object)array('id' => $interview->applicantId, 'populate' => $populateManager->currentPopulate);
			$interviewInfo->applicant = $applicantModel->read($applicantReadOptions);
			if(!isset($interviewInfo->applicant->id)) unset($interviewInfo->applicant);
		}

		if($populateManager->check('interview', $options->populate)) {
			$scheduleModel = new Model_Schedule_ApplicantInterview();
			$scheduleReadOptions = (object)array('id' => $interview->scheduleApplicantInterviewId);
			$interviewInfo->schedule = $scheduleModel->read($scheduleReadOptions);
			if(!isset($interviewInfo->schedule->id)) unset($interviewInfo->schedule);
		}

		return $interviewInfo;
	}




	public function save($info) {
		$formatString = new Etc_FormatString();
		$interviewModel = ORM::factory('orm_applicantinterview');
		$interviewModel->applicantId = $formatString->multiFormat($info->applicantId, array('noSpace', 'numericOnly'));
		$interviewModel->scheduleApplicantInterviewId = $formatString->multiFormat($info->scheduleApplicantInterviewId, array('noSpace', 'numericOnly'));
		$interviewModel->save();
	}



}
