<?php defined('SYSPATH') OR die('No direct access allowed.');


class Model_Applicant_CharacterReference extends Model {


	public function enlist($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$charRefOrm = ORM::factory('orm_applicantCharacterReference');		

		// order by
		if(isset($options->orderBy)) {
			$charRefOrm->order_by($options->orderBy[0], $options->orderBy[1]);
		}

		// enlist from applicant
		if(isset($options->applicantId)) {
			$charRefOrm->where('applicantId', '=', $options->applicantId);
		}

		$characterReferenceArray = array();
		foreach ($charRefOrm->find_all() as $characterReference) {
			$readOptions = (object)array('id' => $characterReference->id, 'populate' => $options->populate);
			$characterReferenceArray[] = $this->read($readOptions);
		}

		return $characterReferenceArray;
	}




	public function read($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$characterReference = ORM::factory('orm_applicantCharacterReference')->where('id', '=', $options->id)->find();
		$characterReferenceInfo = new stdClass();
		$characterReferenceInfo->id = $characterReference->id;
		$characterReferenceInfo->applicantId = $characterReference->applicantId;
		$characterReferenceInfo->name = $characterReference->name;
		$characterReferenceInfo->occupation = $characterReference->occupation;
		$characterReferenceInfo->company = $characterReference->company;
		$characterReferenceInfo->mobile = $characterReference->mobile;
		$characterReferenceInfo->email = $characterReference->email;

		return $characterReferenceInfo;
	}




	public function save($info) {
		$formatString = new Etc_FormatString();
		$characterReferenceModel = ORM::factory('orm_applicantCharacterReference');
		$characterReferenceModel->applicantId = $formatString->multiFormat($info->applicantId, array('noSpace', 'numericOnly'));
		$characterReferenceModel->name = $formatString->multiFormat($info->name, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$characterReferenceModel->occupation = $formatString->multiFormat($info->occupation, array('spaceTrim', 'alphaNumericOnly', 'upperFirstWord'));
		$characterReferenceModel->company = $formatString->multiFormat($info->company, array('spaceTrim', 'alphaNumericOnly', 'upperFirstWord'));
		$characterReferenceModel->mobile = $formatString->multiFormat($info->mobile, array('noSpace', 'numericOnly'));
		$characterReferenceModel->email = !isset($info->email) ? '' : $formatString->multiFormat($info->email, array('noSpace', 'emailCharsOnly'));
		$characterReferenceModel->save();
	}



}