<?php defined('SYSPATH') OR die('No direct access allowed.');


class Model_Company extends Model {

	// fetch list of companies
	// supports pagination, limit, and search
	public function enlist($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$formatString = new Etc_FormatString();
		$companyOrm = ORM::factory('orm_company');

		// order by
		if(isset($options->orderBy)) {
			$companyOrm->order_by($options->orderBy[0], $options->orderBy[1]);
		}

		// search
		if(isset($options->search)) {
			$options->search = explode(' ', $formatString->multiFormat($options->search, array('spaceTrim', 'alphaNumericSymbolOnly')));
			$companyOrm->where_open();
			foreach ($options->search as $key => $word) {
				$companyOrm
				->or_where_open()
				->or_where('name', 'LIKE', "%$word%")
				->or_where('description', 'LIKE', "%$word%")
				->or_where_close();
			}
			$companyOrm->where_close();
		}

		// limit and offset
		if(isset($options->currentPage) && isset($options->limit)) {
			$options->offset = $options->limit * ($options->currentPage - 1);
			$companyOrm->limit($options->limit)->offset($options->offset);
		}

		$results = array();
		foreach ($companyOrm->find_all() as $key => $company) {
			$readOptions = (object)array('id' => $company->id, 'populate' => $options->populate);
			$results[] = $this->read($readOptions);
		}
		
		return $results;
	}








	// returns an company's full info
	public function read($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$utility = new Etc_Utility();

		$company = ORM::factory('orm_company', $options->id);
		
		// getting basic info
		$companyInfo = new stdClass();
		$companyInfo->id = $company->id;
		$companyInfo->name = $company->name;
		$companyInfo->description = $company->description;
		$companyInfo->partnershipDate = $company->partnershipDate;
		$companyInfo->website = $company->website;
		$companyInfo->photo = $company->photo;

		$populateManager = new Etc_PopulateManager();

		// populate company's accountInfo
		if($populateManager->check('account', $options->populate)) {
			$accountModel = new Model_Company_Account();
			$accountOptions = (object)array('companyId' => $options->id, 'populate' => $populateManager->currentPopulate);
			$companyInfo->account = $accountModel->read($accountOptions);
			if(!isset($companyInfo->account->companyId)) unset($companyInfo->account);
		}

		// populate company's jobOpenings
		if($populateManager->check('jobOpenings', $options->populate)) {
			$jobOpeningModel = new Model_Job_Opening();
			$enlistOptions = (object)array('companyId' => $options->id, 'populate' => $populateManager->currentPopulate);
			$companyInfo->jobOpenings = $jobOpeningModel->enlist($enlistOptions);
		}


		return $companyInfo;
	}








	// use for either create or update
	public function save($companyInfo) {
		$companyId = isset($companyInfo->id) ? $companyInfo->id:null;

		// saving company's basic info
		$formatString = new Etc_FormatString();
		$company = ORM::factory('orm_company', $companyId);
		$company->name = $formatString->multiFormat($companyInfo->name, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$company->description = $formatString->multiFormat($companyInfo->description, array('spaceTrim', 'alphaNumericSymbolOnly'));
		$company->partnershipDate = isset($companyInfo->partnershipDate) ? date('Y-m-d H:i:s', strtotime($companyInfo->partnershipDate)) : date('Y-m-d H:i:s', time());
		$company->website = $formatString->multiFormat($companyInfo->website, array('spaceTrim'));
		$company->photo = $companyInfo->photo;
		$company->save();

		$companyId = $company->id;


		// saving company account info
		if(isset($companyInfo->account)) {
			$accountModel = new Model_Company_Account();
			$companyInfo->account->companyId = $companyId;
			$accountModel->save($companyInfo->account);
		}


		return (int)$companyId;
	}





	// updates company's photo name in db
	// also deletes previous photo file
	public function companyPictureUpdate($info, $newPictureName) {
		if(file_exists($_SERVER['DOCUMENT_ROOT'].'/worktrustedV2/app/assets/profilePhotos/'.$info->photo) && $info->photo != 'default-avatar.jpg') {
			unlink($_SERVER['DOCUMENT_ROOT'].'/worktrustedV2/app/assets/profilePhotos/'.$info->photo);
		}
		$company = ORM::factory('orm_company', $info->id);
		$company->photo = $newPictureName;
		$company->save();
	}






	public function uploadProfilePicture($file) {
		$fileUpload = new Etc_FileUpload($file, array('jpg', 'png', 'jpeg','JPEG', 'JPG','PNG'), '512000', 'profilePhotos/');
		$fileUpload->uploadFile();
		echo json_encode($fileUpload->uploadInfoResponse());
	}




	public function uploadBase64ProfilePicture($base64File) {
		list($type, $base64File) = explode(';', $base64File);
		list(, $base64File)      = explode(',', $base64File);
		$base64File = base64_decode($base64File);
		$fileName = rand(0, 10000000).'_'.rand(0, 10000000).'_'.rand(0, 10000000).'.jpeg';
		file_put_contents($_SERVER['DOCUMENT_ROOT'].'/worktrustedV2/app/assets/profilePhotos/'.$fileName, $base64File);
		return array('newFileName' => $fileName);
	}







}