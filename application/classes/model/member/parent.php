<?php defined('SYSPATH') OR die('No direct access allowed.');


class Model_Member_Parent extends Model {



	public function read($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$parent = ORM::factory('orm_memberParent')->where('memberId', '=', $options->memberId)->find();
		$parentInfo = new stdClass();
		$parentInfo->memberId = $parent->memberId;
		$parentInfo->motherName = $parent->motherName;
		$parentInfo->motherOccupation = $parent->motherOccupation;
		$parentInfo->fatherName = $parent->fatherName;
		$parentInfo->fatherOccupation = $parent->fatherOccupation;

		return $parentInfo;
	}







	public function save($info) {
		$formatString = new Etc_FormatString();
		$memberParent = ORM::factory('orm_memberParent')->where('memberId', '=', $info->memberId)->find();
		$memberParent->memberId = $formatString->multiFormat($info->memberId, array('noSpace', 'numericOnly'));
		$memberParent->motherName = $formatString->multiFormat($info->motherName, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$memberParent->motherOccupation = $formatString->multiFormat($info->motherOccupation, array('spaceTrim', 'alphaNumericOnly', 'upperFirstWord'));
		$memberParent->fatherName = $formatString->multiFormat($info->fatherName, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$memberParent->fatherOccupation = $formatString->multiFormat($info->fatherOccupation, array('spaceTrim', 'alphaNumericOnly', 'upperFirstWord'));
		$memberParent->save();
	}


}