<?php defined('SYSPATH') OR die('No direct access allowed.');


class Model_Member_WorkHistory extends Model {


	public function enlist($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$workHistoryOrm = ORM::factory('orm_memberWorkHistory');

		// order by
		if(isset($options->orderBy)) {
			$workHistoryOrm->order_by($options->orderBy[0], $options->orderBy[1]);
		}

		// enlist from member
		if(isset($options->memberId)) {
			$workHistoryOrm->where('memberId', '=', $options->memberId);
		}

		$workHistoryArray = array();
		foreach ($workHistoryOrm->find_all() as $workHistory) {
			$readOptions = (object)array('id' => $workHistory->id, 'populate' => $options->populate);
			$workHistoryArray[] = $this->read($readOptions);
		}

		return $workHistoryArray;
	}




	public function read($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$workHistory = ORM::factory('orm_memberWorkHistory')->where('id', '=', $options->id)->find();
		$workHistoryInfo = new stdClass();
		$workHistoryInfo->id = $workHistory->id;
		$workHistoryInfo->memberId = $workHistory->memberId;
		$workHistoryInfo->company = $workHistory->company;
		$workHistoryInfo->position = $workHistory->position;
		$workHistoryInfo->startDate = $workHistory->startDate;
		$workHistoryInfo->endDate = $workHistory->endDate;

		return $workHistoryInfo;
	}




	public function save($info) {
		$formatString = new Etc_FormatString();
		$workHistoryModel = ORM::factory('orm_memberWorkHistory');
		$workHistoryModel->memberId = $formatString->multiFormat($info->memberId, array('noSpace', 'numericOnly'));
		$workHistoryModel->company = $formatString->multiFormat($info->company, array('spaceTrim', 'alphaNumericOnly', 'upperFirstWord'));
		$workHistoryModel->position = $formatString->multiFormat($info->position, array('spaceTrim', 'alphaNumericOnly', 'upperFirstWord'));
		$workHistoryModel->startDate = date('Y-m-d', strtotime($info->startDate));
		$workHistoryModel->endDate = (isset($info->endDate)) ? date('Y-m-d', strtotime($info->endDate)) : null;
		$workHistoryModel->save();
	}



}