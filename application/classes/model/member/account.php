<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Member_Account extends Model {


	public function read($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$accountOrm = ORM::factory('orm_memberAccount');

		if(isset($options->userName)) {
			$accountOrm->where('userName', '=', $options->userName);
		}

		if(isset($options->memberId)) {
			$accountOrm->where('memberId', '=', $options->memberId);
		}

		$account = $accountOrm->find();
		$accountInfo = new stdClass();
		$accountInfo->userName = $account->userName;
		$accountInfo->memberId = $account->memberId;
		$accountInfo->type = $account->type;
		$accountInfo->status = $account->status;

		$populateManager = new Etc_PopulateManager();

		// populate member if specified
		if($populateManager->check('member', $options->populate)) {
			$memberModel = new Model_Member();
			$readOptions = (object)array('id' => $account->memberId);
			$accountInfo->member = $memberModel->read($readOptions);
			if(!isset($accountInfo->member->id)) unset($accountInfo->member);
		}

		return $accountInfo;
	}




	// use only updating account info
	public function save($accountInfo) {
		$utility = new Etc_Utility();
		$formatString = new Etc_FormatString();

		$account = ORM::factory('orm_memberAccount', $accountInfo->userName);
		$account->memberId = $accountInfo->memberId;
		// $account->salt = $accountInfo->salt;
		$account->type = $accountInfo->type;
		$account->status = $accountInfo->status;

		if(isset($accountInfo->newUserName)) {
			$account->userName = $formatString->multiFormat($accountInfo->newUserName, array('spaceTrim', 'alphaNumericOnly', 'upperCase'));
		}

		if(isset($accountInfo->password)) {
			$account->password = sha1(md5(sha1(strtoupper($accountInfo->password).$account->salt)));
		}

		$account->save();
	}




	// use only for creating new account
	public function create($accountInfo) {
		$utility = new Etc_Utility();
		$formatString = new Etc_FormatString();

		$accountInfo->salt = $utility->generateRandomString();

		if(!isset($accountInfo->userName)) {
			$accountInfo->userName = strtoupper($utility->generateRandomString()).ORM::factory('orm_memberAccount')->count_all();
		}

		$account = ORM::factory('orm_memberAccount');
		$account->userName = strtoupper($accountInfo->userName);
		$account->memberId = $accountInfo->memberId;
		$account->salt = $accountInfo->salt;
		$account->password = sha1(md5(sha1(strtoupper($accountInfo->userName).$accountInfo->salt)));
		$account->type = (isset($accountInfo->type)) ? $accountInfo->type : 'member';
		$account->status = 'active';
		$account->save();

		return $accountInfo;
	}




	// reset password equivalent to userName
	public function resetPassword($accountInfo) {
		$utility = new Etc_Utility();
		$account = ORM::factory('orm_memberAccount', $accountInfo->userName);

		$accountInfo->salt = $utility->generateRandomString();

		$account->salt = $accountInfo->salt;
		$account->password = sha1(md5(sha1(strtoupper($accountInfo->userName).$accountInfo->salt)));

		$account->save();

		return $accountInfo;
	}



	public function checkUserName($userName) {
		$formatString = new Etc_FormatString();
		$userName = $formatString->multiFormat($userName, array('spaceTrim', 'alphaNumericOnly', 'upperCase'));
		$account = ORM::factory('orm_memberAccount')->where('userName', '=', $userName);
		return (int)$account->count_all();
	}


	public function checkPassword($userName, $password) {
		$userName = strtoupper($userName);
		$account = ORM::factory('orm_memberAccount', $userName);
		$result = new stdClass();
		
		if($account->password == sha1(md5(sha1(strtoupper($password).$account->salt)))) {
			$result = 'success';
		} else {
			$result = 'failed';
		}

		return $result;
	}



}