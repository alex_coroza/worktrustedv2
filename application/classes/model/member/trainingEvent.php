<?php defined('SYSPATH') OR die('No direct access allowed.');


class Model_Member_TrainingEvent extends Model {


	public function enlist($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$memberTrainingEventOrm = ORM::factory('orm_memberTrainingEvent');

		// order by
		if(isset($options->orderBy)) {
			$memberTrainingEventOrm->order_by($options->orderBy[0], $options->orderBy[1]);
		}

		// enlist from trainingEvent
		if(isset($options->trainingEventId)) {
			$memberTrainingEventOrm->where('trainingEventId', '=', $options->trainingEventId);
		}

		// enlist from member
		if(isset($options->memberId)) {
			$memberTrainingEventOrm->where('memberId', '=', $options->memberId);
		}

		$results = array();
		foreach ($memberTrainingEventOrm->find_all() as $memberTrainingEvent) {
			// $readOptions = (object)array('memberId' => $memberTrainingEvent->applicantId, 'scheduleApplicantInterviewId' => $options->scheduleApplicantInterviewId, 'populate' => $options->populate);
			$readOptions = (object)array('id' => $memberTrainingEvent->id, 'populate' => $options->populate);
			$results[] = $this->read($readOptions);
		}

		return $results;
	}




	public function read($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$memberTrainingEventOrm = ORM::factory('orm_memberTrainingEvent');

		if(isset($options->id)) {
			$memberTrainingEventOrm->where('id', '=', $options->id);
		}

		if(isset($options->memberId)) {
			$memberTrainingEventOrm->where('memberId', '=', $options->memberId);
		}

		$memberTrainingEvent = $memberTrainingEventOrm->find();
		$memberTrainingEventInfo = new stdClass();
		$memberTrainingEventInfo->id = $memberTrainingEvent->id;
		$memberTrainingEventInfo->memberId = $memberTrainingEvent->memberId;
		$memberTrainingEventInfo->trainingEventId = $memberTrainingEvent->trainingEventId;
		$memberTrainingEventInfo->remark = $memberTrainingEvent->remark;

		$populateManager = new Etc_PopulateManager();

		if($populateManager->check('member', $options->populate)) {
			$memberModel = new Model_Member();
			$membertReadOptions = (object)array('id' => $memberTrainingEvent->memberId, 'populate' => $populateManager->currentPopulate);
			$memberTrainingEventInfo->member = $memberModel->read($membertReadOptions);
			if(!isset($memberTrainingEventInfo->member->id)) unset($memberTrainingEventInfo->member);
		}

		if($populateManager->check('trainingEvent', $options->populate)) {
			$trainingEventModel = new Model_Schedule_TrainingEvent();
			$trainingEventReadOptions = (object)array('id' => $memberTrainingEvent->trainingEventId, 'populate' => $populateManager->currentPopulate);
			$memberTrainingEventInfo->trainingEvent = $trainingEventModel->read($trainingEventReadOptions);
			if(!isset($memberTrainingEventInfo->trainingEvent->id)) unset($memberTrainingEventInfo->trainingEvent);
		}

		return $memberTrainingEventInfo;
	}




	public function save($info) {
		$formatString = new Etc_FormatString();
		$memberTrainingEventModel = ORM::factory('orm_memberTrainingEvent');
		$memberTrainingEventModel->memberId = $info->memberId;
		$memberTrainingEventModel->trainingEventId = $info->trainingEventId;
		$memberTrainingEventModel->remark = !isset($info->remark) ? 'PENDING' : $formatString->multiFormat($info->remark, array('noSpace', 'alphaOnly', 'upperCase'));
		$memberTrainingEventModel->save();
	}



}
