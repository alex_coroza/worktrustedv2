<?php defined('SYSPATH') OR die('No direct access allowed.');


class Model_Member_EmergencyContact extends Model {



	public function read($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$emergencyContact = ORM::factory('orm_memberEmergencyContact')->where('memberId', '=', $options->memberId)->find();
		$emergencyContactInfo = new stdClass();
		$emergencyContactInfo->memberId = $emergencyContact->memberId;
		$emergencyContactInfo->name = $emergencyContact->name;
		$emergencyContactInfo->relation = $emergencyContact->relation;
		$emergencyContactInfo->mobile = $emergencyContact->mobile;
		$emergencyContactInfo->email = $emergencyContact->email;

		return $emergencyContactInfo;
	}







	public function save($info) {
		$formatString = new Etc_FormatString();
		$memberEmergencyContact = ORM::factory('orm_memberEmergencyContact')->where('memberId', '=', $info->memberId)->find();
		$memberEmergencyContact->memberId = $formatString->multiFormat($info->memberId, array('noSpace', 'numericOnly'));
		$memberEmergencyContact->name = $formatString->multiFormat($info->name, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$memberEmergencyContact->relation = $formatString->multiFormat($info->relation, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$memberEmergencyContact->mobile = $formatString->multiFormat($info->mobile, array('noSpace', 'numericOnly'));
		$memberEmergencyContact->email = !isset($info->email) ? '' : $formatString->multiFormat($info->email, array('noSpace', 'emailCharsOnly'));
		$memberEmergencyContact->save();
	}


}