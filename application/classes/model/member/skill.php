<?php defined('SYSPATH') OR die('No direct access allowed.');


class Model_Member_Skill extends Model {


	public function enlist($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$skillOrm = ORM::factory('orm_memberSkill');

		// order by
		if(isset($options->orderBy)) {
			$skillOrm->order_by($options->orderBy[0], $options->orderBy[1]);
		}

		// enlist from member
		if(isset($options->memberId)) {
			$skillOrm->where('memberId', '=', $options->memberId);
		}

		$skillArray = array();
		foreach ($skillOrm->find_all() as $skill) {
			$readOptions = (object)array('id' => $skill->id, 'populate' => $options->populate);
			$skillArray[] = $this->read($readOptions);
		}

		return $skillArray;
	}





	public function read($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$skill = ORM::factory('orm_memberSkill')->where('id', '=', $options->id)->find();
		$skillInfo = new stdClass();
		$skillInfo->id = $skill->id;
		$skillInfo->memberId = $skill->memberId;
		$skillInfo->skill = $skill->skill;

		return $skillInfo;
	}







	public function save($info) {
		$formatString = new Etc_FormatString();
		$memberSkillModel = ORM::factory('orm_memberSkill');
		$memberSkillModel->memberId = $formatString->multiFormat($info->memberId, array('noSpace', 'numericOnly'));
		$memberSkillModel->skill = $formatString->multiFormat($info->skill, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$memberSkillModel->save();
	}


}