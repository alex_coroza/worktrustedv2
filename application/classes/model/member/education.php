<?php defined('SYSPATH') OR die('No direct access allowed.');


class Model_Member_Education extends Model {



	public function read($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$education = ORM::factory('orm_memberEducation')->where('memberId', '=', $options->memberId)->find();
		$educationInfo = new stdClass();
		$educationInfo->memberId = $education->memberId;
		$educationInfo->attainment = $education->attainment;
		$educationInfo->startYear = $education->startYear;
		$educationInfo->endYear = $education->endYear;
		$educationInfo->course = $education->course;

		return $educationInfo;
	}







	public function save($info) {
		$formatString = new Etc_FormatString();
		$memberEducation = ORM::factory('orm_memberEducation')->where('memberId', '=', $info->memberId)->find();
		$memberEducation->memberId = $formatString->multiFormat($info->memberId, array('noSpace', 'numericOnly'));
		$memberEducation->attainment = $formatString->multiFormat($info->attainment, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$memberEducation->startYear = $formatString->multiFormat($info->startYear, array('noSpace', 'numericOnly'));
		$memberEducation->endYear = $formatString->multiFormat($info->endYear, array('noSpace', 'numericOnly'));
		$memberEducation->course = ($info->attainment == 'High School Graduate' || $info->attainment == 'Elementary Graduate') ? '' : $formatString->multiFormat($info->course, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$memberEducation->save();
	}


}