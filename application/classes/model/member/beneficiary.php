<?php defined('SYSPATH') OR die('No direct access allowed.');


class Model_Member_Beneficiary extends Model {


	public function enlist($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$beneficiaryOrm = ORM::factory('orm_memberBeneficiary');

		// order by
		if(isset($options->orderBy)) {
			$beneficiaryOrm->order_by($options->orderBy[0], $options->orderBy[1]);
		}

		// enlist from member
		if(isset($options->memberId)) {
			$beneficiaryOrm->where('memberId', '=', $options->memberId);
		}

		$beneficiaryArray = array();
		foreach ($beneficiaryOrm->find_all() as $beneficiary) {
			$readOptions = (object)array('id' => $beneficiary->id, 'populate' => $options->populate);
			$beneficiaryArray[] = $this->read($readOptions);
		}

		return $beneficiaryArray;
	}




	public function read($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$utility = new Etc_Utility();
		$beneficiary = ORM::factory('orm_memberBeneficiary')->where('id', '=', $options->id)->find();
		$beneficiaryInfo = new stdClass();
		$beneficiaryInfo->id = $beneficiary->id;
		$beneficiaryInfo->memberId = $beneficiary->memberId;
		$beneficiaryInfo->name = $beneficiary->name;
		$beneficiaryInfo->birthDate = $beneficiary->birthDate;
		$beneficiaryInfo->age = $utility->computeAge($beneficiary->birthDate);
		$beneficiaryInfo->relation = $beneficiary->relation;

		return $beneficiaryInfo;
	}







	public function save($info) {
		$formatString = new Etc_FormatString();
		$memberBeneficiaryModel = ORM::factory('orm_memberBeneficiary');
		$memberBeneficiaryModel->memberId = $formatString->multiFormat($info->memberId, array('noSpace', 'numericOnly'));
		$memberBeneficiaryModel->name = $formatString->multiFormat($info->name, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$memberBeneficiaryModel->birthDate = date('Y-m-d', strtotime($info->birthDate));
		$memberBeneficiaryModel->relation = $formatString->multiFormat($info->relation, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$memberBeneficiaryModel->save();
	}


}