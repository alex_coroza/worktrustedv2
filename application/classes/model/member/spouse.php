<?php defined('SYSPATH') OR die('No direct access allowed.');


class Model_Member_Spouse extends Model {



	public function read($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$utility = new Etc_Utility();
		$spouse = ORM::factory('orm_memberSpouse')->where('memberId', '=', $options->memberId)->find();
		$spouseInfo = new stdClass();
		$spouseInfo->memberId = $spouse->memberId;
		$spouseInfo->firstName = $spouse->firstName;
		$spouseInfo->middleName = $spouse->middleName;
		$spouseInfo->lastName = $spouse->lastName;
		$spouseInfo->birthDate = $spouse->birthDate;
		$spouseInfo->age = $utility->computeAge($spouse->birthDate);
		$spouseInfo->occupation = $spouse->occupation;
		$spouseInfo->mobile = $spouse->mobile;
		$spouseInfo->email = $spouse->email;

		return $spouseInfo;
	}







	public function save($info) {
		$formatString = new Etc_FormatString();
		$memberSpouse = ORM::factory('orm_memberSpouse')->where('memberId', '=', $info->memberId)->find();
		$memberSpouse->memberId = $formatString->multiFormat($info->memberId, array('noSpace', 'numericOnly'));
		$memberSpouse->firstName = $formatString->multiFormat($info->firstName, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$memberSpouse->middleName = $formatString->multiFormat($info->middleName, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$memberSpouse->lastName = $formatString->multiFormat($info->lastName, array('spaceTrim', 'alphaOnly', 'upperFirstWord'));
		$memberSpouse->birthDate = date('Y-m-d', strtotime($info->birthDate));
		$memberSpouse->occupation = $formatString->multiFormat($info->occupation, array('spaceTrim', 'alphaNumericOnly', 'upperFirstWord'));
		$memberSpouse->mobile = $formatString->multiFormat($info->mobile, array('noSpace', 'numericOnly'));
		$memberSpouse->email = !isset($info->email) ? '' : $formatString->multiFormat($info->email, array('noSpace', 'emailCharsOnly'));
		$memberSpouse->save();
	}


}