<?php defined('SYSPATH') OR die('No direct access allowed.');


class Model_Company_Account extends Model {


	public function read($options = NULL) {
		if($options === NULL) {
			$options = new stdClass();
		}
		$options->populate = isset($options->populate) ? $options->populate : array();

		$accountOrm = ORM::factory('orm_companyAccount');

		if(isset($options->userName)) {
			$accountOrm->where('userName', '=', $options->userName);
		}

		if(isset($options->companyId)) {
			$accountOrm->where('companyId', '=', $options->companyId);
		}

		$account = $accountOrm->find();
		$accountInfo = new stdClass();
		$accountInfo->userName = $account->userName;
		$accountInfo->companyId = $account->companyId;
		$accountInfo->status = $account->status;

		$populateManager = new Etc_PopulateManager();

		// populate company if specified
		if($populateManager->check('company', $options->populate)) {
			$companyModel = new Model_Company();
			$readOptions = (object)array('id' => $account->companyId);
			$accountInfo->company = $companyModel->read($readOptions);
			if(!isset($accountInfo->company->id)) unset($accountInfo->company);
		}

		return $accountInfo;
	}





	// use only updating account info
	public function save($accountInfo) {
		$utility = new Etc_Utility();
		$formatString = new Etc_FormatString();

		$account = ORM::factory('orm_companyAccount', $accountInfo->userName);
		$account->companyId = $accountInfo->companyId;
		$account->status = $accountInfo->status;

		if(isset($accountInfo->newUserName)) {
			$account->userName = $formatString->multiFormat($accountInfo->newUserName, array('spaceTrim', 'alphaNumericOnly', 'upperCase'));
		}

		if(isset($accountInfo->password)) {
			$account->password = sha1(md5(sha1(strtoupper($accountInfo->password).$account->salt)));
		}

		$account->save();
	}	





	// use only for creating new account
	public function create($accountInfo) {
		$utility = new Etc_Utility();
		$formatString = new Etc_FormatString();

		$accountInfo->salt = $utility->generateRandomString();

		if(!isset($accountInfo->userName)) {
			$accountInfo->userName = strtoupper($utility->generateRandomString()).ORM::factory('orm_companyAccount')->count_all();
		}

		$account = ORM::factory('orm_companyAccount');
		$account->userName = strtoupper($accountInfo->userName);
		$account->companyId = $accountInfo->companyId;
		$account->salt = $accountInfo->salt;
		$account->password = sha1(md5(sha1(strtoupper($accountInfo->userName).$accountInfo->salt)));
		$account->status = 'active';
		$account->save();

		return $accountInfo;
	}




	// reset password equivalent to userName
	public function resetPassword($accountInfo) {
		$utility = new Etc_Utility();
		$account = ORM::factory('orm_companyAccount', $accountInfo->userName);

		$accountInfo->salt = $utility->generateRandomString();

		$account->salt = $accountInfo->salt;
		$account->password = sha1(md5(sha1(strtoupper($accountInfo->userName).$accountInfo->salt)));

		$account->save();

		return $accountInfo;
	}






	public function checkUserName($userName) {
		$formatString = new Etc_FormatString();
		$userName = $formatString->multiFormat($userName, array('spaceTrim', 'alphaNumericOnly', 'upperCase'));
		$account = ORM::factory('orm_companyAccount')->where('userName', '=', $userName);
		return (int)$account->count_all();
	}







	public function checkPassword($userName, $password) {
		$userName = strtoupper($userName);
		$account = ORM::factory('orm_companyAccount', $userName);
		$result = new stdClass();
		
		if($account->password == sha1(md5(sha1(strtoupper($password).$account->salt)))) {
			$result = 'success';
		} else {
			$result = 'failed';
		}

		return $result;
	}









}