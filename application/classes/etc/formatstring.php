<?php defined('SYSPATH') OR die('No direct access allowed.');

class Etc_FormatString {

	/**
	 * trims all whitespaces outside the string then all the consecutive white spaces within the string are converted into a single space 
	 */
	public function spaceTrim($stringSample) {
		return trim(preg_replace('/\s+/', ' ', $stringSample));
	}

	// removes all spaces
	public function noSpace($stringSample) {
		return preg_replace('/\s+/', '', $stringSample);
	}

	/**
	 * removes non alpha characters in the string
	 */
	public function alphaOnly($stringSample) {
		return preg_replace('/[^A-Z a-z]/', '', $stringSample);
	}

	/**
	 * removes non numeric characters in the string
	 */
	public function numericOnly($stringSample) {
		return preg_replace('/[^0-9]/', '', $stringSample);
	}

	/**
	 * removes non alphanumeric characters in the string
	 */
	public function alphaNumericOnly($stringSample) {
		return preg_replace('/[^A-Za-zñÑ 0-9]/', '', $stringSample);
	}

	public function alphaNumericSymbolOnly($stringSample) {
		return preg_replace('/[^a-zA-Z0-9ñÑ ,-._#&!?]/', '', $stringSample);
	}

	public function emailCharsOnly($stringSample) {
		return preg_replace('/[^A-Za-z 0-9@._]/', '', $stringSample);
	}


	// returns uppercased first letter per word
	public function upperFirstWord($stringSample) {
		return ucwords($stringSample);
	}


	// returns uppercased string
	public function upperCase($stringSample) {
		return strtoupper($stringSample);
	}


	// returns lowercased string
	public function lowerCase($stringSample) {
		return strtolower($stringSample);
	}


	// returns empty string if the sampleString is null or undefined
	public function optionalField($stringSample) {
		if(isset($stringSample) || is_null($stringSample)) {
			return '';
		} else {
			return $stringSample;
		}
	}





	// return formatted string based on requested format functions
	public function multiFormat($stringSample, $formatFunctions = array()) {

		if(in_array('optionalField', $formatFunctions)) {
			$stringSample = $this->optionalField($stringSample);
		}


		if(in_array('spaceTrim', $formatFunctions)) {
			$stringSample = $this->spaceTrim($stringSample);
		}


		if(in_array('noSpace', $formatFunctions)) {
			$stringSample = $this->noSpace($stringSample);
		}


		if(in_array('alphaOnly', $formatFunctions)) {
			$stringSample = $this->alphaOnly($stringSample);
		}


		if(in_array('numericOnly', $formatFunctions)) {
			$stringSample = $this->numericOnly($stringSample);
		}


		if(in_array('alphaNumericOnly', $formatFunctions)) {
			$stringSample = $this->alphaNumericOnly($stringSample);
		}


		if(in_array('alphaNumericSymbolOnly', $formatFunctions)) {
			$stringSample = $this->alphaNumericSymbolOnly($stringSample);
		}


		if(in_array('emailCharsOnly', $formatFunctions)) {
			$stringSample = $this->emailCharsOnly($stringSample);
		}


		if(in_array('upperFirstWord', $formatFunctions)) {
			$stringSample = $this->upperFirstWord($stringSample);
		}


		if(in_array('upperCase', $formatFunctions)) {
			$stringSample = $this->upperCase($stringSample);
		}


		if(in_array('lowerCase', $formatFunctions)) {
			$stringSample = $this->lowerCase($stringSample);
		}



		return $stringSample;

	}
}