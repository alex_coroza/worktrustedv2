<?php defined('SYSPATH') OR die('No direct access allowed.');

//all the methods here returns a boolean value
class Etc_CustomValidation {

	//validates date in this format YYYY-MM-DD
	public function dateCheck($sample) {
		$date_exploded =  explode('-', $sample);
		if(empty($date_exploded[2]) or empty($date_exploded[1]) or empty($date_exploded[0])) {
			return false;
		} else {
			$year = $date_exploded[0];
			$month = $date_exploded[1];
			$day = $date_exploded[2];
			if(checkdate($month, $day, $year))
				return true;
			else
				return false;
		}
	}

	//checks if the string only contains alpha chars and spaces
	public function alpha($sample) {
		if(preg_match('/[^A-Za-z ñÑ]/', $sample))
			return false;
		else
			return true;
	}
		
	//checks if the string only contains alphanumeric chars and spaces
	public function alphaNumeric($sample) {
		if(preg_match('/[^A-Za-z0-9 ñÑ]/', $sample))
			return false;
		else
			return true;
	}
	
	//checks if the string is a valid email
	public function email($sample) {
		if(preg_match('/^[\w\.\-]+@([\w\-]+\.)+[a-z]+$/i', $sample))
			return true;
		else
			return false;
	}


} // end of class