<?php defined('SYSPATH') OR die('No direct access allowed.');

class Etc_Utility {


	// generates a random string(all caps) with length based on $length
	public function generateRandomString($length = 10) {
		$characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		$randomString ="";
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}

		return $randomString;
	}



	public function computeAge($birthDate) {
		$date = new DateTime($birthDate);
		$now = new DateTime();
		$interval = $now->diff($date);
		return $interval->y;
	}




	public function login($userName, $password, $loginType) {
		if($loginType == 'companyLogin') {
			$accountModel = new Model_Company_Account();
		} else {
			$accountModel = new Model_Member_Account();
		}

		$result = new stdClass();
		$userName = strtoupper($userName);
		
		if($accountModel->checkPassword($userName, $password) == 'success') {
			session_start();
			$readOptions = (object)array('userName' => $userName);
			$account = $accountModel->read($readOptions);

			if($loginType == 'companyLogin') {
				$result->type = 'company';
			} else {
				$result->type = $account->type;
			}
			
			$_SESSION['user'] = $account->userName;
			$_SESSION['type'] = $result->type;
			$result->message = 'success';
		} else {
			$result->message = 'failed';
			$result->type = '';
		}

		return $result;
	}


	
	public function logout() {
		session_start();
		session_destroy();
	}




	public function redirectUnauthorizedUser() {
		session_start();
		$accountModel = new Model_Member_Account();

		if(!isset($_SESSION['user']) || !isset($_SESSION['type'])) {
			if($_SERVER['REQUEST_URI'] == '/worktrustedV2/') {
				// no action
			} else {
				echo '<script>window.location="/worktrustedV2/"</script>';
			}
		} else {
			if($_SERVER['REQUEST_URI'] == '/worktrustedV2/'.$_SESSION['type'].'Page/') {
				// no action
			} else {
				echo '<script>window.location="/worktrustedV2/'.$_SESSION['type'].'Page/"</script>';
			}
		}
	}




	public function fetchLoggedUser($loginType) {
		session_start();
		if($loginType == 'company') {
			$accountModel = new Model_Company_Account();
		} else {
			$accountModel = new Model_Member_Account();
		}

		$options = (object)array(
			'userName' => $_SESSION['user'],
			'populate' => ($loginType == 'company') ? array('company') : array('member')
		);
		
		return $accountModel->read($options);
	}








}