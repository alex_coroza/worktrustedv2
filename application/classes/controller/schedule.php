<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Schedule extends Controller {




	// MEMBERSHIP APPLICANT INTERVIEW
	public function action_enlistApplicantInterviewSchedules() {
		$post = json_decode(file_get_contents("php://input"));
		$options = $post;
		$scheduleModel = new Model_Schedule_ApplicantInterview();
		echo json_encode($scheduleModel->enlist($options));
	}

	public function action_readApplicantInterviewSchedule() {
		$post = json_decode(file_get_contents("php://input"));
		$options = $post;
		$scheduleModel = new Model_Schedule_ApplicantInterview();
		echo json_encode($scheduleModel->read($options));
	}

	public function action_saveApplicantInterviewSchedule() {
		$post = json_decode(file_get_contents("php://input"));
		$info = $post->info;
		$scheduleModel = new Model_Schedule_ApplicantInterview();
		echo json_encode($scheduleModel->save($info));
	}

	public function action_deleteApplicantInterviewSchedule() {
		$post = json_decode(file_get_contents("php://input"));
		$id = $post->id;
		$scheduleModel = new Model_Schedule_ApplicantInterview();
		echo json_encode($scheduleModel->delete($id));
	}












	// TRAININGS
	public function action_enlistTrainingEvents() {
		$post = json_decode(file_get_contents("php://input"));
		$options = $post;
		$trainingEventModel = new Model_Schedule_TrainingEvent();
		echo json_encode($trainingEventModel->enlist($options));
	}

	public function action_readTrainingEvent() {
		$post = json_decode(file_get_contents("php://input"));
		$options = $post;
		$trainingEventModel = new Model_Schedule_TrainingEvent();
		echo json_encode($trainingEventModel->read($options));
	}

	public function action_saveTrainingEvent() {
		$post = json_decode(file_get_contents("php://input"));
		$info = $post->info;
		$trainingEventModel = new Model_Schedule_TrainingEvent();
		echo json_encode($trainingEventModel->save($info));
	}

	public function action_deleteTrainingEvent() {
		$post = json_decode(file_get_contents("php://input"));
		$id = $post->id;
		$trainingEventModel = new Model_Schedule_TrainingEvent();
		echo json_encode($trainingEventModel->delete($id));
	}








	// SAMPLE EVENTS/SCHEDULE
	public function action_enlistSampleSchedules() {
		$sampleEvents = array(
			array('title' => 'Garage Sale ni Kumareng Lorna', 'description' => 'kjkasjdlkajsldjkasd', 'start' => '2016-05-05', 'end' => '2016-05-06'),
			array('title' => 'Qwe Asd', 'description' => '23123wqzxckjkasjdlkajsldasdasdajkasd', 'start' => '2016-05-15', 'end' => '2016-05-15')
		);
		
		echo json_encode($sampleEvents);
	}


	


}
