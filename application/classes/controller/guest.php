<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Guest extends Controller {
	
	public function action_index() {
		$utility = new Etc_Utility();
		$utility->redirectUnauthorizedUser();
		$this->response->body(View::factory('guest'));
	}


	public function action_sample() {
		/*HOW TO CALL AN ORM MODEL*/
		// $applicantModel = ORM::factory('orm_applicant');
		// foreach ($applicantModel->find_all() as $applicant) {
		// 	echo $applicant->firstName.'<br>';
		// }

		/*HOW TO CALL A MODEL*/
		// $sampleModel = new Model_Table_Applicant();
		// echo json_encode($sampleModel->read('2', array('beneficiaries', 'characterReferences', 'education', 'emergencyContact', 'parent', 'skills', 'spouse', 'workHistories')));

		/*HOW TO CALL ETC MODELS*/
		// $formatString = new Etc_FormatString();
		// echo $formatString->multiFormat('    ang   pogi     # @ _  .   ko    !    !!', array('noSpace', 'emailCharsOnly'));

		echo date('Y-m-d h:m:s', time());
	}

}