<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Company extends Controller {

	public function action_enlist() {
		$post = json_decode(file_get_contents("php://input"));
		$options = $post;
		$companyModel = new Model_Company();
		echo json_encode($companyModel->enlist($options));
	}


	public function action_read() {
		$post = json_decode(file_get_contents("php://input"));
		$readParams = $post;
		$companyModel = new Model_Company();
		echo json_encode($companyModel->read($readParams));
	}

	public function action_save() {
		$post = json_decode(file_get_contents("php://input"));
		$info = $post->info;
		$companyModel = new Model_Company();
		echo json_encode($companyModel->save($info));
	}


	public function action_uploadProfilePicture() {
		$companyModel = new Model_Company();
		$pictureFile = $_FILES['file'];
		$companyModel->uploadProfilePicture($pictureFile);
	}


	

	public function action_permanentDelete() {
		$post = json_decode(file_get_contents("php://input"));
		$id = $post->id;
		$companyModel = new Model_Company();
		echo json_encode($companyModel->permanentDelete($id));
	}



	public function action_companyPictureUpdate() {
		$post = json_decode(file_get_contents("php://input"));
		$info = $post->info;
		$newPictureName = $post->newPictureName;
		$companyModel = new Model_Company();
		echo json_encode($companyModel->companyPictureUpdate($info, $newPictureName));
	}



	public function action_uploadBase64ProfilePicture() {
		$post = json_decode(file_get_contents("php://input"));
		$base64File = $post->base64File;
		$companyModel = new Model_Company();
		echo json_encode($companyModel->uploadBase64ProfilePicture($base64File));
	}
	

	



}
