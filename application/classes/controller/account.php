<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Account extends Controller {

	public function action_login() {
		$post = json_decode(file_get_contents("php://input"));
		$userName = $post->userName;
		$password = $post->password;
		$loginType = $post->loginType;
		$utility = new Etc_Utility();
		echo json_encode($utility->login($userName, $password, $loginType));
	}




	public function action_logout() {
		$post = json_decode(file_get_contents("php://input"));
		$utility = new Etc_Utility();
		echo json_encode($utility->logout());
	}




	public function action_fetchLoggedUser() {
		$post = json_decode(file_get_contents("php://input"));
		$type = $post->type;
		$utility = new Etc_Utility();
		$currentUser = $utility->fetchLoggedUser($type);
		echo json_encode($currentUser);
	}



	public function action_create() {
		$post = json_decode(file_get_contents("php://input"));
		$accountInfo = $post->info;
		$type = $post->type;
		
		if($type == 'member') {
			$accountModel = new Model_Member_Account();
		} else if($type == 'company') {
			$accountModel = new Model_Company_Account();
		}

		$accountModel->create($accountInfo);
	}



	public function action_save() {
		$post = json_decode(file_get_contents("php://input"));
		$accountInfo = $post->info;
		$type = $post->type;
		
		if($type == 'member') {
			$accountModel = new Model_Member_Account();
		} else if($type == 'company') {
			$accountModel = new Model_Company_Account();
		}

		$accountModel->save($accountInfo);
	}



	public function action_resetPassword() {
		$post = json_decode(file_get_contents("php://input"));
		$accountInfo = $post->info;
		$type = $post->type;
		
		if($type == 'member') {
			$accountModel = new Model_Member_Account();
		} else if($type == 'company') {
			$accountModel = new Model_Company_Account();
		}

		$accountModel->resetPassword($accountInfo);
	}




	public function action_checkUserName() {
		$post = json_decode(file_get_contents("php://input"));
		$userName = $post->userName;
		$type = $post->type;
		
		if($type == 'member') {
			$accountModel = new Model_Member_Account();
		} else if($type == 'company') {
			$accountModel = new Model_Company_Account();
		}

		echo json_encode($accountModel->checkUserName($userName));
	}



	public function action_checkPassword() {
		$post = json_decode(file_get_contents("php://input"));
		$userName = $post->userName;
		$password = $post->password;
		$type = $post->type;
		
		if($type == 'member') {
			$accountModel = new Model_Member_Account();
		} else if($type == 'company') {
			$accountModel = new Model_Company_Account();
		}

		echo $accountModel->checkPassword($userName, $password);
	}



}
