<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Test_Schedule extends Controller {
	
	public function action_index() {
		echo 'Input Test/Sample for schedule model';
	}



	public function action_saveApplicantInterviewSchedule() {
		$scheduleModel = new Model_Schedule_ApplicantInterview();

		$info = new stdClass();
		$info->id = 3;
		$info->start = '2016-03-05';
		$info->end = '2016-03-05';
		$info->applicants = array(
			(object)array('applicantId' => '27', 'firstName' => 'kahit ano since hindi naman eto isasama sa pagsasave'),
			(object)array('applicantId' => '29', 'firstName' => 'kahit ano since hindi naman eto isasama sa pagsasave'),
			(object)array('applicantId' => '30', 'firstName' => 'kahit ano since hindi naman eto isasama sa pagsasave'),
		);

		$scheduleModel->save($info);
	}




	public function action_enlistApplicantInterviewSchedules() {
		$scheduleModel = new Model_Schedule_ApplicantInterview();

		$options = new stdClass();
		// $options->populate = array('applicantInterviews' => array('applicant'));
		// $options->monthYear = 'March 2016';
		$options->upcoming = true;
		$options->orderBy = array('start', 'asc');
		// $options->search = '2016';
		// $options->currentPage = 1;
		// $options->limit = 10;

		echo json_encode($scheduleModel->enlist($options));
	}


	public function action_readApplicantInterviewSchedules() {
		$scheduleModel = new Model_Schedule_ApplicantInterview();

		$options = (object)array(
			'id' => 10,
			'populate' => array(
				'applicantInterviews' => array(
					'applicant'
				)
			)
		);

		echo json_encode($scheduleModel->read($options));
	}


}