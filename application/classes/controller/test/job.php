<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Test_Job extends Controller {
	
	public function action_index() {
		echo 'Input Test/Sample for jobOpenings model';
	}



	public function action_enlistJobOpenings() {
		$jobOpeningModel = new Model_Job_Opening();

		$options = new stdClass();
		$options->populate = array('company');
		$options->search = ')*)(ti*()*t';
		$options->currentPage = 1;
		$options->limit = 10;

		echo json_encode($jobOpeningModel->enlist($options));
	}


	public function action_saveJobOpening() {
		$jobOpeningModel = new Model_Job_Opening();

		$jobOpening = (object)array(
			'id' => 2,
			'companyId' => 1,
			'title' => 'title na pogi',
			'description' => 'sample description',
			'minSalary' => 10000,
			'maxSalary' => 30000,
			'employmentType' => 'regular',
			'industryType' => 'IT',
			'subIndustryType' => 'software',
			'slots' => 2,
			'validityDate' => '2016-09-11',
			'status' => 'PENDING'
		);

		echo json_encode($jobOpeningModel->save($jobOpening));
	}


}