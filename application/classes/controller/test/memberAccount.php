<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Test_MemberAccount extends Controller {
	
	public function action_index() {
		echo 'Input Test/Sample for account(login, logout, etc) models';
	}



	public function action_read() {
		$accountModel = new Model_Member_Account();
		$options = (object)array(
			// 'userName' => 'ALEXPOGI',
			'memberId' => 1,
			'populate' => array('member')
		);
		echo json_encode($accountModel->read($options));
	}



	public function action_checkUserName() {
		$accountModel = new Model_Member_Account();
		echo $accountModel->checkUserName('ALEXPOGI');
	}



	public function action_resetPassword() {
		$accountModel = new Model_Member_Account();
		$accountInfo = new stdClass();
		$accountInfo->userName = 'LINELEADERACCOUNT';
		echo json_encode($accountModel->resetPassword($accountInfo));
	}



	public function action_updateAccount() {
		$accountInfo = new stdClass();
		$accountInfo->userName = 'alexpogi';
		$accountInfo->newUserName = 'alexpogi';
		$accountInfo->password = 'silveriopogi';
		$accountInfo->memberId = '1';
		$accountInfo->type = 'admin';
		$accountInfo->status = 'inactive';

		$accountModel = new Model_Member_Account();
		$accountModel->save($accountInfo);
	}



	public function action_createAccount() {
		$accountInfo = new stdClass();
		$accountInfo->userName = 'PerfectoConnorsSumague';
		$accountInfo->memberId = '5';
		$accountModel = new Model_Member_Account();
		$accountModel->create($accountInfo);
	}




	public function action_checkPassword() {
		$accountModel = new Model_Member_Account();
		$userName = 'LINELEADERACCOUNT';
		$password = 'LINELEADERACCOUNT';
		echo json_encode($accountModel->checkPassword($userName, $password));
	}




	public function action_logout() {
		$utility = new Etc_Utility();
		$utility->logout();
	}









}