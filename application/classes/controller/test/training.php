<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Test_Training extends Controller {
	
	public function action_index() {
		echo 'Input Test/Sample for training models';
	}



	public function action_saveTraining() {
		$trainingModel = new Model_Training();

		$info = new stdClass();
		$info->id = 1;
		$info->name = 'edited s//*&(((ample trai(*)(ning name)**)(*)(';
		$info->description = 'This is just a sample descripti*&^_))on f*(&*(()or this sample training. Dont take it seriously!';

		$trainingModel->save($info);
	}



	public function action_enlistTrainings() {
		$trainingModel = new Model_Training();

		$options = new stdClass();
		$options->populate = array(
			'trainingEvents' => array(
				'attendees' => array(
					'trainingEvent' => array(
						'training' => array(
							'trainingEvent'
						)
					), 
					'member' => array(
						'account', 
						'beneficiaries', 
						'characterReferences', 
						'education', 
						'emergencyContact', 
						'parent', 
						'skills', 
						'spouse', 
						'workHistories'
					)
				)
			)
		);
		$options->search = 'master';
		$options->currentPage = 1;
		$options->limit = 10;

		echo json_encode($trainingModel->enlist($options));
	}




	public function action_saveTrainingEvent() {
		$trainingEventModel = new Model_Schedule_TrainingEvent();

		$info = new stdClass();
		$info->id = 2;
		$info->trainingId = 2;
		$info->start = '2016-05-09';
		$info->end = '2016-05-09';
		$info->location = 'santa rosa city sports complex';
		$info->slots = '150';
		$info->requirements = array('ballpen', 'notebook', 'agency id', 'pocket money at least 150pesos', 'extra clothes for 4 days');
		$info->additionalNotes = '4 na araw po etong event pero libre na po ako pagkaen naten dito. fak';
		$info->attendees = array(
			(object)array('memberId' => 1),
			(object)array('memberId' => 2),
			(object)array('memberId' => 5)
		);

		$trainingEventModel->save($info);
	}



	public function action_enlistTrainingEvents() {
		$trainingEventModel = new Model_Schedule_TrainingEvent();

		$options = new stdClass();
		$options->populate = array(
			// 'attendees' => array(
			// 	'member'
			// ), 
			// 'training'
		);
		// $options->search = 'alex**&^**&^ pogi shortlist araw complex';
		// $options->currentPage = 1;
		// $options->limit = 10;
		// $options->monthYear = '2016-06-13';
		$options->currentData = true; // fetch data within prev, current, and next month
		// $options->upcoming = true;
		// $options->orderBy = array('start', 'asc');
		// $options->trainingId = 2;

		echo json_encode($trainingEventModel->enlist($options));
	}




	public function action_enlistTrainingEventAttendees() {
		$attendeeModel = new Model_TrainingEventAttendee();
		$options = new stdClass();
		$options->populate = array('member', 'trainingEvent');
		$options->trainingEventId = 2;
		echo json_encode($attendeeModel->enlist($options));
	}





}