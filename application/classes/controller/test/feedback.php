<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Test_Feedback extends Controller {
	
	public function action_index() {
		echo 'Input Test/Sample for feedback model';
	}



	public function action_enlistFeedbacks() {
		$feedbackModel = new Model_Feedback();

		$options = new stdClass();
		$options->search = 'alex**&^**&^ edited sample pogi shortlist araw complex';
		$options->currentPage = 1;
		$options->limit = 10;

		echo json_encode($feedbackModel->enlist($options));
	}


}