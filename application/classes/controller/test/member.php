<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Test_Member extends Controller {
	
	public function action_index() {
		echo 'Input Test/Sample for member models';
	}



	public function action_saveMember() {
		$memberModel = new Model_Member();

		$info = new stdClass();
		$info->id = 2;
		$info->firstName = 'perfecto';
		$info->middleName = 'connors';
		$info->lastName = 'sumague';
		$info->birthDate = '1980-01-30';
		$info->gender = 'male';
		$info->civilStatus = 'married';
		$info->street = 'general malvar st';
		$info->barangay = 'poblacion';
		$info->city = 'binan';
		$info->province = 'laguna';
		$info->religion = 'roman catholic';
		$info->weight = '77';
		$info->height = '165';
		$info->mobile = '09827721221';
		$info->email = 'perfecto.sumague@sample.com';
		$info->photo = '1759033_9877015_4580688.jpeg';
		$info->tags = array();

		// account
		$info->account = (object)array(
			'userName' => 'PerfectoConnors',
			'type' => 'admin',
			'status' => 'active'
		);

		// beneficiaries
		$info->beneficiaries = array(
			(object)array('name' => 'nicolas sumague', 'birthDate' => '2008-10-03', 'relation' => 'child'),
			(object)array('name' => 'cynthia rose sumague', 'birthDate' => '2010-03-23', 'relation' => 'child')
		);

		// character references
		$info->characterReferences = array(
			(object)array('name' => 'michael manabat', 'occupation' => 'general manager', 'company' => 'precision software builders plus inc', 'mobile' => '09282712121', 'email' => 'michael.manabat@sample.com'),
			(object)array('name' => 'orlan vincent hilomen', 'occupation' => 'chief technical officer', 'company' => 'STRATA', 'mobile' => '09727762512', 'email' => 'orvin.hilomen@sample.com'),
			(object)array('name' => 'enteng aranilla', 'occupation' => 'councilor', 'company' => 'municipality of nagcarlan', 'mobile' => '09282712131', 'email' => 'enteng.aranilla@sample.com')
		);

		// education
		$info->education = (object)array('attainment' => 'college graduate', 'startYear' => '2000', 'endYear' => '2008', 'course' => 'BSIT');

		// emergency contact
		$info->emergencyContact = (object)array('name' => 'alli sumague', 'relation' => 'spouse', 'mobile' => '09992272716', 'email' => 'alli.sumage@sample.com');

		// parents
		$info->parent = (object)array('motherName' => 'rosalinda sumague', 'motherOccupation' => 'farmer', 'fatherName' => 'fernando illusionado sumague', 'fatherOccupation' => 'farmer');

		// skills
		$info->skills = array(
			(object)array('skill' => 'macho'),
			(object)array('skill' => 'guapo'),
			(object)array('skill' => 'pogi'),
			(object)array('skill' => 'mala coco martin')
		);

		// spouse
		$info->spouse = (object)array('firstName' => 'Alli', 'middleName' => 'connors', 'lastName' => 'sumague', 'birthDate' => '1989-12-05', 'occupation' => 'Finance officer', 'mobile' => '09998281722', 'email' => 'alli.sumague@sample.com');

		// trainingEvents
		$info->trainingEvents = array(
			(object)array('trainingEventId' => '2', 'remark' => 'PENDING'),
			(object)array('trainingEventId' => '11', 'remark' => 'PENDING')
		);

		// work histories
		$info->workHistories = array(
			(object)array('company' => 'precision software builders plus inc', 'position' => 'web developer', 'startDate' => '2008-06-10', 'endDate' => '2010-10-10'),
			(object)array('company' => 'strata', 'position' => 'solutions engineer', 'startDate' => '2010-10-24', 'endDate' => '2014-01-25')
		);


		$memberModel->save($info);
	}



	public function action_enlistMembers() {
		$memberModel = new Model_Member();

		$options = new stdClass();
		$options->populate = array('account');
		// $options->populate = array('account', 'beneficiaries', 'characterReferences', 'education', 'emergencyContact', 'parent', 'skills', 'spouse', 'trainingEvents' => array('trainingEvent' => array('training', 'attendees')), 'workHistories');
		$options->search = 'al****)()(//ex coro*((**(za';
		// $options->accountType = 'admin';
		$options->currentPage = 1;
		$options->limit = 10;

		echo json_encode($memberModel->enlist($options));
	}


}