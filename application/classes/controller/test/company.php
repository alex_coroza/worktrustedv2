<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Test_Company extends Controller {
	
	public function action_index() {
		echo 'Input Test/Sample for company model';
	}





	public function action_enlist() {
		$companyModel = new Model_Company();
		$options = (object)array(
			'populate' => array(
				'account'
			)
		);
		$results = $companyModel->enlist($options);

		echo json_encode($results);
	}




	public function action_save() {
		$companyModel = new Model_Company();
		$info = new stdClass();
		$info->id = 2;
		$info->name = 'coroza group of companies';
		$info->description = 'sample description';
		$info->partnershipDate = '2016-01-31';
		$info->website = 'samplewebsite.com';
		$info->photo = 'samplePhoto.jpg';
		$companyModel->save($info);
	}





}