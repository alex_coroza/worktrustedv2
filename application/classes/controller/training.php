<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Training extends Controller {

	public function action_enlist() {
		$post = json_decode(file_get_contents("php://input"));
		$options = $post;
		$trainingModel = new Model_Training();
		echo json_encode($trainingModel->enlist($options));
	}


	public function action_read() {
		$post = json_decode(file_get_contents("php://input"));
		$readParams = $post;
		$trainingModel = new Model_Training();
		echo json_encode($trainingModel->read($readParams));
	}


	public function action_create() {
		$post = json_decode(file_get_contents("php://input"));
		$info = $post->info;
		$trainingModel = new Model_Training();
		echo json_encode($trainingModel->save($info));
	}


	public function action_update() {
		$post = json_decode(file_get_contents("php://input"));
		$info = $post->info;
		$trainingModel = new Model_Training();
		echo json_encode($trainingModel->save($info));
	}


	

	public function action_permanentDelete() {
		$post = json_decode(file_get_contents("php://input"));
		$id = $post->id;
		$trainingModel = new Model_Training();
		echo json_encode($trainingModel->permanentDelete($id));
	}



	



}
