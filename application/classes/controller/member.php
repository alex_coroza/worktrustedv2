<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Member extends Controller {

	public function action_enlist() {
		$post = json_decode(file_get_contents("php://input"));
		$options = $post;
		$memberModel = new Model_Member();
		echo json_encode($memberModel->enlist($options));
	}


	public function action_read() {
		$post = json_decode(file_get_contents("php://input"));
		$readParams = $post;
		$memberModel = new Model_Member();
		echo json_encode($memberModel->read($readParams));
	}


	public function action_create() {
		$post = json_decode(file_get_contents("php://input"));
		$info = $post->info;
		$memberModel = new Model_Member();
		echo json_encode($memberModel->save($info));
	}


	public function action_update() {
		$post = json_decode(file_get_contents("php://input"));
		$info = $post->info;
		$memberModel = new Model_Member();
		echo json_encode($memberModel->save($info));
	}


	public function action_uploadProfilePicture() {
		$memberModel = new Model_Member();
		$pictureFile = $_FILES['file'];
		$memberModel->uploadProfilePicture($pictureFile);
	}


	

	public function action_permanentDelete() {
		$post = json_decode(file_get_contents("php://input"));
		$id = $post->id;
		$memberModel = new Model_Member();
		echo json_encode($memberModel->permanentDelete($id));
	}



	public function action_memberPictureUpdate() {
		$post = json_decode(file_get_contents("php://input"));
		$info = $post->info;
		$newPictureName = $post->newPictureName;
		$memberModel = new Model_Member();
		echo json_encode($memberModel->memberPictureUpdate($info, $newPictureName));
	}



	public function action_uploadBase64ProfilePicture() {
		$post = json_decode(file_get_contents("php://input"));
		$base64File = $post->base64File;
		$memberModel = new Model_Member();
		echo json_encode($memberModel->uploadBase64ProfilePicture($base64File));
	}
	

	



}
