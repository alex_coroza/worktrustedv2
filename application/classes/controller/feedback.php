<?php defined('SYSPATH') or die('No direct script access.');

class Controller_FeedBack extends Controller {

	
	public function action_enlist() {
		$post = json_decode(file_get_contents("php://input"));
		$currentPage = $post->currentPage;
		$limit = $post->limit;
		$feedbackModel = new Model_Feedback();
		echo json_encode($feedbackModel->enlist($currentPage, $limit));
	}


	public function action_read() {
		$post = json_decode(file_get_contents("php://input"));
		$id = $post->id;
		$feedbackModel = new Model_Feedback();
		echo json_encode($feedbackModel->read($id));
	}


	public function action_create() {
		$post = json_decode(file_get_contents("php://input"));
		$info = $post->info;
		$feedbackModel = new Model_Feedback();
		echo json_encode($feedbackModel->save($info));
	}


	public function action_update() {
		$post = json_decode(file_get_contents("php://input"));
		$info = $post->info;
		$feedbackModel = new Model_Feedback();
		echo json_encode($feedbackModel->save($info));
	}


	public function action_delete() {
		$post = json_decode(file_get_contents("php://input"));
		$id = $post->id;
		$feedbackModel = new Model_Feedback();
		echo json_encode($feedbackModel->delete($id));
	}


	public function action_permanentDelete() {
		$post = json_decode(file_get_contents("php://input"));
		$id = $post->id;
		$feedbackModel = new Model_Feedback();
		echo json_encode($feedbackModel->permanentDelete($id));
	}



}
