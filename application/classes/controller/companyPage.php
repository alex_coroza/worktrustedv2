<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Companypage extends Controller {
	
	public function action_index() {
		$utility = new Etc_Utility();
		$utility->redirectUnauthorizedUser();
		$this->response->body(View::factory('company'));
	}


}