<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Applicant extends Controller {

	public function action_enlist() {
		$post = json_decode(file_get_contents("php://input"));
		$options = $post;
		$applicantModel = new Model_Applicant();
		echo json_encode($applicantModel->enlist($options));
	}


	public function action_read() {
		$post = json_decode(file_get_contents("php://input"));
		$readParams = $post;
		$applicantModel = new Model_Applicant();
		echo json_encode($applicantModel->read($readParams));
	}


	public function action_create() {
		$post = json_decode(file_get_contents("php://input"));
		$info = $post->info;
		$applicantModel = new Model_Applicant();
		echo json_encode($applicantModel->save($info));
	}


	public function action_update() {
		$post = json_decode(file_get_contents("php://input"));
		$info = $post->info;
		$applicantModel = new Model_Applicant();
		echo json_encode($applicantModel->save($info));
	}


	public function action_uploadProfilePicture() {
		$applicantModel = new Model_Applicant();
		$pictureFile = $_FILES['file'];
		$applicantModel->uploadProfilePicture($pictureFile);
	}


	public function action_delete() {
		$post = json_decode(file_get_contents("php://input"));
		$id = $post->id;
		$applicantModel = new Model_Applicant();
		echo json_encode($applicantModel->delete($id));
	}


	public function action_permanentDelete() {
		$post = json_decode(file_get_contents("php://input"));
		$id = $post->id;
		$deletePhoto = (isset($post->deletePhoto)) ? true : false;
		$applicantModel = new Model_Applicant();
		echo json_encode($applicantModel->permanentDelete($id, $deletePhoto));
	}

	

	
	public function action_applicantPictureUpdate() {
		$post = json_decode(file_get_contents("php://input"));
		$info = $post->info;
		$newPictureName = $post->newPictureName;
		$applicantModel = new Model_Applicant();
		echo json_encode($applicantModel->applicantPictureUpdate($info, $newPictureName));
	}



	public function action_uploadBase64ProfilePicture() {
		$post = json_decode(file_get_contents("php://input"));
		$base64File = $post->base64File;
		$applicantModel = new Model_Applicant();
		echo json_encode($applicantModel->uploadBase64ProfilePicture($base64File));
	}



	public function action_addInterviewSchedule() {
		$post = json_decode(file_get_contents("php://input"));
		$applicantId = $post->applicantId;
		$schedule = $post->schedule;
		$applicantInterviewModel = ORM::factory('orm_applicantInterview');
		$applicantInterviewModel->applicantId = $applicantId;
		$applicantInterviewModel->schedule = $schedule;
		$applicantInterviewModel->save();
	}




}
