<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Job extends Controller {

	public function action_enlistJobOpenings() {
		$post = json_decode(file_get_contents("php://input"));
		$options = $post;
		$jobOpeningModel = new Model_Job_Opening();
		echo json_encode($jobOpeningModel->enlist($options));
	}


	public function action_saveJobOpening() {
		$post = json_decode(file_get_contents("php://input"));
		$info = $post->info;
		$jobOpeningModel = new Model_Job_Opening();
		echo json_encode($jobOpeningModel->save($info));
	}


	public function action_readJobOpening() {
		$post = json_decode(file_get_contents("php://input"));
		$readOptions = $post;
		$jobOpeningModel = new Model_Job_Opening();
		echo json_encode($jobOpeningModel->read($readOptions));
	}













	public function action_enlistJobApplicants() {
		$post = json_decode(file_get_contents("php://input"));
		$options = $post;
		$jobApplicantModel = new Model_Job_Applicant();
		echo json_encode($jobApplicantModel->enlist($options));
	}


	public function action_saveJobApplicant() {
		$post = json_decode(file_get_contents("php://input"));
		$info = $post->info;
		$jobApplicantModel = new Model_Job_Applicant();
		echo json_encode($jobApplicantModel->save($info));
	}


	public function action_readJobApplicant() {
		$post = json_decode(file_get_contents("php://input"));
		$readOptions = $post;
		$jobApplicantModel = new Model_Job_Applicant();
		echo json_encode($jobApplicantModel->read($readOptions));
	}


	public function action_deleteJobApplicant() {
		$post = json_decode(file_get_contents("php://input"));
		$id = $post->id;
		$jobApplicantModel = new Model_Job_Applicant();
		echo json_encode($jobApplicantModel->delete($id));
	}















}