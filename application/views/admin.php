<!DOCTYPE html>
<html lang="en" ng-app="adminModule">
<head>
	<meta charset="UTF-8">
	<title>Worktrusted Manpower Services Inc.</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta name="theme-color" content="#FE5621">
	<link rel="shortcut icon" href="/worktrustedV2/app/assets/images/worktrusted_06.png">
	<link rel="stylesheet" type="text/css" href="/worktrustedV2/app/assets/bower_components/mdi/css/materialdesignicons.min.css">
	<link rel="stylesheet" type="text/css" href="/worktrustedV2/app/assets/bower_components/angular-motion/dist/angular-motion.min.css">
	<link rel="stylesheet" type="text/css" href="/worktrustedV2/app/assets/bower_components/angular-material/angular-material.min.css">
	<link rel="stylesheet" type="text/css" href="/worktrustedV2/app/assets/bower_components/fullcalendar/dist/fullcalendar.min.css">
	<link rel="stylesheet" type="text/css" href="/worktrustedV2/app/assets/bower_components/angular-busy/dist/angular-busy.css">
	<link rel="stylesheet" type="text/css" href="/worktrustedV2/app/assets/css/customBootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/worktrustedV2/app/assets/css/admin.css">
</head>
<body>
	
	<!-- main output here -->
	<main ui-view class="am-fade mainOutput"></main>

	




	<!-- JS INCLUDES -->
	<!-- core js files -->
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/jquery/dist/jquery.min.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/angular/angular.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/angular-aria/angular-aria.min.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/angular-messages/angular-messages.min.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/angular-animate/angular-animate.min.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/angular-material/angular-material.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/angular-ui-router/release/angular-ui-router.js"></script>
	
	<!-- 3rd party utility scripts -->
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/moment/moment.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/angular-moment/angular-moment.js"></script>
	
	<!-- files for angular file upload -->
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/ng-file-upload/ng-file-upload.min.js"></script>

	<!-- files for angular ng-camera -->
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/webcamjs/webcam.min.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/ng-camera/dist/ng-camera.js"></script>	

	<!-- files for angular ui calendar -->
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/angular-ui-calendar/src/calendar.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>

	<!-- other 3rd party scripts -->
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/angular-busy/dist/angular-busy.js"></script>


	<!-- core directives and services -->
	<script type="text/javascript" src="/worktrustedV2/app/modules/core/services/utility.service.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/core/directives/utility.directive.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/core/filters/utility.filter.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/core/services/http.service.js"></script>

	<!-- files for profile module -->
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/profile/profile.module.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/profile/profile.routes.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/profile/services/profile.service.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/profile/controllers/profileInfo.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/profile/controllers/accountEdit.controller.js"></script>

	<!-- files for membersip-applicant module -->
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/membership-applicant/membershipApplicant.module.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/membership-applicant/membershipApplicant.routes.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/membership-applicant/controllers/membershipApplicantList.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/membership-applicant/controllers/membershipApplicantInfo.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/membership-applicant/controllers/membershipApplicantEdit.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/membership-applicant/controllers/membershipApplicantChangePicture.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/membership-applicant/controllers/membershipApplicantInfo/interviewScheduleList.controller.js"></script>

	<!-- files for manage-member module -->
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/manage-member/manageMember.module.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/manage-member/manageMember.routes.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/manage-member/controllers/memberList.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/manage-member/controllers/memberInfo.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/manage-member/controllers/memberEdit.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/manage-member/controllers/addMember.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/manage-member/controllers/memberChangePicture.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/manage-member/controllers/memberInfo/trainingScheduleList.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/manage-member/controllers/memberEdit/trainingScheduleList2.controller.js"></script>

	<!-- files for jobs module -->
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/jobs/jobs.module.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/jobs/jobs.routes.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/jobs/controllers/jobOpeningList.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/jobs/controllers/jobOpeningList/advancedSearch.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/jobs/controllers/addJobOpening.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/jobs/controllers/jobOpeningInfo.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/jobs/controllers/jobOpeningEdit.controller.js"></script>

	<!-- files for scheduling module -->
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/scheduling/scheduling.module.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/scheduling/scheduling.routes.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/scheduling/services/scheduling.service.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/scheduling/controllers/schedulingMain.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/scheduling/controllers/schedulingMain/addSchedule.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/scheduling/controllers/schedulingMain/membershipApplicantInterview/scheduleInfo.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/scheduling/controllers/schedulingMain/membershipApplicantInterview/scheduleEdit.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/scheduling/controllers/schedulingMain/training/scheduleInfo.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/scheduling/controllers/schedulingMain/training/scheduleEdit.controller.js"></script>

	<!-- files for training module -->
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/training/training.module.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/training/training.routes.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/training/services/training.service.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/training/controllers/trainingList.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/training/controllers/trainingList/addTraining.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/training/controllers/trainingInfo.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/training/controllers/trainingEdit.controller.js"></script>

	<!-- files for company module -->
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/company/company.module.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/company/company.routes.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/company/controllers/companyList.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/company/controllers/companyInfo.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/company/controllers/companyEdit.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/company/controllers/companyChangePicture.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/company/controllers/addCompany.controller.js"></script>
	
	<!-- files for admin's base module -->
	<!-- this includes the navbar, sidenav and mainContent's container -->
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/admin/admin.module.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/admin/admin.routes.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/admin/controllers/admin.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/admin/admin/controllers/admin/sideNav.controller.js"></script>



	










	<!-- concatMinified vendor files -->
	<!-- // <script type="text/javascript" src="/worktrustedV2/app/adminVendors.js"></script> -->
	<!-- concatMinified admin modules together with core module -->
	<!-- // <script type="text/javascript" src="/worktrustedV2/app/adminModules.js"></script> -->
</body>
</html>