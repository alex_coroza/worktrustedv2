<!DOCTYPE html>
<html lang="en" ng-app="testinganModule">
<head>
	<meta charset="UTF-8">
	<title>Worktrusted Manpower Services Inc.</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta name="theme-color" content="#FE5621">
	<link rel="shortcut icon" href="/worktrustedV2/app/assets/images/worktrusted_06.png">
	<link rel="stylesheet" type="text/css" href="/worktrustedV2/app/assets/bower_components/angular-material/angular-material.min.css">
</head>
<body>
	
	<!-- main output here -->
	<div ng-controller="testinganController">
		<md-datepicker ng-model="datePogi" md-placeholder="asdasdas"></md-datepicker>
	</div>

	




	<!-- JS INCLUDES -->
	<!-- core js files -->
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/angular/angular.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/angular-aria/angular-aria.min.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/angular-messages/angular-messages.min.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/angular-animate/angular-animate.min.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/angular-material/angular-material.min.js"></script>


	<script type="text/javascript" src="/worktrustedV2/app/modules/testingan/testingan.module.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/testingan/testingan.controller.js"></script>
	




</body>
</html>