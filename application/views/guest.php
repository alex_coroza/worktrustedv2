<!DOCTYPE html>
<html lang="en" ng-app="guestModule">
<head>
	<meta charset="UTF-8">
	<title>Worktrusted Manpower Services Inc.</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta name="theme-color" content="#FE5621">
	<link rel="shortcut icon" href="app/assets/images/worktrusted_06.png">
	<!-- <link rel="stylesheet" type="text/css" href="/worktrustedV2/app/assets/bower_components/bootstrap/dist/css/bootstrap.min.css"> -->
	<link rel="stylesheet" type="text/css" href="/worktrustedV2/app/assets/bower_components/bootswatch/paper/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/worktrustedV2/app/assets/bower_components/angular-aside/dist/css/angular-aside.min.css">
	<link rel="stylesheet" type="text/css" href="/worktrustedV2/app/assets/bower_components/angular-motion/dist/angular-motion.min.css">
	<link rel="stylesheet" type="text/css" href="/worktrustedV2/app/assets/bower_components/angular-busy/dist/angular-busy.css">
	<link rel="stylesheet" type="text/css" href="/worktrustedV2/app/assets/css/guest.css">
</head>
<body>
	
	<!-- main output here -->
	<main ui-view class="am-fade-and-slide-right"></view>

		

	<!-- JS INCLUDES -->
	<!-- core js files -->
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/angular/angular.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/angular-messages/angular-messages.min.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/angular-animate/angular-animate.min.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/angular-ui-router/release/angular-ui-router.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
	
	<!-- files for ui.bootstrap extensions -->
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/angular-aside/dist/js/angular-aside.min.js"></script>

	<!-- files from angular strap for scrollspy only -->
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/angular-strap/dist/modules/debounce.min.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/angular-strap/dist/modules/dimensions.min.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/angular-strap/dist/modules/scrollspy.min.js"></script>
	
	<!-- files for angular file upload -->
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/ng-file-upload/ng-file-upload.min.js"></script>

	<!-- other 3rd party scripts -->
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/angular-busy/dist/angular-busy.js"></script>

	<!-- core services -->
	<script type="text/javascript" src="/worktrustedV2/app/modules/core/services/utility.service.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/core/services/http.service.js"></script>

	<!-- files for guestHome page -->
	<script type="text/javascript" src="/worktrustedV2/app/modules/guest/guestHome/guestHome.module.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/guest/guestHome/guestHome.routes.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/guest/guestHome/controllers/guestHome.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/guest/guestHome/controllers/guestHome/navbar.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/guest/guestHome/controllers/guestHome/navbar/navbarAside.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/guest/guestHome/controllers/guestHome/contactUs.controller.js"></script>

	<!-- files for apply-membership -->
	<script type="text/javascript" src="/worktrustedV2/app/modules/guest/apply-membership/apply-membership.module.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/guest/apply-membership/apply-membership.routes.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/guest/apply-membership/controllers/apply-membership-form.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/guest/apply-membership/controllers/apply-membership-form/applyMembershipNavbar.controller.js"></script>

	<!-- files for login module -->
	<script type="text/javascript" src="/worktrustedV2/app/modules/guest/login/login.module.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/guest/login/login.routes.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/guest/login/controllers/login.controller.js"></script>

	<!-- guestBaseModule files -->
	<script type="text/javascript" src="/worktrustedV2/app/modules/guest/guest/guest.module.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/guest/guest/guest.routes.js"></script>



	<!-- concatMinified files for guest module's vendors -->
	<!-- // <script type="text/javascript" src="/worktrustedV2/app/guestVendors.js"></script> -->
	<!-- concatMinified files for guest module(with core module) -->
	<!-- // <script type="text/javascript" src="/worktrustedV2/app/guestModules.js"></script> -->
</body>
</html>