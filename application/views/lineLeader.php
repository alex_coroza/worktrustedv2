<!DOCTYPE html>
<html lang="en" ng-app="lineLeaderModule">
<head>
	<meta charset="UTF-8">
	<title>Worktrusted Manpower Services Inc.</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta name="theme-color" content="#FE5621">
	<link rel="shortcut icon" href="/worktrustedV2/app/assets/images/worktrusted_06.png">
	<link rel="stylesheet" type="text/css" href="/worktrustedV2/app/assets/bower_components/mdi/css/materialdesignicons.min.css">
	<link rel="stylesheet" type="text/css" href="/worktrustedV2/app/assets/bower_components/angular-motion/dist/angular-motion.min.css">
	<link rel="stylesheet" type="text/css" href="/worktrustedV2/app/assets/bower_components/angular-material/angular-material.min.css">
	<link rel="stylesheet" type="text/css" href="/worktrustedV2/app/assets/bower_components/fullcalendar/dist/fullcalendar.min.css">
	<link rel="stylesheet" type="text/css" href="/worktrustedV2/app/assets/bower_components/angular-busy/dist/angular-busy.css">
	<link rel="stylesheet" type="text/css" href="/worktrustedV2/app/assets/css/customBootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/worktrustedV2/app/assets/css/lineLeader.css">
</head>
<body>
	
	<!-- main output here -->
	<main ui-view class="am-fade mainOutput"></main>

	




	<!-- JS INCLUDES -->
	<!-- core js files -->
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/jquery/dist/jquery.min.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/angular/angular.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/angular-aria/angular-aria.min.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/angular-messages/angular-messages.min.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/angular-animate/angular-animate.min.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/angular-material/angular-material.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/angular-ui-router/release/angular-ui-router.js"></script>
	
	<!-- 3rd party utility scripts -->
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/moment/moment.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/angular-moment/angular-moment.js"></script>
	
	<!-- files for angular file upload -->
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/ng-file-upload/ng-file-upload.min.js"></script>

	<!-- files for angular ng-camera -->
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/webcamjs/webcam.min.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/ng-camera/dist/ng-camera.js"></script>	

	<!-- files for angular ui calendar -->
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/angular-ui-calendar/src/calendar.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>

	<!-- other 3rd party scripts -->
	<script type="text/javascript" src="/worktrustedV2/app/assets/bower_components/angular-busy/dist/angular-busy.js"></script>


	<!-- core directives and services -->
	<script type="text/javascript" src="/worktrustedV2/app/modules/core/services/utility.service.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/core/directives/utility.directive.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/core/filters/utility.filter.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/core/services/http.service.js"></script>


	<!-- files for profile module -->
	<script type="text/javascript" src="/worktrustedV2/app/modules/lineLeader/profile/profile.module.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/lineLeader/profile/profile.routes.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/lineLeader/profile/controllers/profileInfo.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/lineLeader/profile/controllers/accountEdit.controller.js"></script>


	<!-- files for lineLeader's base module -->
	<!-- this includes the navbar, sidenav and mainContent's container -->
	<script type="text/javascript" src="/worktrustedV2/app/modules/lineLeader/lineLeader/lineLeader.module.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/lineLeader/lineLeader/lineLeader.routes.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/lineLeader/lineLeader/controllers/lineLeader.controller.js"></script>
	<script type="text/javascript" src="/worktrustedV2/app/modules/lineLeader/lineLeader/controllers/lineLeader/sideNav.controller.js"></script>



	










	<!-- concatMinified vendor files -->
	<!-- // <script type="text/javascript" src="/worktrustedV2/app/lineLeaderVendors.js"></script> -->
	<!-- concatMinified admin modules together with core module -->
	<!-- // <script type="text/javascript" src="/worktrustedV2/app/lineLeaderModules.js"></script> -->
</body>
</html>