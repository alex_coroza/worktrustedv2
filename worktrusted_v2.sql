/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 5.5.46 : Database - worktrusted_v2
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`worktrusted_v2` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `worktrusted_v2`;

/*Table structure for table `applicant_beneficiaries` */

DROP TABLE IF EXISTS `applicant_beneficiaries`;

CREATE TABLE `applicant_beneficiaries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicantId` int(11) DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthDate` date DEFAULT NULL,
  `relation` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_applicant_beneficiaries` (`applicantId`),
  CONSTRAINT `FK_applicant_beneficiaries` FOREIGN KEY (`applicantId`) REFERENCES `applicants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `applicant_beneficiaries` */

insert  into `applicant_beneficiaries`(`id`,`applicantId`,`name`,`birthDate`,`relation`) values (62,35,'Nicolas Calvez','2008-10-03','Child'),(68,41,'Nicolas Calvez','2008-10-03','Child'),(71,44,'Nicolas Calvez','2008-10-03','Child'),(72,45,'Nicolas Calvez','2008-10-03','Child'),(73,46,'Nicolas Calvez','2008-10-03','Child'),(75,48,'Nicolas Calvez','2008-10-03','Child'),(92,36,'Nicolas Calvez','1970-01-01','Child'),(102,31,'Nicolas Calvez','2009-10-03','Child');

/*Table structure for table `applicant_character_references` */

DROP TABLE IF EXISTS `applicant_character_references`;

CREATE TABLE `applicant_character_references` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicantId` int(11) DEFAULT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `occupation` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_applicant_character_references` (`applicantId`),
  CONSTRAINT `FK_applicant_character_references` FOREIGN KEY (`applicantId`) REFERENCES `applicants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=169 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `applicant_character_references` */

insert  into `applicant_character_references`(`id`,`applicantId`,`name`,`occupation`,`company`,`mobile`,`email`) values (58,35,'Marly Felipe','Faculty','Sti Southwoods','09882712712','marly@yahoo.com.ph'),(59,35,'Rexon Arevalo','Councilor','Municipality Of Nagcarlan','09271261217','rexonPuge@gmail.com'),(60,35,'Enteng Aranilla','Councilor','Municipality Of Nagcarlan','09998281727','entengKabs@gmail.com'),(76,41,'Marly Felipe','Faculty','Sti Southwoods','09882712712','marly@yahoo.com.ph'),(77,41,'Rexon Arevalo','Councilor','Municipality Of Nagcarlan','09271261217','rexonPuge@gmail.com'),(78,41,'Enteng Aranilla','Councilor','Municipality Of Nagcarlan','09998281727','entengKabs@gmail.com'),(85,44,'Marly Felipe','Faculty','Sti Southwoods','09882712712','marly@yahoo.com.ph'),(86,44,'Rexon Arevalo','Councilor','Municipality Of Nagcarlan','09271261217','rexonPuge@gmail.com'),(87,44,'Enteng Aranilla','Councilor','Municipality Of Nagcarlan','09998281727','entengKabs@gmail.com'),(88,45,'Marly Felipe','Faculty','Sti Southwoods','09882712712','marly@yahoo.com.ph'),(89,45,'Rexon Arevalo','Councilor','Municipality Of Nagcarlan','09271261217','rexonPuge@gmail.com'),(90,45,'Enteng Aranilla','Councilor','Municipality Of Nagcarlan','09998281727','entengKabs@gmail.com'),(91,46,'Marly Felipe','Faculty','Sti Southwoods','09882712712','marly@yahoo.com.ph'),(92,46,'Rexon Arevalo','Councilor','Municipality Of Nagcarlan','09271261217','rexonPuge@gmail.com'),(93,46,'Enteng Aranilla','Councilor','Municipality Of Nagcarlan','09998281727','entengKabs@gmail.com'),(97,48,'Marly Felipe','Faculty','Sti Southwoods','09882712712','marly@yahoo.com.ph'),(98,48,'Rexon Arevalo','Councilor','Municipality Of Nagcarlan','09271261217','rexonPuge@gmail.com'),(99,48,'Enteng Aranilla','Councilor','Municipality Of Nagcarlan','09998281727','entengKabs@gmail.com'),(136,36,'Marly Felipe','Faculty','Sti Southwoods','09882712712','marly@yahoo.com.ph'),(137,36,'Rexon Arevalo','Councilor','Municipality Of Nagcarlan','09271261217','rexonPuge@gmail.com'),(138,36,'Enteng Aranilla','Councilor','Municipality Of Nagcarlan','09998281727','entengKabs@gmail.com'),(166,31,'Marly Felipe','Faculty','Sti Southwoods','09882712712','marly@yahoo.com.ph'),(167,31,'Rexon Arevalo','Councilor','Municipality Of Nagcarlan','09271261217','rexonPuge@gmail.com'),(168,31,'Enteng Aranilla','Councilor','Municipality Of Nagcarlan','09998281727','entengKabs@gmail.com');

/*Table structure for table `applicant_education` */

DROP TABLE IF EXISTS `applicant_education`;

CREATE TABLE `applicant_education` (
  `applicantId` int(11) NOT NULL,
  `attainment` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `startYear` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `endYear` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `course` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`applicantId`),
  CONSTRAINT `FK_applicant_education` FOREIGN KEY (`applicantId`) REFERENCES `applicants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `applicant_education` */

insert  into `applicant_education`(`applicantId`,`attainment`,`startYear`,`endYear`,`course`) values (31,'College Graduate','2001','2005','BS Business Education'),(35,'College Graduate','2001','2005','BS Business Education'),(36,'College Graduate','2001','2005','BS Business Education'),(41,'College Graduate','2001','2005','BS Business Education'),(44,'College Graduate','2001','2005','BS Business Education'),(45,'College Graduate','2001','2005','BS Business Education'),(46,'College Graduate','2001','2005','BS Business Education'),(48,'College Graduate','2001','2005','BS Business Education');

/*Table structure for table `applicant_emergency_contact` */

DROP TABLE IF EXISTS `applicant_emergency_contact`;

CREATE TABLE `applicant_emergency_contact` (
  `applicantId` int(11) DEFAULT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `relation` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `relation1` (`applicantId`),
  CONSTRAINT `relation1` FOREIGN KEY (`applicantId`) REFERENCES `applicants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `applicant_emergency_contact` */

insert  into `applicant_emergency_contact`(`applicantId`,`name`,`relation`,`mobile`,`email`) values (31,'Rodel Calvez','Spouse','09282712712','rodel.calvez@gmail.com'),(35,'Rodel Calvez','Spouse','09282712712','rodel.calvez@gmail.com'),(36,'Rodel Calvez','Spouse','09282712712','rodel.calvez@gmail.com'),(41,'Rodel Calvez','Spouse','09282712712','rodel.calvez@gmail.com'),(44,'Rodel Calvez','Spouse','09282712712','rodel.calvez@gmail.com'),(45,'Rodel Calvez','Spouse','09282712712','rodel.calvez@gmail.com'),(46,'Rodel Calvez','Spouse','09282712712','rodel.calvez@gmail.com'),(48,'Rodel Calvez','Spouse','09282712712','rodel.calvez@gmail.com');

/*Table structure for table `applicant_interview` */

DROP TABLE IF EXISTS `applicant_interview`;

CREATE TABLE `applicant_interview` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicantId` int(11) DEFAULT NULL,
  `scheduleApplicantInterviewId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_interview_applicant` (`applicantId`),
  KEY `FK_applicant_interview1` (`scheduleApplicantInterviewId`),
  CONSTRAINT `FK_applicant_interview` FOREIGN KEY (`applicantId`) REFERENCES `applicants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_applicant_interview2` FOREIGN KEY (`scheduleApplicantInterviewId`) REFERENCES `schedule_applicant_interviews` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `applicant_interview` */

insert  into `applicant_interview`(`id`,`applicantId`,`scheduleApplicantInterviewId`) values (42,31,38);

/*Table structure for table `applicant_parent` */

DROP TABLE IF EXISTS `applicant_parent`;

CREATE TABLE `applicant_parent` (
  `applicantId` int(11) NOT NULL,
  `motherName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `motherOccupation` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fatherName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fatherOccupation` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`applicantId`),
  CONSTRAINT `FK_applicant_parent` FOREIGN KEY (`applicantId`) REFERENCES `applicants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `applicant_parent` */

insert  into `applicant_parent`(`applicantId`,`motherName`,`motherOccupation`,`fatherName`,`fatherOccupation`) values (31,'Rafaela Coroza','Custom Dress Taylor','Silverio Coroza','Farmer'),(35,'Rafaela Coroza','Custom Dress Taylor','Silverio Coroza','Farmer'),(36,'Rafaela Coroza','Custom Dress Taylor','Silverio Coroza','Farmer'),(41,'Rafaela Coroza','Custom Dress Taylor','Silverio Coroza','Farmer'),(44,'Rafaela Coroza','Custom Dress Taylor','Silverio Coroza','Farmer'),(45,'Rafaela Coroza','Custom Dress Taylor','Silverio Coroza','Farmer'),(46,'Rafaela Coroza','Custom Dress Taylor','Silverio Coroza','Farmer'),(48,'Rafaela Coroza','Custom Dress Taylor','Silverio Coroza','Farmer');

/*Table structure for table `applicant_skills` */

DROP TABLE IF EXISTS `applicant_skills`;

CREATE TABLE `applicant_skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicantId` int(11) DEFAULT NULL,
  `skill` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_applicant_skills` (`applicantId`),
  CONSTRAINT `FK_applicant_skills` FOREIGN KEY (`applicantId`) REFERENCES `applicants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=171 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `applicant_skills` */

insert  into `applicant_skills`(`id`,`applicantId`,`skill`) values (77,35,'Good Cook'),(78,35,'Can Use Computer Keyboard Efficiently Wven Without Looking'),(89,41,'Good Cook'),(90,41,'Can Use Computer Keyboard Efficiently Wven Without Looking'),(95,44,'Good Cook'),(96,44,'Can Use Computer Keyboard Efficiently Wven Without Looking'),(97,45,'Good Cook'),(98,45,'Can Use Computer Keyboard Efficiently Wven Without Looking'),(99,46,'Good Cook'),(100,46,'Can Use Computer Keyboard Efficiently Wven Without Looking'),(103,48,'Good Cook'),(104,48,'Can Use Computer Keyboard Efficiently Wven Without Looking'),(149,36,'Good Cook'),(150,36,'Can Use Computer Keyboard Efficiently Wven Without Looking'),(169,31,'Good Cook'),(170,31,'Can Use Computer Keyboard Efficiently Wven Without Looking');

/*Table structure for table `applicant_spouse` */

DROP TABLE IF EXISTS `applicant_spouse`;

CREATE TABLE `applicant_spouse` (
  `applicantId` int(11) NOT NULL,
  `firstName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middleName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthDate` date DEFAULT NULL,
  `occupation` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`applicantId`),
  CONSTRAINT `FK_applicant_spouse` FOREIGN KEY (`applicantId`) REFERENCES `applicants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `applicant_spouse` */

insert  into `applicant_spouse`(`applicantId`,`firstName`,`middleName`,`lastName`,`birthDate`,`occupation`,`mobile`,`email`) values (31,'Rodel','Coroza','Calvez','1975-01-11','Teacher','09282712712','rodel.calvez@gmail.com'),(35,'Rodel','Coroza','Calvez','1975-01-11','Teacher','09282712712','rodel.calvez@gmail.com'),(36,'Rodel','Coroza','Calvez','1975-01-11','Teacher','09282712712','rodel.calvez@gmail.com'),(41,'Rodel','Coroza','Calvez','1975-01-11','Teacher','09282712712','rodel.calvez@gmail.com'),(44,'Rodel','Coroza','Calvez','1975-01-11','Teacher','09282712712','rodel.calvez@gmail.com'),(45,'Rodel','Coroza','Calvez','1975-01-11','Teacher','09282712712','rodel.calvez@gmail.com'),(46,'Rodel','Coroza','Calvez','1975-01-11','Teacher','09282712712','rodel.calvez@gmail.com'),(48,'Rodel','Coroza','Calvez','1975-01-11','Teacher','09282712712','rodel.calvez@gmail.com');

/*Table structure for table `applicant_work_histories` */

DROP TABLE IF EXISTS `applicant_work_histories`;

CREATE TABLE `applicant_work_histories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicantId` int(11) DEFAULT NULL,
  `company` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_applicant_work_histories` (`applicantId`),
  CONSTRAINT `FK_applicant_work_histories` FOREIGN KEY (`applicantId`) REFERENCES `applicants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `applicant_work_histories` */

insert  into `applicant_work_histories`(`id`,`applicantId`,`company`,`position`,`startDate`,`endDate`) values (39,35,'Manila Christian Academy','Guidance Counselor','2008-05-13','2010-05-26'),(40,35,'STI Southwoods','Guidance Counselor','2010-08-18','2013-09-18'),(51,41,'Manila Christian Academy','Guidance Counselor','2008-05-13','2010-05-26'),(52,41,'STI Southwoods','Guidance Counselor','2010-08-18','2013-09-18'),(57,44,'Manila Christian Academy','Guidance Counselor','2008-05-13','2010-05-26'),(58,44,'STI Southwoods','Guidance Counselor','2010-08-18','2013-09-18'),(59,45,'Manila Christian Academy','Guidance Counselor','2008-05-13','2010-05-26'),(60,45,'STI Southwoods','Guidance Counselor','2010-08-18','2013-09-18'),(61,46,'Manila Christian Academy','Guidance Counselor','2008-05-13','2010-05-26'),(62,46,'STI Southwoods','Guidance Counselor','2010-08-18','2013-09-18'),(65,48,'Manila Christian Academy','Guidance Counselor','2008-05-13','2010-05-26'),(66,48,'STI Southwoods','Guidance Counselor','2010-08-18','2013-09-18'),(91,36,'Manila Christian Academy','Guidance Counselor','1970-01-01','1970-01-01'),(92,36,'STI Southwoods','Guidance Counselor','1970-01-01','1970-01-01'),(111,31,'Manila Christian Academy','Guidance Counselor','2016-01-31','2016-01-21'),(112,31,'STI Southwoods','Guidance Counselor','2014-03-01','2015-12-12');

/*Table structure for table `applicants` */

DROP TABLE IF EXISTS `applicants`;

CREATE TABLE `applicants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `applicationDate` datetime DEFAULT NULL,
  `firstName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middleName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthDate` date DEFAULT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `civilStatus` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `barangay` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `province` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `religion` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `mobile` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` text COLLATE utf8_unicode_ci,
  `tags` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `applicants` */

insert  into `applicants`(`id`,`applicationDate`,`firstName`,`middleName`,`lastName`,`birthDate`,`gender`,`civilStatus`,`street`,`barangay`,`city`,`province`,`religion`,`weight`,`height`,`mobile`,`email`,`photo`,`tags`) values (31,'2016-02-08 16:35:58','Perfecto','Sumague','Connors','1970-01-01','Female','Married','Kalachuchi','Tagapo','Santa Rosa','Laguna','Christian',67,158,'09275297293','pcoroza@yahoo.com.ph','606079_2017517_9458008.jpg',''),(35,'2016-02-01 16:37:35','Precy','Coroza','Calvez','1981-04-02','Female','Married','Kalachuchi','Tagapo','Santa Rosa','Laguna','Christian',67,158,'09275297293','pcoroza@yahoo.com.ph','9295350_6491699_7612610.jpg',NULL),(36,'2015-10-05 10:25:44','Precy','Coroza','Calvez','1981-04-02','Female','Married','Kalachuchi','Tagapo','Santa Rosa','Laguna','Christian',67,158,'09275297293','pcoroza@yahoo.com.ph','7723389_6241760_9588013.jpeg',''),(41,'2016-02-14 16:37:47','Precy','Coroza','Calvez','1981-04-02','Female','Married','Kalachuchi','Tagapo','Santa Rosa','Laguna','Christian',67,158,'09275297293','pcoroza@yahoo.com.ph','2094421_3465576_2140197.jpg',NULL),(44,'2016-02-14 16:37:52','Precy','Coroza','Calvez','1981-04-02','Female','Married','Kalachuchi','Tagapo','Santa Rosa','Laguna','Christian',67,158,'09275297293','pcoroza@yahoo.com.ph','693664_470581_1623230.jpg',NULL),(45,'2016-02-26 16:37:54','Precy','Coroza','Calvez','1981-04-02','Female','Married','Kalachuchi','Tagapo','Santa Rosa','Laguna','Christian',67,158,'09275297293','pcoroza@yahoo.com.ph','3355713_6415100_9732056.jpg',NULL),(46,'2016-03-25 16:37:55','Precy','Coroza','Calvez','1981-04-02','Female','Married','Kalachuchi','Tagapo','Santa Rosa','Laguna','Christian',67,158,'09275297293','pcoroza@yahoo.com.ph','359802_7517090_4380188.jpeg',NULL),(48,'2016-04-13 16:37:59','Precy','Coroza','Calvez','1981-04-02','Female','Married','Kalachuchi','Tagapo','Santa Rosa','Laguna','Christian',67,158,'09275297293','pcoroza@yahoo.com.ph','3111877_930786_1160583.jpg',NULL);

/*Table structure for table `companies` */

DROP TABLE IF EXISTS `companies`;

CREATE TABLE `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `partnershipDate` date DEFAULT NULL,
  `website` text COLLATE utf8_unicode_ci,
  `photo` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `companies` */

insert  into `companies`(`id`,`name`,`description`,`partnershipDate`,`website`,`photo`) values (1,'Globe Telecom','Globe Telecom, Inc. is the number one mobile brand in the Philippines and the purveyor of the Filipino digital lifestyle. We provide cellular, broadband and mobile data services by focusing on enriching our content offerings amid customers\' growing preference for multimedia platforms across multiple screens and devices.\r\n','2016-06-12','globe.com.ph','4905090_3265991_5072937.png'),(3,'Coroza And Company Incorporated','Founded by Alex Coroza on 2025','2016-06-22','coroza.com.ph','3123779_9884339_1629028.png'),(6,'Waterich','This is just a sample description just to fill the empty space for this field.','2016-07-24','facebook.com/waterichPH','9768982_5617676_9844056.jpg');

/*Table structure for table `company_accounts` */

DROP TABLE IF EXISTS `company_accounts`;

CREATE TABLE `company_accounts` (
  `userName` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `companyId` int(11) DEFAULT NULL,
  `password` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `salt` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `FK_company_accounts` (`companyId`),
  CONSTRAINT `FK_company_accounts` FOREIGN KEY (`companyId`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `company_accounts` */

insert  into `company_accounts`(`userName`,`companyId`,`password`,`salt`,`status`) values ('COMPANYACCOUNT',1,'8a8316ce6237291dfa8690e12ea6db545512d6b7','WTmDK6vycu','active'),('COROZAANDCOMPANY',3,'034fd9708dd522fb3abd7005b7848d74a63b3af2','ScXqpixK4N','active'),('WATERICH',6,'947e46db24217035a42f02e09059c8ec4c105335','JlJpR8DjMy','active');

/*Table structure for table `feedbacks` */

DROP TABLE IF EXISTS `feedbacks`;

CREATE TABLE `feedbacks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subject` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `feedbacks` */

insert  into `feedbacks`(`id`,`name`,`email`,`subject`,`message`,`datetime`) values (1,'Alex Coroza','coroza.alex.urriza@gmail.com','Sample Feedback','qweqweasdasd zxczxc 123123 qweqwe asdasd','2015-12-03 11:12:45'),(2,'Kaye Fabre','k.fabre@gmail.com','Membership Application','Mag apply daw po ako sabe ni papa Alex Sya na lang daw po magencode ng data ko','2015-12-05 03:12:24'),(3,'Alex','coroza.alex.urriza@gmail.com','Sample Subject','kkasd aslkdjasdlk kklasjdlkasdi aka. bhosxz mala# iasdu boi-echos kksaj., ,','2016-06-05 04:06:32'),(4,'Alex Pogi','coroza.alex.urriza@gmail.com','','asdkljjalksdasdkllkajsldaksjdalklsjd','2016-06-05 04:06:00'),(5,'Alex Coroza','coroza.alex.urriza@gmail.com','','this is just a sample message! tangina ka!','2016-06-13 10:06:20');

/*Table structure for table `job_applicants` */

DROP TABLE IF EXISTS `job_applicants`;

CREATE TABLE `job_applicants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jobOpeningId` int(11) DEFAULT NULL,
  `memberId` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `status` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jobOpeningId` (`jobOpeningId`),
  KEY `memberId` (`memberId`),
  CONSTRAINT `job_applicants_ibfk_1` FOREIGN KEY (`jobOpeningId`) REFERENCES `job_openings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `job_applicants_ibfk_2` FOREIGN KEY (`memberId`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `job_applicants` */

insert  into `job_applicants`(`id`,`jobOpeningId`,`memberId`,`date`,`status`) values (2,1,4,'2016-07-19 00:00:00','Hired'),(7,2,11,'2016-08-17 22:39:33','Pending'),(8,1,11,'2016-08-17 23:19:17','Pending');

/*Table structure for table `job_openings` */

DROP TABLE IF EXISTS `job_openings`;

CREATE TABLE `job_openings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyId` int(11) DEFAULT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `date` datetime DEFAULT NULL,
  `minSalary` int(11) DEFAULT NULL,
  `maxSalary` int(11) DEFAULT NULL,
  `employmentType` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `industryType` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slots` int(11) DEFAULT NULL,
  `validityDate` date DEFAULT NULL,
  `status` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `companyId` (`companyId`),
  CONSTRAINT `job_openings_ibfk_1` FOREIGN KEY (`companyId`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `job_openings` */

insert  into `job_openings`(`id`,`companyId`,`title`,`description`,`date`,`minSalary`,`maxSalary`,`employmentType`,`industryType`,`slots`,`validityDate`,`status`) values (1,1,'Mabuhok Na Title','description','2016-08-17 00:00:00',15000,15000,'Contractual','Computer Software',4,'2016-09-23','posted'),(2,6,'Title Na Malake','sample description','2016-08-17 00:00:00',10000,10000,'Regular','IT',5,'2016-09-11','Posted'),(3,6,'Forklift Driver','magddrive ka lang ng forklift at mga kahon ang ikakarga mo. maghapon kang nakaupo lang pero malaki ang sahod. taena! san ka pa?','2016-08-17 00:00:00',0,0,'Contractual','Manufacturing',5,'2016-09-05','Posted');

/*Table structure for table `member_accounts` */

DROP TABLE IF EXISTS `member_accounts`;

CREATE TABLE `member_accounts` (
  `userName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `memberId` int(11) DEFAULT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `salt` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`userName`),
  KEY `accounts-members-fk` (`memberId`),
  CONSTRAINT `accounts-members-fk` FOREIGN KEY (`memberId`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `member_accounts` */

insert  into `member_accounts`(`userName`,`memberId`,`password`,`salt`,`type`,`status`) values ('ALEXPOGI',1,'7b7d093b40e60ef9837172ca01b2438e1c06fd81','u7TJdFFWuL','admin','active'),('AlexUrrizaCoroza',8,'83244df8a0e7d3b5e0e82219e19bc814bebb0147','XO7sJPG8Mg','member','active'),('DgRqGVX0vw6',9,'8230a40e5a4c9775957bc2e2da0b44357d3c0ccc','Q2vUMxUsn2','admin','active'),('LINELEADERACCOUNT',5,'925e8d101e5dd5917340bc79ecf55514c31e8c1c','32zkQgdcJN','lineLeader','active'),('LKJLKJ',18,'0ae6d4da4e9ac452b19944c395de4bc0d6cdbae8','IcuSn9T77y','lineLeader','active'),('MEMBERACCOUNT',11,'13e5338fb3baa8a5b2c1f866337145f6670e0bf8','og8LLeg9gj','member','active'),('NUH4INYDCK10',16,'9b0edaf6b1dd4d712c24e88e31488aecf3d008a5','YeIh7xzt1f','member','active'),('PEBCXEISPL11',17,'3bee2ef68a9b6d97289973ab6a690185a474e70b','M0eHseR3ol','member','active'),('PerfectoConnors',2,'1231qweqwe','1231','admin','active'),('PrecyCorozaCalvez',4,'88a1fc12f6389a49f3caafe6d67d2c45c61c662b','usgbuNeWZp','member','active'),('PrecyCorozaCalvez Account II',7,'92b29aaff7c5688418ebe4f02ad033a1bac712e0','xNNdJNFgWY','member','active'),('RYAYF7RJLQ13',19,'d464fe62cb0f8f608fd9ac0ece9bfc5949d552c6','mYIR0bsnEk','member','active'),('venuschan',13,'56b87d436bf3b9f92c2e7f64914859d3bfce80e9','ee8otEFRWQ','admin','active'),('YV3LDFFEQW7',10,'6423cf681964e2cec79cd918b8f6da3c236e8f4f','SNXiXQPBTc','member','active');

/*Table structure for table `member_beneficiaries` */

DROP TABLE IF EXISTS `member_beneficiaries`;

CREATE TABLE `member_beneficiaries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` int(11) DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthDate` date DEFAULT NULL,
  `relation` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_member_beneficiaries` (`memberId`),
  CONSTRAINT `FK_member_beneficiaries` FOREIGN KEY (`memberId`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `member_beneficiaries` */

insert  into `member_beneficiaries`(`id`,`memberId`,`name`,`birthDate`,`relation`) values (8,6,'Nicolas Calvez','2008-10-03','Child'),(17,9,'Nicolas Calvez','1970-01-01','Child'),(18,10,'Nicolas Calvez','1970-01-01','Child'),(19,5,'Margarita Olidan','1970-01-01','Mother'),(23,1,'Alfonse Coroza','1970-01-01','Child'),(31,2,'Nicolas Sumague','1970-01-01','Child'),(32,2,'Cynthia Rose Sumague','1970-01-01','Child'),(42,7,'Nicolas Calvez','2009-10-03','Child'),(44,17,'Nicolas Calvez','2008-10-03','Child'),(47,19,'Nicolas Calvez','2008-10-03','Child'),(49,13,'Crisostomo Antonidas Coroza','2016-07-01','Child'),(53,11,'Nicolas Calvez','1970-01-01','Child'),(54,16,'Nicolas Calvez','2008-10-03','Child'),(63,8,'Rafaela Coroza','1956-10-24','Mother'),(64,8,'Silverio Coroza','1950-06-22','Father'),(65,4,'Nicolas Calvez','1970-01-01','Child');

/*Table structure for table `member_character_references` */

DROP TABLE IF EXISTS `member_character_references`;

CREATE TABLE `member_character_references` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` int(11) DEFAULT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `occupation` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_member_character_references` (`memberId`),
  CONSTRAINT `FK_member_character_references` FOREIGN KEY (`memberId`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=168 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `member_character_references` */

insert  into `member_character_references`(`id`,`memberId`,`name`,`occupation`,`company`,`mobile`,`email`) values (14,6,'Marly Felipe','Faculty','Sti Southwoods','09882712712','marly@yahoo.com.ph'),(15,6,'Rexon Arevalo','Councilor','Municipality Of Nagcarlan','09271261217','rexonPuge@gmail.com'),(16,6,'Enteng Aranilla','Councilor','Municipality Of Nagcarlan','09998281727','entengKabs@gmail.com'),(32,9,'Marly Felipe','Faculty','Sti Southwoods','09882712712','marly@yahoo.com.ph'),(33,9,'Rexon Arevalo','Councilor','Municipality Of Nagcarlan','09271261217','rexonPuge@gmail.com'),(34,9,'Enteng Aranilla','Councilor','Municipality Of Nagcarlan','09998281727','entengKabs@gmail.com'),(35,10,'Marly Felipe','Faculty','Sti Southwoods','09882712712','marly@yahoo.com.ph'),(36,10,'Rexon Arevalo','Councilor','Municipality Of Nagcarlan','09271261217','rexonPuge@gmail.com'),(37,10,'Enteng Aranilla','Councilor','Municipality Of Nagcarlan','09998281727','entengKabs@gmail.com'),(38,5,'Marly Felipe','Faculty','Sti Southwoods','09882712712','marly@yahoo.com.ph'),(39,5,'Rexon Arevalo','Councilor','Municipality Of Nagcarlan','09271261217','rexonPuge@gmail.com'),(40,5,'Enteng Aranilla','Councilor','Municipality Of Nagcarlan','09998281727','entengKabs@gmail.com'),(45,1,'Abe Villajos','Chief Technical Officer In SMD','Globe Telecom','09837212121','apvillajos@globe.com.ph'),(61,2,'Michael Manabat','General Manager','Precision Software Builders Plus Inc','09282712121','michael.manabat@sample.com'),(89,7,'Marly Felipe','Faculty','Sti Southwoods','09882712712','marly@yahoo.com.ph'),(90,7,'Rexon Arevalo','Councilor','Municipality Of Nagcarlan','09271261217','rexonPuge@gmail.com'),(91,7,'Enteng Aranilla','Councilor','Municipality Of Nagcarlan','09998281727','entengKabs@gmail.com'),(105,17,'Marly Felipe','Faculty','Sti Southwoods','09882712712','marly@yahoo.com.ph'),(106,17,'Rexon Arevalo','Councilor','Municipality Of Nagcarlan','09271261217','rexonPuge@gmail.com'),(107,17,'Enteng Aranilla','Councilor','Municipality Of Nagcarlan','09998281727','entengKabs@gmail.com'),(111,18,'Alex','Chief Technical Officer','Strata Ph','09353293790','coroza.alex.urriza@gmail.com'),(112,18,'Alex','Chief Technical Officer','Strata Ph','09353293790','coroza.alex.urriza@gmail.com'),(113,18,'Alex','Chief Technical Officer','Strata Ph','99353293790','coroza.alex.urriza@gmail.com'),(120,19,'Marly Felipe','Faculty','Sti Southwoods','09882712712','marly@yahoo.com.ph'),(121,19,'Rexon Arevalo','Councilor','Municipality Of Nagcarlan','09271261217','rexonPuge@gmail.com'),(122,19,'Enteng Aranilla','Councilor','Municipality Of Nagcarlan','09998281727','entengKabs@gmail.com'),(126,13,'Michael Manabat','Owner And Manager','Precision Software Builders Plus Inc','09271621212','m.manabat@gmail.com'),(127,13,'Orvin Hilomen','Chief Technical Officer','Strata Ph','09228121277','orvino@gmail.com'),(128,13,'Abe Villajos','System Automation Head','Globe Telecom','09276651215','abe.villajos@globe.com.ph'),(135,11,'Marly Felipe','Faculty','Sti Southwoods','09882712712','marly@yahoo.com.ph'),(136,11,'Rexon Arevalo','Councilor','Municipality Of Nagcarlan','09271261217','rexonPuge@gmail.com'),(137,11,'Enteng Aranilla','Councilor','Municipality Of Nagcarlan','09998281727','entengKabs@gmail.com'),(138,16,'Marly Felipe','Faculty','Sti Southwoods','09882712712','marly@yahoo.com.ph'),(139,16,'Rexon Arevalo','Councilor','Municipality Of Nagcarlan','09271261217','rexonPuge@gmail.com'),(140,16,'Enteng Aranilla','Councilor','Municipality Of Nagcarlan','09998281727','entengKabs@gmail.com'),(162,8,'Precy Calvez','Guidance Coordinator','Malayan Colleges Laguna','09275297293','pcalvez@gmail.com'),(163,8,'Michael Manabat','Manager','PSBPINC','09221515151','michael.manabat@psbpinc.com'),(164,8,'Orlan Vincent Hilomen','Technical Chief Officer','STRATA','09175564848','orvin.hilomen@strata.ph'),(165,4,'Marly Felipe','Faculty','Sti Southwoods','09882712712','marly@yahoo.com.ph'),(166,4,'Rexon Arevalo','Councilor','Municipality Of Nagcarlan','09271261217','rexonPuge@gmail.com'),(167,4,'Enteng Aranilla','Councilor','Municipality Of Nagcarlan','09998281727','entengKabs@gmail.com');

/*Table structure for table `member_education` */

DROP TABLE IF EXISTS `member_education`;

CREATE TABLE `member_education` (
  `memberId` int(11) NOT NULL,
  `attainment` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `startYear` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `endYear` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `course` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`memberId`),
  CONSTRAINT `FK_member_education` FOREIGN KEY (`memberId`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `member_education` */

insert  into `member_education`(`memberId`,`attainment`,`startYear`,`endYear`,`course`) values (1,'College Graduate','2010','2014','BSIT'),(2,'College Graduate','2000','2008','BSIT'),(4,'College Graduate','2001','2005','BS Business Education'),(5,'College Graduate','2010','2015','BSIT'),(6,'College Graduate','2001','2005','BS Business Education'),(7,'College Graduate','2001','2005','BS Business Education'),(8,'College Graduate','2010','2014','BSIT'),(9,'College Graduate','2001','2005','BS Business Education'),(10,'College Graduate','2001','2005','BS Business Education'),(11,'College Graduate','2001','2005','BS Business Education'),(13,'College Undergraduate','2014','2015','BS Business Marketing'),(16,'College Graduate','2001','2005','BS Business Education'),(17,'College Graduate','2001','2005','BS Business Education'),(18,'College Graduate','2020','2021','Lkjlk'),(19,'College Graduate','2001','2005','BS Business Education');

/*Table structure for table `member_emergency_contact` */

DROP TABLE IF EXISTS `member_emergency_contact`;

CREATE TABLE `member_emergency_contact` (
  `memberId` int(11) DEFAULT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `relation` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  KEY `relation1` (`memberId`),
  CONSTRAINT `FK_member_emergency_contact` FOREIGN KEY (`memberId`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `member_emergency_contact` */

insert  into `member_emergency_contact`(`memberId`,`name`,`relation`,`mobile`,`email`) values (1,'Precy Calvez','Sister','09275297293','pcalvez@gmail.com'),(2,'Alli Sumague','Spouse','09992272716','alli.sumage@sample.com'),(4,'Rodel Calvez','Spouse','09282712712','rodel.calvez@gmail.com'),(5,'Gelice Caravana','Other','09282712712','gelisi@gmail.com'),(6,'Rodel Calvez','Spouse','09282712712','rodel.calvez@gmail.com'),(7,'Rodel Calvez','Spouse','09282712712','rodel.calvez@gmail.com'),(8,'Precy Calvez','Sister','09275297293','pcalvez@gmail.com'),(9,'Rodel Calvez','Spouse','09282712712','rodel.calvez@gmail.com'),(10,'Rodel Calvez','Spouse','09282712712','rodel.calvez@gmail.com'),(11,'Rodel Calvez','Spouse','09282712712','rodel.calvez@gmail.com'),(13,'Maria Priscilla Langa','Sister','09227162121','mp_langa@gmail.com'),(16,'Rodel Calvez','Spouse','09282712712','rodel.calvez@gmail.com'),(17,'Rodel Calvez','Spouse','09282712712','rodel.calvez@gmail.com'),(18,'Precy Calvez','Sister','09275297293','pcalvez@gmail.com'),(19,'Rodel Calvez','Spouse','09282712712','rodel.calvez@gmail.com');

/*Table structure for table `member_parent` */

DROP TABLE IF EXISTS `member_parent`;

CREATE TABLE `member_parent` (
  `memberId` int(11) NOT NULL,
  `motherName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `motherOccupation` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fatherName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fatherOccupation` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`memberId`),
  CONSTRAINT `FK_member_parent` FOREIGN KEY (`memberId`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `member_parent` */

insert  into `member_parent`(`memberId`,`motherName`,`motherOccupation`,`fatherName`,`fatherOccupation`) values (1,'Rafaela Coroza','Custom Taylor','Silverio Coroza','Farmer'),(2,'Rosalinda Sumague','Farmer','Fernando Illusionado Sumague','Farmer'),(4,'Rafaela Coroza','Custom Dress Taylor','Silverio Coroza','Farmer'),(5,'Rafaela Coroza','Custom Dress Taylor','Silverio Coroza','Farmer'),(6,'Rafaela Coroza','Custom Dress Taylor','Silverio Coroza','Farmer'),(7,'Rafaela Coroza','Custom Dress Taylor','Silverio Coroza','Farmer'),(8,'Rafaela Coroza','Custom Tailor','Silverio Coroza','Farmer'),(9,'Rafaela Coroza','Custom Dress Taylor','Silverio Coroza','Farmer'),(10,'Rafaela Coroza','Custom Dress Taylor','Silverio Coroza','Farmer'),(11,'Rafaela Coroza','Custom Dress Taylor','Silverio Coroza','Farmer'),(13,'Erlinda Langa','Housewife','Rorsalindo Langa','Jeepney Driver'),(16,'Rafaela Coroza','Custom Dress Taylor','Silverio Coroza','Farmer'),(17,'Rafaela Coroza','Custom Dress Taylor','Silverio Coroza','Farmer'),(18,'Rafaela Coroza','Custom Tailor','Silverio Coroza','Farmer'),(19,'Rafaela Coroza','Custom Dress Taylor','Silverio Coroza','Farmer');

/*Table structure for table `member_skills` */

DROP TABLE IF EXISTS `member_skills`;

CREATE TABLE `member_skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` int(11) DEFAULT NULL,
  `skill` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_member_skills` (`memberId`),
  CONSTRAINT `FK_member_skills` FOREIGN KEY (`memberId`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `member_skills` */

insert  into `member_skills`(`id`,`memberId`,`skill`) values (16,6,'Good Cook'),(17,6,'Can Use Computer Keyboard Efficiently Wven Without Looking'),(26,9,'Good Cook'),(27,9,'Can Use Computer Keyboard Efficiently Wven Without Looking'),(28,10,'Good Cook'),(29,10,'Can Use Computer Keyboard Efficiently Wven Without Looking'),(30,5,'Good Cook'),(31,5,'Gentleman'),(32,5,'Marunong Mag Rasengan'),(33,5,'Hindi Nahuhulog Sa Kahit Anong Genjustsu'),(34,5,'Kayang Dumurog Ng Isang Bundok Gamet Ang Taijutsu'),(43,1,'Pogi'),(44,1,'Guapo'),(45,1,'Macho'),(46,1,'Mala Coco Martin'),(57,2,'Macho'),(58,2,'Guapo'),(59,2,'Pogi'),(60,2,'Mala Coco Martin'),(79,7,'Good Cook'),(80,7,'Can Use Computer Keyboard Efficiently Wven Without Looking'),(90,17,'Good Cook'),(91,17,'Can Use Computer Keyboard Efficiently Wven Without Looking'),(96,18,'Pogi'),(97,18,'Guapo'),(98,18,'Macho'),(99,18,'Mala Coco Martin'),(104,19,'Good Cook'),(105,19,'Can Use Computer Keyboard Efficiently Wven Without Looking'),(108,13,'Can Sing'),(109,13,'Can Dance'),(110,13,'Always Calm Kahit Na Malapet Na Ang Dealine'),(111,13,'Cool Lang'),(118,11,'Good Cook'),(119,11,'Can Use Computer Keyboard Efficiently Wven Without Looking'),(120,16,'Good Cook'),(121,16,'Can Use Computer Keyboard Efficiently Wven Without Looking'),(138,8,'Pogi'),(139,8,'Guapo'),(140,8,'Macho'),(141,8,'Mala Coco Martin'),(142,4,'Good Cook'),(143,4,'Can Use Computer Keyboard Efficiently Wven Without Looking');

/*Table structure for table `member_spouse` */

DROP TABLE IF EXISTS `member_spouse`;

CREATE TABLE `member_spouse` (
  `memberId` int(11) NOT NULL,
  `firstName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middleName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthDate` date DEFAULT NULL,
  `occupation` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`memberId`),
  CONSTRAINT `FK_member_spouse` FOREIGN KEY (`memberId`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `member_spouse` */

insert  into `member_spouse`(`memberId`,`firstName`,`middleName`,`lastName`,`birthDate`,`occupation`,`mobile`,`email`) values (1,'Venus','Lloveras','Langa','1996-09-02','Professional Actress','09271262126','missv@gmail.com'),(2,'Alli','Connors','Sumague','1989-12-05','Finance Officer','09998281722','alli.sumague@sample.com'),(4,'Rodel','Coroza','Calvez','1975-01-11','Teacher','09282712712','rodel.calvez@gmail.com'),(6,'Rodel','Coroza','Calvez','1975-01-11','Teacher','09282712712','rodel.calvez@gmail.com'),(7,'Rodel','Coroza','Calvez','1975-01-11','Teacher','09282712712','rodel.calvez@gmail.com'),(9,'Rodel','Coroza','Calvez','1975-01-11','Teacher','09282712712','rodel.calvez@gmail.com'),(10,'Rodel','Coroza','Calvez','1975-01-11','Teacher','09282712712','rodel.calvez@gmail.com'),(11,'Rodel','Coroza','Calvez','1975-01-11','Teacher','09282712712','rodel.calvez@gmail.com'),(16,'Rodel','Coroza','Calvez','1975-01-11','Teacher','09282712712','rodel.calvez@gmail.com'),(17,'Rodel','Coroza','Calvez','1975-01-11','Teacher','09282712712','rodel.calvez@gmail.com'),(19,'Rodel','Coroza','Calvez','1975-01-11','Teacher','09282712712','rodel.calvez@gmail.com');

/*Table structure for table `member_training_events` */

DROP TABLE IF EXISTS `member_training_events`;

CREATE TABLE `member_training_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` int(11) DEFAULT NULL,
  `trainingEventId` int(11) DEFAULT NULL,
  `remark` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_member_trainings` (`memberId`),
  KEY `FK_member_trainings2` (`trainingEventId`),
  CONSTRAINT `FK_member_trainings` FOREIGN KEY (`memberId`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_member_training_events` FOREIGN KEY (`trainingEventId`) REFERENCES `training_events` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `member_training_events` */

insert  into `member_training_events`(`id`,`memberId`,`trainingEventId`,`remark`) values (47,5,2,'PENDING'),(54,1,2,'PASSED'),(62,2,2,'PENDING'),(83,1,11,'PENDING'),(84,2,11,'PENDING'),(91,7,11,'PENDING'),(92,7,15,'PENDING'),(93,7,16,'PENDING'),(99,19,16,'PENDING'),(101,13,15,'PENDING'),(102,13,11,'PENDING'),(105,11,17,'PENDING'),(106,11,18,'PENDING'),(107,16,16,'PENDING'),(108,16,19,'PENDING'),(113,8,18,'PENDING'),(114,4,14,'PENDING');

/*Table structure for table `member_violations` */

DROP TABLE IF EXISTS `member_violations`;

CREATE TABLE `member_violations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` int(11) DEFAULT NULL,
  `policyId` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `member_violations-memberfk1` (`memberId`),
  KEY `member_violations-policies-fk1` (`policyId`),
  CONSTRAINT `member_violations-memberfk1` FOREIGN KEY (`memberId`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `member_violations-policies-fk1` FOREIGN KEY (`policyId`) REFERENCES `policies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `member_violations` */

/*Table structure for table `member_work_histories` */

DROP TABLE IF EXISTS `member_work_histories`;

CREATE TABLE `member_work_histories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `memberId` int(11) DEFAULT NULL,
  `company` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_member_work_histories` (`memberId`),
  CONSTRAINT `FK_member_work_histories` FOREIGN KEY (`memberId`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `member_work_histories` */

insert  into `member_work_histories`(`id`,`memberId`,`company`,`position`,`startDate`,`endDate`) values (6,5,'Manila Christian Academy','Guidance Counselor','2008-05-13','2010-05-26'),(7,5,'STI Southwoods','Guidance Counselor','2010-08-18','2013-09-18'),(8,6,'Manila Christian Academy','Guidance Counselor','2008-05-13','2010-05-26'),(9,6,'STI Southwoods','Guidance Counselor','2010-08-18','2013-09-18'),(14,9,'Manila Christian Academy','Guidance Counselor','1970-01-01','1970-01-01'),(15,9,'STI Southwoods','Guidance Counselor','1970-01-01','1970-01-01'),(16,10,'Manila Christian Academy','Guidance Counselor','1970-01-01','1970-01-01'),(17,10,'STI Southwoods','Guidance Counselor','1970-01-01','1970-01-01'),(21,1,'PSBPINC','Web Developer','1970-01-01','1970-01-01'),(26,2,'Precision Software Builders Plus Inc','Web Developer','2015-01-01','2015-06-01'),(27,2,'Strata','Solutions Engineer','2014-01-01','2014-06-01'),(44,7,'Manila Christian Academy','Guidance Counselor','2011-01-13','2013-05-31'),(45,7,'STI Southwoods','Guidance Counselor','2014-06-01','2016-06-07'),(50,17,'Manila Christian Academy','Guidance Counselor','2008-05-13','2010-05-26'),(51,17,'STI Southwoods','Guidance Counselor','2010-08-18','2013-09-18'),(56,19,'Manila Christian Academy','Guidance Counselor','2008-05-13','2010-05-26'),(57,19,'STI Southwoods','Guidance Counselor','2010-08-18','2013-09-18'),(60,13,'Yachang Talent Management','Talent','2015-01-16','2016-06-16'),(65,11,'Manila Christian Academy','Guidance Counselor','1970-01-01','1970-01-01'),(66,11,'STI Southwoods','Guidance Counselor','1970-01-01','1970-01-01'),(67,16,'Manila Christian Academy','Guidance Counselor','2008-05-13','2010-05-26'),(68,16,'STI Southwoods','Guidance Counselor','2010-08-18','2013-09-18'),(83,8,'PSBPINC','Web Developer','2013-12-07','2014-03-27'),(84,8,'STRATA','Solutions Engineer','2014-06-23','2016-04-27'),(85,4,'Manila Christian Academy','Guidance Counselor','2010-01-12','2013-08-21'),(86,4,'Waterich','Mabuhok Na Title','2016-08-12',NULL);

/*Table structure for table `members` */

DROP TABLE IF EXISTS `members`;

CREATE TABLE `members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `membershipDate` date DEFAULT NULL,
  `firstName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middleName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthDate` date DEFAULT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `civilStatus` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `barangay` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `province` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `religion` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `mobile` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` text COLLATE utf8_unicode_ci,
  `tags` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `members` */

insert  into `members`(`id`,`membershipDate`,`firstName`,`middleName`,`lastName`,`birthDate`,`gender`,`civilStatus`,`street`,`barangay`,`city`,`province`,`religion`,`weight`,`height`,`mobile`,`email`,`photo`,`tags`) values (1,'2015-02-06','Alex','Urriza','Coroza','1992-12-12','Male','Married','Kalachuchi Sreet','Tagapo','Santa Rosa','Laguna','Christian',64,172,'09353293790','alex.coroza@worktrusted.com.ph','6057739_4998474_8132325.jpg',''),(2,'2016-06-02','Perfecto','Connors','Sumague','1980-01-30','Male','Married','General Malvar St','Poblacion','Binan','Laguna','Roman Catholic',77,165,'09827721221','perfecto.sumague@sample.com','1759033_9877015_4580688.jpeg',''),(4,'2016-05-14','Precyz','Coroza','Calvez','1981-04-02','Female','Married','Kalachuchi','Tagapo','Santa Rosa','Laguna','Christian',67,158,'09275297293','pcoroza@yahoo.com.ph','7034912_3697815_4797974.jpg',''),(5,'2016-05-14','Nimuel','Asino','Olidan','1993-03-03','Male','Single','Anonymous St','De La Paz','Binan','Laguna','Christian',60,164,'09376212621','rhastah_24@yahoo.com.ph','202941_8231812_3847351.jpg','tunay na hokage'),(6,'2016-05-14','Precy','Coroza','Calvez','1981-04-02','Female','Married','Kalachuchi','Tagapo','Santa Rosa','Laguna','Christian',67,158,'09275297293','pcoroza@yahoo.com.ph','7829895_4569702_9677430.jpg',''),(7,'2016-05-14','Precy','Coroza','Calvez Account II','1981-04-02','Female','Married','Kalachuchi','Tagapo','Santa Rosa','Laguna','Christian',67,158,'09275297293','pcoroza@yahoo.com.ph','4647522_2066040_3653259.jpg',''),(8,'2016-05-15','Alex','Urriza','Coroza','1992-12-12','Male','Single','Kalachuchi','Tagapo','Santa Rosa','Laguna','Christian',64,172,'09353293790','coroza.alex.urriza@gmail.com','9488526_4891663_3677368.jpg','anak ng legends|boi_echos'),(9,'2016-05-26','Precy','Coroza','Calvez','1981-04-02','Female','Married','Kalachuchi','Tagapo','Santa Rosa','Laguna','Christian',67,158,'09275297293','pcoroza@yahoo.com.ph','7513428_856018_42114.jpg',''),(10,'2016-05-26','Precy','Coroza','Calvez','1981-04-02','Female','Married','Kalachuchi','Tagapo','Santa Rosa','Laguna','Christian',67,158,'09275297293','pcoroza@yahoo.com.ph','6626587_5752869_4111328.jpg',''),(11,'2016-06-13','Precy','Coroza','Calvez','1981-04-02','Female','Married','Kalachuchi','Tagapo','Santa Rosa','Laguna','Christian',67,158,'09275297293','pcoroza@yahoo.com.ph','4130554_2904053_8434143.png',''),(13,'2016-06-16','Venus','Lloveras','Langa','1997-09-02','Male','Single','Lakeville 2 Subdivision','Sinalhan','Santa Rosa','Laguna','Christian',48,150,'09872271212','venus_chan@gmail.com','7078247_3733826_7785645.jpg','chix|crush ni bhosxz alex yan'),(16,'2016-06-29','Precy','Coroza','Calvez','1981-04-02','Female','Married','Kalachuchi','Tagapo','Santa Rosa','Laguna','Christian',67,158,'09275297293','pcoroza@yahoo.com.ph','4257202_5614929_4700928.jpg',''),(17,'2016-06-29','Precy','Coroza','Calvez','1981-04-02','Female','Married','Kalachuchi','Tagapo','Santa Rosa','Laguna','Christian',67,158,'09275297293','pcoroza@yahoo.com.ph','2787170_1787719_5816345.jpg',''),(18,'2016-07-07','Lkjlkjlkjlk','Lkjlkjlkj','Lkjlkkjlkj','2016-07-07','Male','Single','Lkjlkjlkj','Lkjlkjlkkj','Lkkjlkjlkpj','Lkjlkjlkkj','Ljlkjlkj',32,123,'09353293790','coroza.alex.urriza@gmail.com','3312683_2506103_8807679.jpeg','qwe|asd|zxc'),(19,'2016-07-07','Hokagurl','Coroza','Uzumaki','1981-04-02','Female','Married','Kalachuchi','Tagapo','Santa Rosa','Laguna','Christian',67,158,'09275297293','pcoroza@yahoo.com.ph','6239624_6698914_7855225.jpg','');

/*Table structure for table `policies` */

DROP TABLE IF EXISTS `policies`;

CREATE TABLE `policies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `actions` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `policies` */

/*Table structure for table `schedule_applicant_interviews` */

DROP TABLE IF EXISTS `schedule_applicant_interviews`;

CREATE TABLE `schedule_applicant_interviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `schedule_applicant_interviews` */

insert  into `schedule_applicant_interviews`(`id`,`start`,`end`) values (1,'2016-03-10 00:00:00','2016-03-10 00:00:00'),(2,'2016-04-05 00:00:00','2016-04-05 00:00:00'),(4,'2016-04-28 00:00:00','2016-04-28 00:00:00'),(7,'1970-01-01 00:00:00','1970-01-01 00:00:00'),(8,'1970-01-01 00:00:00','1970-01-01 00:00:00'),(11,'2016-05-12 00:00:00','2016-05-12 00:00:00'),(12,'2013-12-01 00:00:00','2013-12-01 00:00:00'),(14,'2016-07-08 00:00:00','2016-07-08 00:00:00'),(15,'2016-07-24 00:00:00','2016-07-24 00:00:00'),(16,'2016-07-02 00:00:00','2016-07-02 00:00:00'),(19,'2016-05-30 00:00:00','2016-05-30 00:00:00'),(37,'2016-06-08 00:00:00','2016-06-08 00:00:00'),(38,'2016-06-22 00:00:00','2016-06-22 00:00:00'),(39,'2016-07-27 00:00:00','2016-07-27 00:00:00'),(40,'2016-07-30 00:00:00','2016-07-30 00:00:00');

/*Table structure for table `training_events` */

DROP TABLE IF EXISTS `training_events`;

CREATE TABLE `training_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trainingId` int(11) DEFAULT NULL,
  `start` date DEFAULT NULL,
  `end` date DEFAULT NULL,
  `location` text COLLATE utf8_unicode_ci COMMENT 'will add gmaps coordinates values',
  `slots` int(11) DEFAULT NULL,
  `requirements` text COLLATE utf8_unicode_ci,
  `additionalNotes` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `FK_training_events` (`trainingId`),
  CONSTRAINT `FK_training_events` FOREIGN KEY (`trainingId`) REFERENCES `trainings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `training_events` */

insert  into `training_events`(`id`,`trainingId`,`start`,`end`,`location`,`slots`,`requirements`,`additionalNotes`) values (2,2,'2016-05-31','2016-05-31','Santa Rosa City Sports Complex',150,'ballpen|notebook|agency id|pocket money at least 150pesos|extra clothes for 4 days','4 na araw po etong event pero libre na po ako pagkaen naten dito fak'),(11,1,'2016-06-16','2016-06-17','Santa Rosa, Yellow Bell',90,'asd|dsa|qwe','wala lang, g lang ng G!'),(14,2,'2016-06-03','2016-06-03','Santa Rosa',88,'KJ|SDF','pwede magdala ng sariling tae'),(15,1,'2016-06-21','2016-06-22','Alonte Sports Complex, Binan, Laguna',100,'ballpen|notebook','G lang ng G'),(16,4,'2016-07-20','2016-07-20','Santa Rosa Sports Complex',200,'ballpen|laptop|smartphone','magdala na rin ng sariling extension chord kung kailan!'),(17,3,'2016-07-29','2016-07-30','Santa Rosa Sports Complex',100,'ballpen|id','wag naw ag kakalimutan ang id. no id no entry!'),(18,4,'2016-07-29','2016-07-29','Alonte Sports Complex',50,'laptop|extension|charger|id',''),(19,4,'2016-08-20','2016-08-21','Santa Rosa',99,'id|notebook|lapis or ballpen','pasok lang basta may id');

/*Table structure for table `trainings` */

DROP TABLE IF EXISTS `trainings`;

CREATE TABLE `trainings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `trainings` */

insert  into `trainings`(`id`,`name`,`description`) values (1,'Self Defense 1','Learn how to defend yourself against criminals acts and other bad elements.'),(2,'Handwriting Mastery 2nd Edition','much description, very wow'),(3,'Introduction To ReactJS','This is introduction only and is intended for beginners.'),(4,'AngularJS Pro Edition','Advanced training for created angular powered web apps');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
